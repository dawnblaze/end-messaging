﻿using Endor.Comm.ApiModels;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Exceptions;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.Services.Classes;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.FunctionalTests.DtoBuilders;
using Endor.Comm.UnitTests.ObjectBuilders;
using Endor.Comm.Web.Common;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.Comm.FunctionalTests.ControllerTests
{
    [TestClass]
    public class EmailAccountControllerTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/emailaccount";
        private byte locationID;
        public const string encryptionPassphrase = "aQhnjYNsk4HOmUvlJ9mBGVrXan9L5Bxe";
       
        private async Task<EmailAccountData> SimpleTestEmailAccountSaved()
        {
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            locationID = domainEmail.LocationLinks.First().LocationID;
            await repository.AddAsync<DomainEmail>(domainEmail);
            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();
            await repository.AddAsync<EmailAccountData>(emailAccount);

            return emailAccount;
        }

        [TestMethod]
        public async Task EmailAccountCRUDSucceed()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            #region CREATE
            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var originalEmployeeID = newEmailAccount.EmployeeID;
            newEmailAccount.EmployeeID = null;
            await AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.BadRequest);

            newEmailAccount.EmployeeID = originalEmployeeID;
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            dapperRepo.SetTableNameForClassType(1023);
            var savedEntry = await dapperRepo.GetByShortIdAsync<EmailAccountData>((short)email.ID);
            var credentials = JsonConvert.DeserializeObject<EmailAccountCredentials>(savedEntry.Credentials);

            StringEncryptor.Decrypt(credentials.Password, encryptionPassphrase).Should().Be(newEmailAccount.NewPassword);

            #endregion

            #region RETRIEVE

            var getEmail = await AssertGetStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}", HttpStatusCode.OK);
            getEmail.HasPassword.Should().Be(true);
            await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}/", HttpStatusCode.OK);

            // GET .../simplelist
            var simpleList = await AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/SimpleList?IsActive=false", HttpStatusCode.OK);
            foreach (SimpleEmailAccountData sga in simpleList)
            {
                if (sga.DisplayName == email.DisplayName)
                {
                    email.ID = sga.ID;
                }
            }
            #endregion

            #region UPDATE

            email.DomainName = domainEmail.DomainName;
            await AssertPutStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}", JsonConvert.SerializeObject(email), HttpStatusCode.OK);

            email.NewPassword = "other";
            await AssertPutStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}", JsonConvert.SerializeObject(email), HttpStatusCode.OK);
            
            savedEntry = await dapperRepo.GetByShortIdAsync<EmailAccountData>((short)email.ID);
            var newUpdatedCredentials = JsonConvert.DeserializeObject<EmailAccountCredentials>(savedEntry.Credentials);
            
            StringEncryptor.Decrypt(newUpdatedCredentials.Password, encryptionPassphrase).Should().Be(email.NewPassword);

            var oldPass = email.NewPassword;
            email.NewPassword = null;
            await AssertPutStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}", JsonConvert.SerializeObject(email), HttpStatusCode.OK);

            StringEncryptor.Decrypt(newUpdatedCredentials.Password, encryptionPassphrase).Should().Be(oldPass);

            #endregion

            #region DELETE

            await AssertDeleteStatusCode(client, $"{apiUrl}/{email.ID}", HttpStatusCode.OK);

            #endregion
        }

        [TestMethod]
        public async Task EmailAccountGetByFiltersSucceed() 
        {
            //Arrange
            var initialFalseCount = await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            var initialTrueCount = await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var result1 = await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            var result2 = await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);

            result1.Count().Should().Equals(initialFalseCount.Count() + 1);
            result2.Count().Should().Equals(initialTrueCount.Count());
        }
        
        [TestMethod]
        public async Task EmailAccountGetByLocationSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var originalResult = await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}/location/{domainEmail.LocationLinks.First().LocationID}?IsActive=true", HttpStatusCode.OK);
            await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var result1 = await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}/location/{domainEmail.LocationLinks.First().LocationID}?IsActive=true", HttpStatusCode.OK);

            result1.Should().HaveCount(originalResult.Count() + 1);
        }
        
        [TestMethod]
        public async Task EmailAccountGetByLocationSimpleListSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;

            var originalResult = await AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/location/{domainEmail.LocationLinks.First().LocationID}/simplelist?IsActive=false", HttpStatusCode.OK);
            await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var result1 = await AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/location/{domainEmail.LocationLinks.First().LocationID}/simplelist?IsActive=false", HttpStatusCode.OK);

            result1.Should().HaveCount(originalResult.Count() + 1);
        }
        
        [TestMethod]
        public async Task EmailAccountGetByEmployeeSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var originalResult = await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}/employee/{newEmailAccount.EmployeeID}?IsActive=false", HttpStatusCode.OK);
            await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var result1 = await AssertGetStatusCode<EmailAccountDto[]>(client, $"{apiUrl}/employee/{newEmailAccount.EmployeeID}?IsActive=false&IncludeShared=false", HttpStatusCode.OK);

            result1.Should().HaveCount(originalResult.Count() + 1);
        }
        
        [TestMethod]
        public async Task EmailAccountGetByEmployeeSimpleListSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;

            var originalInActiveResult = await AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/employee/{newEmailAccount.EmployeeID}/simplelist?IsActive=false", HttpStatusCode.OK);
            var originalActiveResult = await AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/employee/{newEmailAccount.EmployeeID}/simplelist?IsActive=true", HttpStatusCode.OK);
            await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var resultInActive1 = await AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/employee/{newEmailAccount.EmployeeID}/simplelist?IsActive=false&IncludeShared=false", HttpStatusCode.OK);

            resultInActive1.Should().HaveCount(originalInActiveResult.Count() + 1);
        }
        
        [TestMethod]
        public async Task EmailAccountSimpleListSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;

            var originalResult = await AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/simplelist?IsActive=false", HttpStatusCode.OK);

            await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var result1 = await AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/simplelist?IsActive=false&IncludeShared=false", HttpStatusCode.OK);

            result1.Should().HaveCount(originalResult.Count() + 1);
        }
        
        [TestMethod]
        public async Task EmailAccountDefaultNotFound() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            await AssertGetStatusCode<EmailAccountDto>(client, $"{apiUrl}/default", HttpStatusCode.NotFound);

        }
        
        [TestMethod]
        public async Task EmailAccountEmployeeDefaultNotFound() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            await AssertGetStatusCode<EmailAccountDto>(client, $"{apiUrl}/employee/{newEmailAccount.EmployeeID}/default", HttpStatusCode.NotFound);
        }
        
        [TestMethod]
        public async Task EmailAccountSetActiveSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var result = await AssertPostStatusCode<SuccessfulActionResponse>(client, $"{apiUrl}/{email.ID}/action/setactive", null, HttpStatusCode.OK);

            result.message.Should().Contain(" active");
        }
        
        [TestMethod]
        public async Task EmailAccountSetInactiveSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var result = await AssertPostStatusCode<SuccessfulActionResponse>(client, $"{apiUrl}/{email.ID}/action/setinactive", null, HttpStatusCode.OK);

            result.message.Should().Contain("inactive");
        }

        [TestMethod]
        public async Task EmailAccountCanDeleteSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var result = await AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{email.ID}/action/candelete", HttpStatusCode.OK);
        }
        
        [TestMethod]
        public async Task EmailAccountLinkTeamSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var employeeTeamRespository = GetDapperRepository();
            var employeeTeamW = EmployeeTeam.GetEmployeeTeamWithEmployeeTeamLinks(employeeTeamRespository.Connection(), 1).Where(a => a.EmployeeTeamLinks.Any()).ToList();
            
            var result = await AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{email.ID}/Action/LinkTeam/{employeeTeamW.First().ID}", null, HttpStatusCode.OK);

        }

        [TestMethod]
        public async Task EmailAccountUnLinkTeamSucceed() 
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            var employeeTeamRespository = GetDapperRepository();
            var employeeTeamW = EmployeeTeam.GetEmployeeTeamWithEmployeeTeamLinks(employeeTeamRespository.Connection(), 1).Where(a => a.EmployeeTeamLinks.Any()).ToList();

            var result = await AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{email.ID}/Action/LinkTeam/{employeeTeamW.First().ID}", null, HttpStatusCode.OK);

            var result1 = await AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{email.ID}/Action/UnLinkTeam/{employeeTeamW.First().ID}", null, HttpStatusCode.OK);

        }

        [TestMethod]
        public async Task EmailAccountCRUDSWithTeamsSucceeds()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            #region CREATE
            var employeeTeamRespository = GetDapperRepository();
            var employeeTeamW = EmployeeTeam.GetEmployeeTeamWithEmployeeTeamLinks(employeeTeamRespository.Connection(), 1).Where(a => a.EmployeeTeamLinks.Any()).ToList();

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .AddEmployeeTeamLink(employeeTeamW.FirstOrDefault().ID)
                .Build();

            var origEmployeeID = newEmailAccount.EmployeeID;
            newEmailAccount.EmployeeID = null;
            newEmailAccount.DomainEmailID = domainEmail.IDAsShort;
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);
            #endregion

            #region RETRIEVE

            var result = await AssertGetStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}", HttpStatusCode.OK);
            result.EmailAccountTeamLinks.Count().Should().Be(0);
            #endregion

            #region UPDATE

            result.EmployeeID = origEmployeeID;
            result.EmailAccountTeamLinks = new List<EmailAccountTeamLinkDto>();
            await AssertPutStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}", JsonConvert.SerializeObject(result), HttpStatusCode.OK);

            result = await AssertGetStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}", HttpStatusCode.OK);
            result.EmailAccountTeamLinks.Count().Should().Be(0);

            result.EmailAccountTeamLinks = new List<EmailAccountTeamLinkDto>() {
                new EmailAccountTeamLinkDto()
                {
                    EmailAccountID = (short)result.ID,
                    TeamID = employeeTeamW.FirstOrDefault().ID
                }};
            result.EmployeeID = null;
            await AssertPutStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}", JsonConvert.SerializeObject(result), HttpStatusCode.OK);
            result = await AssertGetStatusCode<EmailAccountDto>(client, $"{apiUrl}/{email.ID}?IncludeTeams=1", HttpStatusCode.OK);
            result.EmailAccountTeamLinks.Count().Should().Be(1);

            #endregion

            #region DELETE

            await AssertDeleteStatusCode(client, $"{apiUrl}/{email.ID}", HttpStatusCode.OK);

            #endregion
        }

        [TestMethod]
        public async Task EmailAccountTestMicrosoftEndpoint()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();

            domainEmail.DomainName = "corebridge.net";
            domainEmail.SMTPAddress = "smtp.office365.com";
            domainEmail.SMTPPort = 587;
            domainEmail.SMTPSecurityType = EmailSecurityType.TLS;
            domainEmail.SMTPAuthenticateFirst = true;


            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainName = "corebridge.net";
            newEmailAccount.AliasUserNames = "Scott St. Cyr";
            newEmailAccount.UserName = "scotts";
            newEmailAccount.NewPassword = JsonConvert.SerializeObject(new EmailAccountCredentials()
            {
                Password = "zcfzsxqyvnxvmngj"
            });

            //Act
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            //Assert
            await AssertPostStatusCode(client, $"{apiUrl}/{email.ID}/action/test", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);
        }


        [TestMethod]
        public async Task EmailAccountTestMicrosoftEndpoint5Times()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();

            domainEmail.DomainName = "corebridge.net";
            domainEmail.SMTPAddress = "smtp.office365.com";
            domainEmail.SMTPPort = 587;
            domainEmail.SMTPSecurityType = EmailSecurityType.TLS;
            domainEmail.SMTPAuthenticateFirst = true;

            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainName = "corebridge.net";
            newEmailAccount.AliasUserNames = "Scott St. Cyr";
            newEmailAccount.UserName = "scotts";
            newEmailAccount.NewPassword = JsonConvert.SerializeObject(new EmailAccountCredentials()
            {
                Password = "zcfzsxqyvnxvmngj"
            });

            //Act
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            //Assert
            for (var i = 1; i <= 5; i++)
            {
                await AssertPostStatusCode(client, $"{apiUrl}/{email.ID}/action/test", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);
            }

        }

        [TestMethod]
        public async Task EmailAccountTestGmailEndpoint()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();

            domainEmail.DomainName = "corebridge.net";
            domainEmail.SMTPAddress = "smtp.gmail.com";
            domainEmail.SMTPPort = 465;
            domainEmail.SMTPSecurityType = EmailSecurityType.SSL;
            domainEmail.SMTPAuthenticateFirst = true;

            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainName = "corebridge.net";
            newEmailAccount.AliasUserNames = "Scott St. Cyr";
            newEmailAccount.UserName = "scotts";
            newEmailAccount.NewPassword = JsonConvert.SerializeObject(new EmailAccountCredentials()
            {
                Password = "dbvdssbjniricrqi"
            });

            //Act
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            //Assert
            await AssertPostStatusCode(client, $"{apiUrl}/{email.ID}/action/test", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);
        }


        [TestMethod]
        public async Task EmailAccountTestGmailEndpoint5Times()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();

            domainEmail.DomainName = "corebridge.net";
            domainEmail.SMTPAddress = "smtp.gmail.com";
            domainEmail.SMTPPort = 465;
            domainEmail.SMTPSecurityType = EmailSecurityType.SSL;
            domainEmail.SMTPAuthenticateFirst = true;

            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            var newEmailAccount = new EmailAccountDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .DomainEmailId(domainEmail.IDAsShort)
                .Build();

            newEmailAccount.DomainName = "corebridge.net";
            newEmailAccount.AliasUserNames = "Scott St. Cyr";
            newEmailAccount.UserName = "scotts";
            newEmailAccount.NewPassword = JsonConvert.SerializeObject(new EmailAccountCredentials()
            {
                Password = "dbvdssbjniricrqi"
            });

            //Act
            var email = await AssertPostStatusCode<EmailAccountDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            //Assert
            for(var i = 1; i <= 5; i++)
            {
                await AssertPostStatusCode(client, $"{apiUrl}/{email.ID}/action/test", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);
            }
            
        }
    }
}
