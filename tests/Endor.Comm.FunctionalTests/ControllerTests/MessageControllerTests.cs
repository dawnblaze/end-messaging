﻿using Endor.Comm.ApiModels;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Exceptions;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.Specifications;
using Endor.Comm.FunctionalTests.DtoBuilders;
using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.FunctionalTests
{
    [TestClass]
    public class MessageControllerTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/message";

        private async Task<MessageHeader> SimpleTestMessageHeader()
        {
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Build();
            await repository.AddAsync<MessageBody>(messageBody);
            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageHeader>(messageHeader);
            return messageHeader;
        }
        
        [TestMethod]
        public async Task MarkMessageAsRead()
        {
            //Arrange
            var messageHeader = await SimpleTestMessageHeader();

            //Act
            await AssertPostStatusCode(client, $"{apiUrl}/{messageHeader.ID}/action/markasread", "", System.Net.HttpStatusCode.OK);

            //Assert
            repository.Refresh(messageHeader);
            var result = await repository.GetByIdAsync<MessageHeader>(-100);
            result.CheckIsRead().Should().BeTrue();
        }

        [TestMethod]
        public async Task MarkMessageAsReadReturnsNotFoundIfDoesNotExist()
        {
            //Arrange

            //Act
            await AssertPostStatusCode(client, $"{apiUrl}/-100/action/markasread", "", System.Net.HttpStatusCode.NotFound);

            //Assert
        }

        [TestMethod]
        public async Task CreateMessageTest()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var participantTo = new MessageParticipantInfoBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            var participantFrom = new MessageParticipantInfoBuilder()
                .WithDefaultValues(dapperRepo)
                .ParticipantRoleType(MessageParticipantRoleType.From)
                .Build();
            var message = new MessageDtoBuilder()
                .WithDefaultValues(dapperRepo)
                .AddMessageParticipantInfo(participantTo)
                .AddMessageParticipantInfo(participantFrom)
                .Build();

            //Act
            var postResult = await AssertPostStatusCode<MessageDto>(client, $"{apiUrl}", message, System.Net.HttpStatusCode.OK);
            var postedMessageID = postResult.ID;
            //Assert

            //Cleanup
            await AssertDeleteStatusCode(client, $"/api/message/{postedMessageID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task GetMessagesFromChannelMixedResults()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Id(-99).Build();
            var messageBodyII = new MessageBodyBuilder()
                .WithDefaultValues().Id(-100).Build();
            var messageBodyIII = new MessageBodyBuilder()
                .WithDefaultValues().Id(-101).Build();

            await repository.AddAsync<MessageBody>(messageBody);
            await repository.AddAsync<MessageBody>(messageBodyII);
            await repository.AddAsync<MessageBody>(messageBodyIII);

            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-99)
                .MessageBodyId(messageBody.ID)
                .Build();
            var messageParticipantInfoII = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-100)
                .MessageBodyId(messageBodyII.ID)
                .Build();
            var messageParticipantInfoIII = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-101)
                .MessageBodyId(messageBodyII.ID)
                .Build();

            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfoII);
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfoIII);

            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-99)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Build();

            var messageHeaderII = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-100)
                .MessageParticipantInfoId(messageParticipantInfoII.ID)
                .MessageBodyId(messageBodyII.ID)
                .Build();

            var messageHeaderIII = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-101)
                .MessageParticipantInfoId(messageParticipantInfoIII.ID)
                .MessageBodyId(messageBodyIII.ID)
                .Build();

            await repository.AddAsync<MessageHeader>(messageHeader);
            await repository.AddAsync<MessageHeader>(messageHeaderII);
            await repository.AddAsync<MessageHeader>(messageHeaderIII);

            //Act
            var result = await AssertGetStatusCode<MessageDto[]>(client, $"{apiUrl}/channel/{(int)messageHeader.Channels}?EmployeeID={messageHeader.EmployeeID}", System.Net.HttpStatusCode.OK);
            var resultTake3 = await AssertGetStatusCode<MessageDto[]>(client, $"{apiUrl}/channel/{(int)messageHeader.Channels}?take=3&&EmployeeID={messageHeader.EmployeeID}", System.Net.HttpStatusCode.OK);
            var resultTEmployeeID0 = await AssertGetStatusCode<MessageDto[]>(client, $"{apiUrl}/channel/{(int)messageHeader.Channels}?EmployeeID={messageHeader.EmployeeID}", System.Net.HttpStatusCode.OK);

            //Assert
            result.Should().NotBeEmpty().Should().NotBeNull();
            result.Count().Should().BeGreaterOrEqualTo(3);
            result.FirstOrDefault(a => a.ID == messageHeader.ID).Should().NotBeNull();
            result.FirstOrDefault(a => a.ID == messageHeaderII.ID).Should().NotBeNull();
            result.FirstOrDefault(a => a.ID == messageHeaderIII.ID).Should().NotBeNull();

            resultTake3.Should().NotBeEmpty().Should().NotBeNull();
            resultTake3.Count().Should().Be(3);
            resultTake3.FirstOrDefault(a => a.ID == messageHeader.ID).Should().NotBeNull();
            resultTake3.FirstOrDefault(a => a.ID == messageHeaderII.ID).Should().NotBeNull();
            resultTake3.FirstOrDefault(a => a.ID == messageHeaderIII.ID).Should().NotBeNull();

            resultTEmployeeID0.Should().NotBeEmpty().Should().NotBeNull();
            resultTEmployeeID0.Count().Should().BeGreaterOrEqualTo(3);
            resultTEmployeeID0.FirstOrDefault(a => a.ID == messageHeader.ID).Should().NotBeNull();
            resultTEmployeeID0.FirstOrDefault(a => a.ID == messageHeaderII.ID).Should().NotBeNull();
            resultTEmployeeID0.FirstOrDefault(a => a.ID == messageHeaderIII.ID).Should().NotBeNull();
        }

        [TestMethod]
        public async Task GetSingleMessage()
        {
            //Arrange
            var messageHeader = await SimpleTestMessageHeader();

            //Act
            var result = await AssertGetStatusCode<MessageDto>(client, $"{apiUrl}/{messageHeader.ID}", System.Net.HttpStatusCode.OK);

            //Assert
            result.ID.Should().Be(messageHeader.ID);
        }

        [TestMethod]
        public async Task DeleteSingleMessage()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Build();
            await repository.AddAsync<MessageBody>(messageBody);
            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Id(int.MaxValue)
                .Build();
            await repository.AddAsync<MessageHeader>(messageHeader);

            //Act - Calling Twice because first just mark as deleted 
            await AssertDeleteStatusCode(client, $"{apiUrl}/{messageHeader.ID}", System.Net.HttpStatusCode.OK);
            await AssertDeleteStatusCode(client, $"{apiUrl}/{messageHeader.ID}", System.Net.HttpStatusCode.OK);

            //Assert
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await repository.GetByIdAsync<MessageHeader>(messageHeader.ID));

            await dbContext.Entry(messageHeader).ReloadAsync();
        }

        [TestMethod]
        public async Task SingleMessageActionMarkAsReadSucceed()
        {
            //Arrange
            var messageHeader = await SimpleTestMessageHeader();

            //Act
            await AssertPostStatusCode(client, $"{apiUrl}/{messageHeader.ID}/action/markasread", null, System.Net.HttpStatusCode.OK);
            await dbContext.Entry(messageHeader).ReloadAsync();

            //Assert
            messageHeader.IsRead.Should().Be(true);
        }

        [TestMethod]
        public async Task SingleMessageActionMarkAsUnReadSucceed()
        {
            //Arrange
            var messageHeader = await SimpleTestMessageHeader();

            //Act/Assert
            await AssertPostStatusCode(client, $"{apiUrl}/{messageHeader.ID}/action/markasread", null, System.Net.HttpStatusCode.OK);
            await dbContext.Entry(messageHeader).ReloadAsync();
            messageHeader.IsRead.Should().Be(true);

            await AssertPostStatusCode(client, $"{apiUrl}/{messageHeader.ID}/action/markasunread", null, System.Net.HttpStatusCode.OK);
            await dbContext.Entry(messageHeader).ReloadAsync();
            messageHeader.IsRead.Should().Be(false);
        }

        [TestMethod]
        public async Task SingleMessageActionUnDeleteSucceed()
        {
            //Arrange
            var messageHeader = await SimpleTestMessageHeader();

            //Act
            await AssertDeleteStatusCode(client, $"{apiUrl}/{messageHeader.ID}", System.Net.HttpStatusCode.OK);
            await dbContext.Entry(messageHeader).ReloadAsync();
            messageHeader.IsDeleted.Should().Be(true);

            await AssertPostStatusCode(client, $"{apiUrl}/{messageHeader.ID}/action/undelete", null, System.Net.HttpStatusCode.OK);
            await dbContext.Entry(messageHeader).ReloadAsync();

            //Assert
            messageHeader.IsDeleted.Should().Be(false);
        }

        [TestMethod]
        public async Task SingleMessageActionMarkAllReadSucceed()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Id(-99).Build();
            var messageBodyII = new MessageBodyBuilder()
                .WithDefaultValues().Id(-100).Build();
            var messageBodyIII = new MessageBodyBuilder()
                .WithDefaultValues().Id(-101).Build();

            await repository.AddAsync<MessageBody>(messageBody);
            await repository.AddAsync<MessageBody>(messageBodyII);
            await repository.AddAsync<MessageBody>(messageBodyIII);

            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-99)
                .MessageBodyId(messageBody.ID)
                .Build();
            var messageParticipantInfoII = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-100)
                .MessageBodyId(messageBodyII.ID)
                .Build();
            var messageParticipantInfoIII = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-101)
                .MessageBodyId(messageBodyII.ID)
                .Build();

            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfoII);
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfoIII);

            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-99)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Build();

            var messageHeaderII = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-100)
                .MessageParticipantInfoId(messageParticipantInfoII.ID)
                .MessageBodyId(messageBodyII.ID)
                .Build();

            var messageHeaderIII = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-101)
                .MessageParticipantInfoId(messageParticipantInfoIII.ID)
                .MessageBodyId(messageBodyIII.ID)
                .Build();

            await repository.AddAsync<MessageHeader>(messageHeader);
            await repository.AddAsync<MessageHeader>(messageHeaderII);
            await repository.AddAsync<MessageHeader>(messageHeaderIII);

            //Act
            await AssertPostStatusCode(client, $"{apiUrl}/channel/{(int)messageHeader.Channels}/action/markallread", null, System.Net.HttpStatusCode.OK);
            var result = await AssertGetStatusCode<MessageDto[]>(client, $"{apiUrl}/channel/{(int)messageHeader.Channels}?EmployeeID={messageHeader.EmployeeID}", System.Net.HttpStatusCode.OK);

            //Assert
            result.All(a => a.IsRead).Should().Be(true);
        }

        [TestMethod]
        public async Task SingleMessageActionDeleteAllSucceed()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Id(-99).Build();
            var messageBodyII = new MessageBodyBuilder()
                .WithDefaultValues().Id(-100).Build();
            var messageBodyIII = new MessageBodyBuilder()
                .WithDefaultValues().Id(-101).Build();

            await repository.AddAsync<MessageBody>(messageBody);
            await repository.AddAsync<MessageBody>(messageBodyII);
            await repository.AddAsync<MessageBody>(messageBodyIII);

            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-99)
                .MessageBodyId(messageBody.ID)
                .Build();
            var messageParticipantInfoII = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-100)
                .MessageBodyId(messageBodyII.ID)
                .Build();
            var messageParticipantInfoIII = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .Id(-101)
                .MessageBodyId(messageBodyII.ID)
                .Build();

            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfoII);
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfoIII);

            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-99)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Build();

            var messageHeaderII = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-100)
                .MessageParticipantInfoId(messageParticipantInfoII.ID)
                .MessageBodyId(messageBodyII.ID)
                .Build();

            var messageHeaderIII = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .Id(-101)
                .MessageParticipantInfoId(messageParticipantInfoIII.ID)
                .MessageBodyId(messageBodyIII.ID)
                .Build();

            await repository.AddAsync<MessageHeader>(messageHeader);
            await repository.AddAsync<MessageHeader>(messageHeaderII);
            await repository.AddAsync<MessageHeader>(messageHeaderIII);

            //Act
            await AssertPostStatusCode(client, $"{apiUrl}/channel/{(int)messageHeader.Channels}/action/deleteall", null, System.Net.HttpStatusCode.OK);
            var result = await AssertGetStatusCode<MessageDto[]>(client, $"{apiUrl}/channel/{(int)messageHeader.Channels}?EmployeeID={messageHeader.EmployeeID}", System.Net.HttpStatusCode.OK);

            //Assert
            result.All(a => a.IsDeleted).Should().Be(true);
        }
    }
}
