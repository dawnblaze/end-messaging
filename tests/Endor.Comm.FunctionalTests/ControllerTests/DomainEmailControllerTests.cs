﻿using Endor.Comm.ApiModels;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Exceptions;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.FunctionalTests.DtoBuilders;
using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.Comm.FunctionalTests.ControllerTests
{
    [TestClass]
    public class DomainEmailControllerTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/domain/email";

        private async Task<DomainEmail> SimpleTestDomainEmail()
        {
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();
            await repository.AddAsync<DomainEmail>(domainEmail);

            return domainEmail;
        }

        [TestMethod]
        public async Task GetDomainEmailSucceed()
        {
            //Arrange
            var domainEmail = await SimpleTestDomainEmail();

            //Act
            var result = await AssertGetStatusCode<DomainEmailDto>(client, $"{apiUrl}/{domainEmail.ID}", HttpStatusCode.OK);
            var result2 = await AssertGetStatusCode<DomainEmailDto[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            var result3 = await AssertGetStatusCode<DomainEmailDto[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            var result4 = await AssertGetStatusCode<DomainEmailDto[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);

            //Assert
            result.ID.Should().Be((short)domainEmail.ID);
            result2.Count().Should().BeGreaterThan(0);
            result3.Count().Should().BeGreaterThan(0);
            result4.Count().Should().BeGreaterOrEqualTo(0);
        }

        [TestMethod]
        public async Task CreateDomainEmailTemplateSucceed()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailDtoBuilder()
                            .WithDefaultValues(dapperRepo)
                            .Build();


            //Act
            var result = await AssertPostStatusCode<DomainEmailDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(domainEmail), HttpStatusCode.OK);
            
            //Assert
            result.ID.Should().BeGreaterThan(0);
            result.DomainName.Should().Be(domainEmail.DomainName);
            result.ProviderType.Should().Be(domainEmail.ProviderType);
            result.SMTPConfigurationType.Should().Be(domainEmail.SMTPConfigurationType);
            result.SMTPAddress.Should().Be(domainEmail.SMTPAddress);
            result.SMTPPort.Should().Be(domainEmail.SMTPPort);
            result.SMTPSecurityType.Should().Be(domainEmail.SMTPSecurityType);
            result.SMTPAuthenticateFirst.Should().Be(domainEmail.SMTPAuthenticateFirst);
            result.LastVerificationAttemptDT.Should().Be(domainEmail.LastVerificationAttemptDT);
            result.LastVerificationSuccess.Should().Be(domainEmail.LastVerificationSuccess);
            result.LastVerificationResult.Should().Be(domainEmail.LastVerificationResult);
            result.LastVerifiedDT.Should().Be(domainEmail.LastVerifiedDT);
            result.IsForAllLocations.Should().Be(domainEmail.IsForAllLocations);
            result.SimpleLocations.Count.Should().Be(domainEmail.SimpleLocations.Count);
            //CleanUp
            await AssertDeleteStatusCode(client, $"{apiUrl}/{result.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task UpdateDomainEmailSucceed()
        {
            //Arrange
            var domainEmail = await SimpleTestDomainEmail();

            //Act
            var result = await AssertGetStatusCode<DomainEmailDto>(client, $"{apiUrl}/{domainEmail.ID}", HttpStatusCode.OK);

            result.DomainName = "New Updated Domain Email Name";

            await AssertPutStatusCode(client, $"{apiUrl}/{domainEmail.ID}", JsonConvert.SerializeObject(result), HttpStatusCode.OK);
            var updatedsResult = await AssertGetStatusCode<DomainEmailDto>(client, $"{apiUrl}/{domainEmail.ID}", HttpStatusCode.OK);

            //Assert
            domainEmail.DomainName.Should().NotBe(updatedsResult.DomainName);
            updatedsResult.DomainName.Should().Be(result.DomainName);

            repository.Refresh(domainEmail);
        }

        [TestMethod]
        public async Task DeleteDomainEmailSucceed()
        {
            //Arrange
            var domainEmail = await SimpleTestDomainEmail();

            //Act
            await AssertDeleteStatusCode(client, $"{apiUrl}/{domainEmail.ID}", HttpStatusCode.OK);

            //break this relationship to allow the refresh
            var count = domainEmail.LocationLinks.Count;
            for (var i=0; i< count; i++)
            {
                var locationLink = domainEmail.LocationLinks[i];
                repository.Refresh(locationLink);
            }
            domainEmail.LocationLinks = new List<DomainEmailLocationLink>(); 
            repository.Refresh(domainEmail);

            //Assert
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await repository.GetByShortIdAsync<DomainEmail>(-100));
        }

        [TestMethod]
        public async Task TestDomainEmailSetActiveInactive()
        {
            //Arrange
            var template = await SimpleTestDomainEmail();

            //Act/Assert

            // set inactive
            await AssertPostStatusCode(client, $"{apiUrl}/{template.ID}/Action/SetInactive", null, HttpStatusCode.OK);

            var adHocTemplateTest = await AssertGetStatusCode<DomainEmailDto>(client, $"{apiUrl}/{template.ID}", HttpStatusCode.OK);
            adHocTemplateTest.IsActive.Should().BeFalse();

            // set active
            await AssertPostStatusCode(client, $"{apiUrl}/{template.ID}/Action/SetActive", null, HttpStatusCode.OK);

            // confirm adhoc is now active again
            adHocTemplateTest = await AssertGetStatusCode<DomainEmailDto>(client, $"{apiUrl}/{template.ID}", HttpStatusCode.OK);
            adHocTemplateTest.IsActive.Should().BeTrue();
        }

        [TestMethod]
        public async Task TestDomainEmailCanDeleteFalse()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailDtoBuilder()
                            .WithDefaultValues(dapperRepo)
                            .Build();

            //Act
            var adHocTemplate = await AssertPostStatusCode<DomainEmail>(client, $"{apiUrl}", JsonConvert.SerializeObject(domainEmail), HttpStatusCode.OK);
            var statusResp = await AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{adHocTemplate.ID}/Action/CanDelete", HttpStatusCode.OK);

            //Assert
            statusResp.Value.Should().BeFalse();

            //CleanUp
            await AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestDomainEmailCanDeleteTrue()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailDtoBuilder()
                            .WithDefaultValues(dapperRepo)
                            .Build();
            domainEmail.SimpleLocations = new List<SimpleListItem>();

            //Act
            var adHocTemplate = await AssertPostStatusCode<DomainEmail>(client, $"{apiUrl}", JsonConvert.SerializeObject(domainEmail), HttpStatusCode.OK);
            var statusResp = await AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{adHocTemplate.ID}/Action/CanDelete", HttpStatusCode.OK);

            //Assert
            statusResp.Value.Should().BeTrue();

            //CleanUp
            await AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestDomainEmailRemovingLocationLink()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailDtoBuilder()
                            .WithDefaultValues(dapperRepo)
                            .Build();
            var linkLocation = domainEmail.SimpleLocations.First();


            //Act
            var domainEmailDto = await AssertPostStatusCode<DomainEmailDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(domainEmail), HttpStatusCode.OK);

            // Test Move Before
            var linkResult = await AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{domainEmailDto.ID}/action/unlinklocation/{linkLocation.ID}", null, HttpStatusCode.OK);

            var result = await AssertGetStatusCode<DomainEmailDto>(client, $"{apiUrl}/{domainEmailDto.ID}", HttpStatusCode.OK);

            //Assert
            result.LocationLinks.Count.Should().Be(0);
            await AssertDeleteStatusCode(client, $"{apiUrl}/{domainEmailDto.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestDomainEmailAddLocationLink()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailDtoBuilder()
                            .WithDefaultValues(dapperRepo)
                            .Build();
            var linkLocation = domainEmail.SimpleLocations.First();
            domainEmail.SimpleLocations = new List<SimpleListItem>();


            //Act
            var domainEmailDto = await AssertPostStatusCode<DomainEmailDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(domainEmail), HttpStatusCode.OK);

            // Test Move Before
            var linkResult = await AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{domainEmailDto.ID}/action/linklocation/{linkLocation.ID}", null, HttpStatusCode.OK);

            var result = await AssertGetStatusCode<DomainEmailDto>(client, $"{apiUrl}/{domainEmailDto.ID}", HttpStatusCode.OK);

            //Assert
            result.LocationLinks.Count.Should().Be(1);
            await AssertDeleteStatusCode(client, $"{apiUrl}/{domainEmailDto.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task DomainEmailTestGmailEndpoint()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();

            domainEmail.DomainName = "corebridge.net";
            domainEmail.SMTPAddress = "smtp.gmail.com";
            domainEmail.SMTPPort = 465;
            domainEmail.SMTPSecurityType = EmailSecurityType.SSL;
            domainEmail.SMTPAuthenticateFirst = true;

            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            //Assert
            await AssertPostStatusCode(client, $"{apiUrl}/{domainEmail.IDAsShort}/action/test", JsonConvert.SerializeObject(new {
                emailaddress = "scotts@corebridge.net",
                password = "dbvdssbjniricrqi"
            }), HttpStatusCode.OK);

            await AssertPostStatusCode(client, $"{apiUrl}/{domainEmail.IDAsShort}/action/test", JsonConvert.SerializeObject(new
            {
                emailaddress = "scotts",
                password = "dbvdssbjniricrqi"
            }), HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task DomainEmailTestMicrosoftEndpoint()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(dapperRepo)
                .Build();

            domainEmail.DomainName = "corebridge.net";
            domainEmail.SMTPAddress = "smtp.office365.com";
            domainEmail.SMTPPort = 587;
            domainEmail.SMTPSecurityType = EmailSecurityType.TLS;
            domainEmail.SMTPAuthenticateFirst = true;

            domainEmail = await repository.AddAsync<DomainEmail>(domainEmail);

            //Assert
            await AssertPostStatusCode(client, $"{apiUrl}/{domainEmail.IDAsShort}/action/test", JsonConvert.SerializeObject(new
            {
                emailaddress = "scotts@corebridge.net",
                password = "zcfzsxqyvnxvmngj"
            }), HttpStatusCode.OK);


            await AssertPostStatusCode(client, $"{apiUrl}/{domainEmail.IDAsShort}/action/test", JsonConvert.SerializeObject(new
            {
                emailaddress = "scotts",
                password = "zcfzsxqyvnxvmngj"
            }), HttpStatusCode.OK);
        }
    }
}
