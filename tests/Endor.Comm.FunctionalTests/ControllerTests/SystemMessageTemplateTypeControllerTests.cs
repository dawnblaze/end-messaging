﻿using Endor.Comm.ApiModels;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Exceptions;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.Comm.FunctionalTests.ControllerTests
{
    [TestClass]
    public class SystemMessageTemplateTypeControllerTests : CommonControllerTestClass
    {
        public const string apiUrl = "api/system/messagetemplatetype";

        [TestMethod]
        public async Task GetMessageBodyTemplateSucceed()
        {
            //Arrange
            
            //Act
            var result = await AssertGetStatusCode<List<SystemMessageTemplateType>>(client, $"{apiUrl}", HttpStatusCode.OK);
            var resultByClassTypeID = await AssertGetStatusCode<List<SystemMessageTemplateType>>(client, $"{apiUrl}?AppliesToClasstypeID=10000", HttpStatusCode.OK);
            var resultByChannelType = await AssertGetStatusCode<List<SystemMessageTemplateType>>(client, $"{apiUrl}?ChannelType=4", HttpStatusCode.OK);

            //Assert
            result.Count.Should().Be(26);
            resultByClassTypeID.Count.Should().Be(9);
            resultByChannelType.Count.Should().Be(26);

        }

        [TestMethod]
        public async Task GetMessageBodyTemplateSimpleListSucceed()
        {
            //Arrange

            //Act
            var result = await AssertGetStatusCode<List<SimpleListItem>>(client, $"{apiUrl}/simplelist", HttpStatusCode.OK);
            var resultByClassTypeID = await AssertGetStatusCode<List<SimpleListItem>>(client, $"{apiUrl}/simplelist?AppliesToClasstypeID=10000", HttpStatusCode.OK);
            var resultByChannelType = await AssertGetStatusCode<List<SimpleListItem>>(client, $"{apiUrl}/simplelist?ChannelType=4", HttpStatusCode.OK);

            //Assert
            result.Count.Should().Be(26);
            resultByClassTypeID.Count.Should().Be(9);
            resultByChannelType.Count.Should().Be(26);

        }
    }
}
