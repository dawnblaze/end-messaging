﻿using Endor.Comm.ApiModels;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Exceptions;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.FunctionalTests.DtoBuilders;
using Endor.Comm.IntegrationTests.Specifications;
using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.Comm.FunctionalTests.ControllerTests
{
    [TestClass]
    public class MessageBodyTemplateControllerTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/message/bodytemplate";

        private async Task<MessageBodyTemplate> SimpleTestMessageBodyTemplate()
        {
            var messageBodyTemplate = new MessageBodyTemplateBuilder()
                .WithDefaultValues(GetDapperRepository())
                .Build();
            await repository.AddAsync<MessageBodyTemplate>(messageBodyTemplate);
            
            return messageBodyTemplate;
        }

        [TestMethod]
        public async Task GetMessageBodyTemplateSucceed()
        {
            //Arrange
            var messageBodyTemplate = await SimpleTestMessageBodyTemplate();

            //Act
            var result = await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{messageBodyTemplate.ID}", HttpStatusCode.OK);
            var result2 = await AssertGetStatusCode<MessageBodyTemplateDto[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            var result3 = await AssertGetStatusCode<MessageBodyTemplateDto[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            var result4 = await AssertGetStatusCode<MessageBodyTemplateDto[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);

            //Assert
            result.ID.Should().Be((short)messageBodyTemplate.ID);
            result2.Count().Should().BeGreaterThan(0);
            result3.Count().Should().BeGreaterThan(0);
            result4.Count().Should().BeGreaterOrEqualTo(0);
        }
        
        [TestMethod]
        public async Task CreateMessageBodyTemplateSucceed()
        {
            var newModel = new MessageBodyTemplateDtoBuilder().WithDefaultValues(GetDapperRepository()).Build();

            //Act
            var result = await AssertPostStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            //Assert
            result.ID.Should().BeGreaterThan(0);
            result.Name.Should().Be(newModel.Name);
            result.Description.Should().Be(newModel.Description);
            result.LocationID.Should().Be(newModel.LocationID);
            result.CompanyID.Should().Be(newModel.CompanyID);
            result.EmployeeID.Should().Be(newModel.EmployeeID);
            result.Subject.Should().Be(newModel.Subject);
            result.Body.Should().Be(newModel.Body);

            //CleanUp
            await AssertDeleteStatusCode(client, $"{apiUrl}/{result.ID}", HttpStatusCode.OK);
        }
        
        [TestMethod]
        public async Task UpdateMessageBodyTemplateSucceed()
        {
            //Arrange
            var messageBodyTemplate = await SimpleTestMessageBodyTemplate();

            //Act
            var result = await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{messageBodyTemplate.ID}", HttpStatusCode.OK);

            result.Name = "New Updated Message Body Template Name";

            await AssertPutStatusCode(client, $"{apiUrl}/{messageBodyTemplate.ID}", JsonConvert.SerializeObject(result), HttpStatusCode.OK);
            var updatedsResult = await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{messageBodyTemplate.ID}", HttpStatusCode.OK);

            //Assert
            messageBodyTemplate.Name.Should().NotBe(updatedsResult.Name);
            updatedsResult.Name.Should().Be(result.Name);

            repository.Refresh(messageBodyTemplate);
        }

        [TestMethod]
        public async Task DeleteMessageBodyTemplateSucceed()
        {
            //Arrange
            var messageBodyTemplate = await SimpleTestMessageBodyTemplate();

            //Act
            await AssertDeleteStatusCode(client, $"{apiUrl}/{messageBodyTemplate.ID}", HttpStatusCode.OK);

            repository.Refresh(messageBodyTemplate);

            //Assert
            await Assert.ThrowsExceptionAsync<EntityNotFoundException>(async () => await repository.GetByShortIdAsync<MessageBodyTemplate>(-100));
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateSetActiveInactive()
        {
            //Arrange
            var template = await SimpleTestMessageBodyTemplate();

            //Act/Assert

            // set inactive
            await AssertPostStatusCode(client, $"{apiUrl}/{template.ID}/Action/SetInactive", null, HttpStatusCode.OK);
            
            var adHocTemplateTest = await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{template.ID}", HttpStatusCode.OK);
            adHocTemplateTest.IsActive.Should().BeFalse();

            // set active
            await AssertPostStatusCode(client, $"{apiUrl}/{template.ID}/Action/SetActive", null, HttpStatusCode.OK);
            
            // confirm adhoc is now active again
            adHocTemplateTest = await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{template.ID}", HttpStatusCode.OK);
            adHocTemplateTest.IsActive.Should().BeTrue();
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateCanDelete()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var newModel = new MessageBodyTemplateBuilder()
              .WithDefaultValues(dapperRepo)
              .Build();

            //Act
            var adHocTemplate = await AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);
            var statusResp = await AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{adHocTemplate.ID}/Action/CanDelete", HttpStatusCode.OK);

            //Assert
            statusResp.Value.Should().BeTrue();

            //CleanUp
            await AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateMoveBefore()
        {
            //Arrange
            var template = await SimpleTestMessageBodyTemplate();
            var newModel = new MessageBodyTemplateDtoBuilder().WithDefaultValues(GetDapperRepository()).Build();

            //Act
            var adHocTemplateTarget = await AssertPostStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            // Test Move Before
            await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{adHocTemplateTarget.ID}/Action/MoveBefore/{template.ID}", HttpStatusCode.OK);

            var result = await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{adHocTemplateTarget.ID}", HttpStatusCode.OK);
            result.SortIndex.Should().Be(-99);

            await AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplateTarget.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateMoveAfter()
        {
            //Arrange
            var template = await SimpleTestMessageBodyTemplate();
            var newModel = new MessageBodyTemplateDtoBuilder().WithDefaultValues(GetDapperRepository()).Build();

            //Act
            var adHocTemplateTarget = await AssertPostStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            // Test Move Before
            await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{adHocTemplateTarget.ID}/Action/MoveAfter/{template.ID}", HttpStatusCode.OK);

            var result = await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{adHocTemplateTarget.ID}", HttpStatusCode.OK);
            result.SortIndex.Should().Be(-98);

            await AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplateTarget.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateParticipantsList()
        {
            var newModel = new MessageBodyTemplateDtoBuilder().WithDefaultValues(GetDapperRepository()).Build();
            newModel.Participants = new List<MessageParticipantInfoDto>()
            {
                new MessageParticipantInfoDtoBuilder().WithDefaultValues(null).Build()
            };

            var createTemplate = await AssertPostStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);
            Assert.IsNotNull(createTemplate);
            Assert.IsNotNull(createTemplate.Participants);
            Assert.IsTrue(createTemplate.Participants.Count > 0);

            var result = await AssertGetStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{createTemplate.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Participants);
            Assert.IsTrue(result.Participants.Count > 0);

            result.Participants = null;
            result = await AssertPutStatusCode<MessageBodyTemplateDto>(client, $"{apiUrl}/{createTemplate.ID}", result, HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Participants);
            Assert.IsFalse(result.Participants.Count > 0);

            await AssertDeleteStatusCode(client, $"{apiUrl}/{createTemplate.ID}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateParticipantsListReturnsError()
        {
            var newModel = new MessageBodyTemplateDtoBuilder().WithDefaultValues(GetDapperRepository()).Build();
            newModel.Participants = new List<MessageParticipantInfoDto>()
            {
                new MessageParticipantInfoDtoBuilder().WithDefaultValues(null).Build()
            };
            newModel.Participants.FirstOrDefault().ContactID = -999;
            newModel.Participants.FirstOrDefault().TeamID = -999;
            newModel.Participants.FirstOrDefault().EmployeeID = -999;

            var response = await AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.NotFound);
            string responseString = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(responseString);
            responseString.Contains("Contact with ID `-999` not found. Employee with ID `-999` not found. EmployeeTeam with ID `-999` not found. ").Should().BeTrue();
        }

    }
}
