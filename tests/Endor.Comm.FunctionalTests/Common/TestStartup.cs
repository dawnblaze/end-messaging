﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Acheve.AspNetCore.TestHost.Security;
using Acheve.TestHost;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Endor.Comm.IntegrationTests;
using Endor.Tenant;
using Endor.Tasks;
using Endor.Logging.Client;
using Endor.RTM;

namespace Endor.Comm.FunctionalTests
{
    public class TestStartup : BaseStartup
    {
        public TestStartup(IWebHostEnvironment env) : base(env)
        {
        }

        public override IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            var configBuilder = base.GetConfigurationBuilder(env);
            configBuilder.AddInMemoryCollection(MockTenantDataCache.GetLocalSettings());
            return configBuilder;
        }

        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = TestServerDefaults.AuthenticationScheme;
            })
            .AddTestServer();
        }

        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            app.UseAuthentication();
            base.Configure(app, env, loggerFactory, appLifetime);
        }

        public override void ConfigureExternalEndorServices(IServiceCollection services)
        {
            var tdc = GetMockTenantDataCache();
            services.AddSingleton<ITenantDataCache>(tdc);
            services.AddSingleton<ITaskQueuer>(new MockTaskQueuer(tdc));//>(new HttpTaskQueuer(Configuration["Endor:TasksAPIURL"]));
            services.AddSingleton<RemoteLogger>((x) => new RemoteLogger(tdc)); //new RemoteLogger(tdc));
            services.AddTransient<IRTMPushClient>((x) => new MockRealTimeMessagingPushClient());

            //services.AddSingleton<IMigrationHelper, MigrationHelper>();
        }

        private MockTenantDataCache GetMockTenantDataCache()
        {
            var tdc = new MockTenantDataCache();
            return tdc;
        }
    }
}
