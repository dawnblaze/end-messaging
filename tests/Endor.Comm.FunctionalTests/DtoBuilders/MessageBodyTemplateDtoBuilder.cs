﻿using Endor.Comm.ApiModels;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.IntegrationTests.Specifications;
using Endor.Comm.UnitTests;
using System;
using System.Collections.Generic;
using System.Linq;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.FunctionalTests.DtoBuilders
{
    public class MessageDtoBuilder : CommonControllerTestClass
    {
        private MessageDto _messageDto = new MessageDto();

        public MessageDtoBuilder Id(int id)
        {
            _messageDto.ID = id;
            return this;
        }

        public MessageDtoBuilder MessageParticipantInfoId(int id)
        {
            _messageDto.ParticipantID = id;
            return this;
        }

        public MessageDtoBuilder AddMessageParticipantInfo(MessageParticipantInfo participantInfo)
        {
            _messageDto.Participants.Add(participantInfo);
            return this;
        }

        public MessageDtoBuilder MessageBodyId(int id)
        {
            _messageDto.BodyID = id;
            return this;
        }

        public MessageDtoBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            short employeeID = 0;
            if (repository != null)
            {
                repository.SetTableNameForClassType((new EmployeeData()).ClassTypeID);
                employeeID = (repository.List<EmployeeData>(new GetValidIDByBIDSpecification<EmployeeData>(BID))).First().IDAsShort;
            }
            _messageDto = new MessageDto()
            {
                ReceivedDT = DateTime.UtcNow,
                Channels = MessageChannelType.None,
                Subject = "The Quick",
                HasBody = true,
                Body = "<b>brown fox</>",
                Participants = new List<MessageParticipantInfo>(),
                EmployeeID = employeeID
            };
            return this;
        }

        public MessageDto Build() => _messageDto;
    }
}
