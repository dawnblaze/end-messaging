﻿using Endor.Comm.ApiModels;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.IntegrationTests.Specifications;
using Endor.Comm.UnitTests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.FunctionalTests.DtoBuilders
{
    public class DomainEmailDtoBuilder : CommonControllerTestClass
    {
        private DomainEmailDto _domainEmail = new DomainEmailDto();

        public DomainEmailDtoBuilder Id(short id)
        {
            _domainEmail.ID = id;
            return this;
        }

        private string GetTestDomainName()
        {
            var tickString = DateTime.UtcNow.Ticks.ToString();
            return "unitest." + tickString.Substring(tickString.Length - 7, 4) + "." + tickString.Substring(tickString.Length - 3);
        }

        public DomainEmailDtoBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            short locationID = 0;
            if (repository != null)
            {
                repository.SetTableNameForClassType((new LocationData()).ClassTypeID);
                locationID = (repository.List<LocationData>(new GetValidIDByBIDSpecification<LocationData>(BID))).First().IDAsShort;
            }
            _domainEmail = new DomainEmailDto()
            {
                ID = -100,
                IsActive = true,
                //DomainName = GetTestDomainName(),
                ProviderType = EmailProviderType.CustomSMTP,
                CreatedDate = DateTime.Now,
                DomainName = "corebridge.net",
                SMTPAddress = "smtp.gmail.com",
                SMTPPort = 465,
                SMTPSecurityType = EmailSecurityType.SSL,
                SMTPAuthenticateFirst = true
            };
            SimpleListItem simpleLocation = new SimpleListItem()
            {
                BID = BID,
                ID = locationID
            };
            _domainEmail.SimpleLocations = new List<SimpleListItem>() { simpleLocation };
            return this;
        }

        public DomainEmailDto Build() => _domainEmail;
    }
}
