﻿using Endor.Comm.ApiModels;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.IntegrationTests.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class EmailAccountDtoBuilder
    {
        private EmailAccountDto _emailAccount = new EmailAccountDto();

        public EmailAccountDtoBuilder()
        {
        }

        public EmailAccountDtoBuilder Id(int id)
        {
            _emailAccount.ID = id;
            return this;
        }

        public EmailAccountDtoBuilder DomainEmailId(short id)
        {
            _emailAccount.DomainEmailID = id;
            return this;
        }

        public EmailAccountDtoBuilder AddEmployeeTeamLink(int employeeTeamID)
        {
            if (_emailAccount.EmailAccountTeamLinks == null)
                _emailAccount.EmailAccountTeamLinks = new List<EmailAccountTeamLinkDto>();
            _emailAccount.EmailAccountTeamLinks.Add(new EmailAccountTeamLinkDto()
            {
                TeamID = employeeTeamID,
                EmailAccountID = (short)_emailAccount.ID
            });
            return this;
        }

        private string GetTestDomainName()
        {
            var tickString = DateTime.UtcNow.Ticks.ToString();
            return "unitest." + tickString.Substring(tickString.Length - 7, 4) + "." + tickString.Substring(tickString.Length - 3);
        }

        public EmailAccountDtoBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            short employeeID = 0;

            if (repository != null)
            {
                repository.SetTableNameForClassType((new EmployeeData()).ClassTypeID);
                employeeID = (repository.List<EmployeeData>(new GetValidIDByBIDSpecification<EmployeeData>(BID))).First().IDAsShort;

            }

            _emailAccount = new EmailAccountDto()
            {
                ID = -100,
                IsActive = true,
                StatusType = EmailAccountStatus.Authorized,
                UserName = GetTestDomainName(),
                EmployeeID = employeeID,
                DomainName = "corebridge.net",
                DomainEmailID = 0,
                IsPrivate = true,
                NewPassword = "teste",
                EmailAddress = "test@test.com",
                AliasUserNames = "test",
                DisplayName = "test",
            };

            return this;
        }

        public EmailAccountDto Build() => _emailAccount;
    }
}
