﻿using Endor.Comm.ApiModels;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.IntegrationTests.Specifications;
using Endor.Comm.UnitTests;
using System;
using System.Collections.Generic;
using System.Linq;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.FunctionalTests.DtoBuilders
{
    public class MessageBodyTemplateDtoBuilder : CommonControllerTestClass
    {
        private MessageBodyTemplateDto _messageBodyTemplate = new MessageBodyTemplateDto();

        public MessageBodyTemplateDtoBuilder Id(short id)
        {
            _messageBodyTemplate.ID = id;
            return this;
        }

        public MessageBodyTemplateDtoBuilder SortIndex(short sortIndex)
        {
            _messageBodyTemplate.SortIndex = sortIndex;
            return this;
        }


        public MessageBodyTemplateDtoBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            byte locationID = 0;
            short employeeID = 0;
            int companyID = 0;
            var newMsgBodyTemplate = DateTime.UtcNow + " MESSAGE BODY TEMPLATE";

            if (repository != null)
            {
                repository.SetTableNameForClassType((new EmployeeData()).ClassTypeID);
                employeeID = (repository.List<EmployeeData>(new GetValidIDByBIDSpecification<EmployeeData>(BID))).First().IDAsShort;
                repository.SetTableNameForClassType((new LocationData()).ClassTypeID);
                locationID = (repository.List<LocationData>(new GetValidIDByBIDSpecification<LocationData>(BID))).First().IDAsByte;
                repository.SetTableNameForClassType((new CompanyData()).ClassTypeID);
                companyID = (repository.List<CompanyData>(new GetValidIDByBIDSpecification<CompanyData>(BID))).First().ID;
            }

            _messageBodyTemplate = new MessageBodyTemplateDto()
            {
                ID = -100,
                IsActive = true,
                AppliesToClassTypeID = 5000,
                MessageTemplateType = 0,
                ChannelType = MessageChannelType.Email,
                LocationID = locationID,
                CompanyID = companyID,
                EmployeeID = employeeID,
                Name = newMsgBodyTemplate,
                Description = $"{newMsgBodyTemplate} with MergeFields",
                Subject = "Test Mail {{Order.OrderNumber}}{{Order.ItemNumber}}",
                Body = "This is just a test {{Order.ordernumber}}{{Order.TransactionType}}",
                AttachedFileCount = 0,
                AttachedFileNames = "none",
                SizeInKB = 0,
                SortIndex = 1

            };
            return this;
        }

        public MessageBodyTemplateDto Build() => _messageBodyTemplate;
    }
}
