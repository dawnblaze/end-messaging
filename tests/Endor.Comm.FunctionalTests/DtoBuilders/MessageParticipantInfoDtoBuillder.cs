﻿using Endor.Comm.ApiModels;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.IntegrationTests.Specifications;
using Endor.Comm.UnitTests;
using System;
using System.Collections.Generic;
using System.Linq;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.FunctionalTests.DtoBuilders
{
    public class MessageParticipantInfoDtoBuilder : CommonControllerTestClass
    {
        private MessageParticipantInfoDto _messageParticipantInfoDto = new MessageParticipantInfoDto();

        public MessageParticipantInfoDtoBuilder Id(short id)
        {
            _messageParticipantInfoDto.ID = id;
            return this;
        }

        public MessageParticipantInfoDtoBuilder ParticipantRoleType(MessageParticipantRoleType type)
        {
            _messageParticipantInfoDto.ParticipantRoleType = type;
            return this;
        }

        public MessageParticipantInfoDtoBuilder MessageBodyId(int id)
        {
            _messageParticipantInfoDto.BodyID = id;
            return this;
        }

        public MessageParticipantInfoDtoBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            _messageParticipantInfoDto = new MessageParticipantInfoDto()
            {
                ID = -100,
                ModifiedDT = DateTime.Now,
                ParticipantRoleType = MessageParticipantRoleType.To,
                Channel = MessageChannelType.Email,
                UserName = "Test",
                IsMergeField = false,
                DeliveredDT = DateTime.Now,
                IsDelivered = false
            };
            return this;
        }

        public MessageParticipantInfoDto Build() => _messageParticipantInfoDto;
    }
}
