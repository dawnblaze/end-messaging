﻿using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.IntegrationTests.Specifications
{
    public class GetFirstValidAuthUserIDSpecification : BaseSpecification<UserLink>
    {
        public GetFirstValidAuthUserIDSpecification(short bid)
            : base()
        {
            AddCriteria(new GetFirstValidAuthUserIDCriteria(bid));
            ApplyPaging(0, 1);
        }
    }

    public class GetFirstValidAuthUserIDCriteria : Criteria<UserLink>
    {
        private readonly short _bid;
        public GetFirstValidAuthUserIDCriteria(short bid)
        {
            _bid = bid;
        }
        public override Expression<Func<UserLink, bool>> ToExpression()
        {
            return ul => ul.BID == _bid && ul.EmployeeID != null && ul.AuthUserID != null;
        }
    }
}
