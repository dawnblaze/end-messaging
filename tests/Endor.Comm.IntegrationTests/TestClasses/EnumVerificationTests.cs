﻿using Endor.Comm.Core.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.IntegrationTests
{
    [TestClass]
    public class MessagingEnumVerificationTests : MessagingEfRepoTestFixture
    {
        [TestMethod]
        public void ValidateMessageParticipantRoleTypeEnum()
        {
            EnumVerification<EnumMessageParticipantRoleType, MessageParticipantRoleType>();
        }

        [TestMethod]
        public void ValidateMessageChannelTypeEnum()
        {
            EnumVerification<EnumMessageChannelType, MessageChannelType>();
        }

        [TestMethod]
        public void ValidateEmailProviderTypeEnum()
        {
            EnumVerification<EnumEmailProviderType, EmailProviderType>();
        }

        [TestMethod]
        public void ValidateEmailSecurityTypeEnum()
        {
            EnumVerification<EnumEmailSecurityType, EmailSecurityType>();
        }
    }
}
