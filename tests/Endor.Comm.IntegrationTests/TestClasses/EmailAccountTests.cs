﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Factories.Classes;
using Endor.Comm.Core.Services.Classes;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Endor.Comm.IntegrationTests.TestClasses
{
    public class SmtpConfig
    {
        public string DomainName { get; set; }
        public string SMTPAddress { get; set; }
        public short SMTPPort { get; set; }
        public EmailSecurityType SMTPSecurityType { get; set; }
    }
    [TestClass]
    public class EmailAccountTests : MessagingEfRepoTestFixture
    {
        IFixture _fixture;
        public EmailAccountTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });
        }

        private async Task SendSmtpEmail(SmtpConfig smtpConfig, string email, string subject, string content, string fromEmail = "scotts@corebridge.net")
        {
            //Arrange
            var passphrase = "12345";
            var toEmail = email;
            var toName = "Test Email";

            var dapperRepo = GetDapperRepository();
            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(dapperRepo).Build();

            var password = StringEncryptor.Encrypt("dbvdssbjniricrqi", passphrase);
            emailAccount.Credentials = "{ 'password': '" + password + "' }";

            var domainEmail = new DomainEmailBuilder().WithDefaultValues(dapperRepo).Build();
            domainEmail.DomainName = "gmail";
            await repository.AddAsync<DomainEmail>(domainEmail);

            emailAccount.DomainEmail = domainEmail;
            await repository.AddAsync<EmailAccountData>(emailAccount);

            var emailProviderFactory = new EmailProviderFactory();
            var emailProvider = emailProviderFactory.Create(EmailProviderType.CustomSMTP, emailAccount.DomainEmail);

            var emailMessage = new EmailMessageBuilder().WithDefaultValues().Build();
            emailMessage.Subject = subject;
            emailMessage.Content = content;
            emailMessage.ToAddresses.Add(new EmailAddress(toName, toEmail));
            emailMessage.FromAddresses.Add(new EmailAddress("Test From", fromEmail));
            emailMessage.EmailAccountEmailAddress = "scotts@corebridge.net";

            //Act
            var result = await repository.GetByShortIdAsync<EmailAccountData>(-100);
            var emailResult = await result.SendEmail(emailMessage, emailProvider, passphrase);

            //Assert
            emailResult.Success.Should().Be(true);
        }

        [TestMethod]
        [DataRow("Test Sending Email From Supplied", "Test Sending Email From Supplied")]
        public async Task SendEmailToGmailEmail_FromEmailSpecified(string subject, string content)
        {
            var email = MockTenantDataCache.GetLocalSettings()["EmailSendTo"];
            var smtpConfig = new SmtpConfig()
            {
                DomainName = "corebridge.net",
                SMTPAddress = "smtp.gmail.com",
                SMTPPort = 465,
                SMTPSecurityType = EmailSecurityType.SSL,
            };
            await SendSmtpEmail(smtpConfig, email, subject, content, "juliusbacosa@outlook.com");
        }


        [TestMethod]
        [DataRow("Test Sending Email Through Gmail", "Test Sending Email Through Gmail")]
        public async Task SendEmailThroughGmailEmail(string subject, string content)
        {
            var email = MockTenantDataCache.GetLocalSettings()["EmailSendTo"];
            var smtpConfig = new SmtpConfig()
            {
                DomainName = "corebridge.net",
                SMTPAddress = "smtp.gmail.com",
                SMTPPort = 465,
                SMTPSecurityType = EmailSecurityType.SSL,
            };
            await SendSmtpEmail(smtpConfig, email, subject, content);
        }

        [TestMethod]
        [DataRow("[1] Test Sending Email Through Gmail", "[1] Test Sending Email Through Gmail")]
        [DataRow("[2] Test Sending Email Through Gmail", "[2] Test Sending Email Through Gmail")]
        [DataRow("[3] Test Sending Email Through Gmail", "[3] Test Sending Email Through Gmail")]
        [DataRow("[4] Test Sending Email Through Gmail", "[4] Test Sending Email Through Gmail")]
        [DataRow("[5] Test Sending Email Through Gmail", "[5] Test Sending Email Through Gmail")]
        public async Task Send5EmailsThroughGmailEmailBacktoBack(string subject, string content)
        {
            var email = MockTenantDataCache.GetLocalSettings()["EmailSendTo"];
            var smtpConfig = new SmtpConfig()
            {
                DomainName = "corebridge.net",
                SMTPAddress = "smtp.gmail.com",
                SMTPPort = 465,
                SMTPSecurityType = EmailSecurityType.SSL,
            };
            await SendSmtpEmail(smtpConfig, email, subject, content);
        }

        [TestMethod]
        [DataRow("Test Sending Email Through Gmail", "Test Sending Email Through Gmail")]
        public async Task SendEmailThroughOffice365Email(string subject, string content)
        {
            var email = MockTenantDataCache.GetLocalSettings()["EmailSendTo"];
            var smtpConfig = new SmtpConfig()
            {
                DomainName = "corebridge.net",
                SMTPAddress = "smtp.office365.com",
                SMTPPort = 587,
                SMTPSecurityType = EmailSecurityType.TLS,
            };
            await SendSmtpEmail(smtpConfig, email, subject, content);
        }

        [TestMethod]
        [DataRow("[1] Test Sending Email Through Office365", "[1] Test Sending Email Through Office365")]
        [DataRow("[2] Test Sending Email Through Office365", "[2] Test Sending Email Through Office365")]
        [DataRow("[3] Test Sending Email Through Office365", "[3] Test Sending Email Through Office365")]
        [DataRow("[4] Test Sending Email Through Office365", "[4] Test Sending Email Through Office365")]
        [DataRow("[5] Test Sending Email Through Office365", "[5] Test Sending Email Through Office365")]
        public async Task Send5EmailsThroughOffice365EmailBacktoBack(string subject, string content)
        {
            var email = MockTenantDataCache.GetLocalSettings()["EmailSendTo"];
            var smtpConfig = new SmtpConfig()
            {
                DomainName = "corebridge.net",
                SMTPAddress = "smtp.office365.com",
                SMTPPort = 587,
                SMTPSecurityType = EmailSecurityType.TLS,
            };
            await SendSmtpEmail(smtpConfig, email, subject, content);
        }


    }
}
