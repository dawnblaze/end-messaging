﻿using Endor.Comm.Core.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.IntegrationTests
{
    public class MessagingEfRepoTestFixture : BaseEfRepoTestFixture
    {
        [TestCleanup]
        public override async Task Cleanup()
        {
            dbContext.RemoveRange(dbContext.MessageBody.Where(x => x.ID < 0));
            dbContext.RemoveRange(dbContext.MessageDeliveryRecord.Where(x => x.ID < 0));
            dbContext.RemoveRange(dbContext.MessageObjectLink.Where(x => x.ID < 0));
            dbContext.RemoveRange(dbContext.MessageParticipantInfo.Where(x => x.ID < 0));
            dbContext.RemoveRange(dbContext.MessageHeader.Where(x => x.ID < 0 || x.ID == int.MaxValue));
            dbContext.RemoveRange(dbContext.MessageBodyTemplate.Where(x => x.IDAsShort < 0 || x.IDAsShort == short.MaxValue));
            dbContext.RemoveRange(dbContext.EmailAccountTeamLink.Where(x => x.EmailAccountID < 0 || x.EmailAccountID == short.MaxValue || x.EmailAccount.DomainEmailID < 0));
            dbContext.RemoveRange(dbContext.EmailAccountData.Where(x => x.IDAsShort < 0 || x.IDAsShort == short.MaxValue || x.DomainEmailID < 0));
            dbContext.RemoveRange(dbContext.DomainEmailLocationLink.Where(x => x.DomainID < 0));
            await dbContext.SaveChangesAsync();

            //var entries = dbContext.EmailAccountData.Where(x => x.IDAsShort < 0 || x.IDAsShort == short.MaxValue || x.DomainEmailID < 0);
            //dbContext.ChangeTracker.Entries().ToList().ForEach(x => dbContext.Remove(x));

            dbContext.RemoveRange(dbContext.DomainEmail.Where(x => x.IDAsShort < 0 || x.IDAsShort == short.MaxValue));

            await dbContext.SaveChangesAsync();
        }
    }
}
