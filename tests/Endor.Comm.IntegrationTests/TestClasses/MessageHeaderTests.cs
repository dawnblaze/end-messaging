using Endor.Comm.Core.Entities;
using Endor.Comm.UnitTests.ObjectBuilders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using FluentAssertions;
using static Endor.Comm.UnitTests.UnitTestHelpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Endor.Comm.IntegrationTests
{
    [TestClass]
    public class MessageHeaderTests : MessagingEfRepoTestFixture
    {

        [TestMethod]
        public async Task LoadingMessageHeaderShouldIncludeChildEntities()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Build();
            await repository.AddAsync<MessageBody>(messageBody);
            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageHeader>(messageHeader);
            //Act
            var result = await repository.GetByIdAsync<MessageHeader>(-100);

            //Assert
            result.Should().NotBeNull();
            result.MessageBody.Should().NotBeNull();
        }

        [TestMethod]
        public async Task MessageHeaderShouldBeAddedToRespository()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Build();
            await repository.AddAsync<MessageBody>(messageBody);
            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Build();
            //Act
            await repository.AddAsync<MessageHeader>(messageHeader);

            //Assert
            var result = await repository.GetByIdAsync<MessageHeader>(-100);
            result.Should().NotBeNull();
            result.MessageBody.Should().NotBeNull();
        }

        [TestMethod]
        public async Task MessageHeaderUpdatesAfterAddingIt()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Build();
            await repository.AddAsync<MessageBody>(messageBody);
            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageHeader>(messageHeader);
            
            //Act
            dbContext.Entry(messageHeader).State = EntityState.Detached;

            // fetch the item and update its title
            var newItem = await repository.GetByIdAsync<MessageHeader>(-100);
            newItem.Should().NotBeNull();
            messageHeader.Should().NotBeSameAs(newItem);
            var newTitle = Guid.NewGuid().ToString();
            newItem.MarkAsRead();


            await repository.UpdateAsync(newItem);
            
            //Assert
            var result = await repository.GetByIdAsync<MessageHeader>(-100);
            result.Should().NotBeNull();
            result.IsRead.Should().BeTrue();
            result.MessageBody.Should().NotBeNull();
        }

        [TestMethod]
        public async Task DeleteMessageHeaderAfterAddingIt()
        {
            //Arrange
            var dapperRepo = GetDapperRepository();
            var messageBody = new MessageBodyBuilder()
                .WithDefaultValues().Build();
            await repository.AddAsync<MessageBody>(messageBody);
            var messageParticipantInfo = new MessageParticipantInfoBuilder()
                .WithDefaultValues()
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageParticipantInfo>(messageParticipantInfo);
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(dapperRepo)
                .MessageParticipantInfoId(messageParticipantInfo.ID)
                .MessageBodyId(messageBody.ID)
                .Build();
            await repository.AddAsync<MessageHeader>(messageHeader);

            //Act
            await repository.DeleteAsync(messageHeader);

            //Assert
            try
            {
                var result = await repository.GetByIdAsync<MessageHeader>(-100);
                Assert.Fail(); //An exception should have been thrown
            }
            catch (Exception)
            {
                
            }
        }

    }
}
