﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Dapper;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Infrastructure.Common;
using Endor.Comm.UnitTests;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Endor.Comm.UnitTests.UnitTestHelpers;
using System.Linq.Expressions;

namespace Endor.Comm.IntegrationTests
{
    
    public class TestDapperRepository : DapperRepository, IDapperReadOnlyRepository
    {
        public override IDbConnection Connection()
        {
            var tenantCache = new MockTenantDataCache();
            return new SqlConnection(tenantCache.Get(_BID).Result.BusinessDBConnectionString);
        }

        public TestDapperRepository()
            : base (BID,"","")
        {
            _BID = BID;
        }

        internal virtual dynamic Mapping<T>(T item)
            where T : BaseEntity
        {
            return item;
        }

        public async Task<T> AddAsync<T>(T item) where T : BaseEntity
        {
            using (IDbConnection cn = Connection())
            {
                var parameters = (object)Mapping(item);
                cn.Open();
                cn.Insert<int>(_tableName, parameters);
            }
            return await Task.FromResult(item);
        }

        public async Task<int> UpdateAsync<T>(T item) where T : BaseIdentityEntity
        {
            using (IDbConnection cn = Connection())
            {
                var updateQuery = GenerateUpdateQuery<T>();

                cn.Open();
                await cn.ExecuteAsync(updateQuery, item);
            }
            return 0;
        }

        public async Task<int> UpdateListAsync<T>(List<T> items) where T : BaseIdentityEntity
        {
            foreach (var item in items)
            {
                await this.UpdateAsync(item);
            }
            return 0;
        }

        private string GenerateUpdateQuery<T>() where T : class
        {
            var updateQuery = new StringBuilder($"UPDATE {_tableName} SET ");
            var GetProperties = typeof(T).GetProperties();
            var properties = GenerateListOfProperties(GetProperties);

            properties.ForEach(property =>
            {
                if (!((property.Equals("ID"))||(property.Equals("BID"))))
                {
                    updateQuery.Append($"{property}=@{property},");
                }
            });
            
            updateQuery.Remove(updateQuery.Length - 1, 1); //remove last comma
            updateQuery.Append(" WHERE BID=@BID AND ID=@ID");

            return updateQuery.ToString();
        }

        private static List<string> GenerateListOfProperties(IEnumerable<PropertyInfo> listOfProperties)
        {
            return (from prop in listOfProperties
                    let attributes = prop.GetCustomAttributes(typeof(DescriptionAttribute), false)
                    where attributes.Length <= 0 || (attributes[0] as DescriptionAttribute)?.Description != "ignore"
                    select prop.Name).ToList();
        }

        public async Task<int> DeleteAsync<T>(T item) where T : BaseEntity
        {
            using (IDbConnection cn = Connection())
            {
                var ID = (byte)(item.GetType().GetProperty("ID").GetValue(item, null));
                var BID = (byte)(item.GetType().GetProperty("ID").GetValue(item, null));
                cn.Open();
                cn.Execute("DELETE FROM " + _tableName + "  WHERE BID=@BID AND ID=@ID", new { ID = ID, BID = BID });
            }
            return await Task.FromResult(0);
        }

        public async Task<int> DeleteListAsync<T>(List<T> items) where T : BaseEntity
        {
            foreach(var item in items)
            {
                await this.DeleteAsync(item);
            }
            return await Task.FromResult(0);
        }

        public Task<int> RequestIDIntegerAsync(short bid, int classtypeID)
        {
            throw new NotImplementedException();
        }

        public async Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters)
        {
            var param = new DynamicParameters();
            foreach (var p in parameters)
            {
                if (p is IDataParameter)
                {
                    var DbP = (IDataParameter)p;
                    param.Add(DbP.ParameterName, DbP.Value);
                }
            }
            using (IDbConnection cn = Connection())
            {
                cn.Open();
                return await cn.ExecuteAsync(sql, param);
            }
        }

        public Task<T> CloneAsync<T>(int ID, Expression<Func<T, string>> nameSelector, Type IDtype) where T : BaseIdentityEntity
        {
            throw new NotImplementedException();
        }
    }
}
