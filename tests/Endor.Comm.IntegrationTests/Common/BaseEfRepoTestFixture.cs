﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Infrastructure.Data;
using Endor.Comm.Core.Entities;
using Endor.Common;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Endor.Comm.UnitTests.UnitTestHelpers;
using System.Text.RegularExpressions;

namespace Endor.Comm.IntegrationTests
{
    public abstract class BaseEfRepoTestFixture
    {
        protected AppDbContext dbContext;
        protected TestEfRepository repository;

        [TestInitialize]
        public async Task Initialize()
        {
            repository = GetEFRepository();
            await Cleanup();
        }

        public abstract Task Cleanup();

        protected TestEfRepository GetEFRepository()
        {
            var options = CreateNewContextOptions();
            var mockDispatcher = new Mock<IDomainEventDispatcher>();
            var httpContext = CreateDefaultHttpContext();
            dbContext = new AppDbContext(options, mockDispatcher.Object, httpContext, new MockTenantDataCache());
            return new TestEfRepository(dbContext, httpContext);
        }

        private HttpContextAccessor CreateDefaultHttpContext()
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                 new Claim(ClaimNameConstants.UserID, AuthUserID.ToString()),
                 new Claim(ClaimNameConstants.BID, BID.ToString()),
                 new Claim(ClaimNameConstants.AID, "1")
                 //new Claim(ClaimNameConstants.UserLinkID, UserLinkID.ToString()),
                 //new Claim(ClaimNameConstants.EmployeeID, TestConstants.AuthEmployeeID.ToString())
            }));
            var httpContext = new DefaultHttpContext() { User = user };
            var httpContextAccessor = new HttpContextAccessor();
            httpContextAccessor.HttpContext = httpContext;
            return httpContextAccessor;
        }

        protected IDapperReadOnlyRepository GetDapperRepository()
        {
            return new TestDapperRepository();
        }

        protected static DbContextOptions<AppDbContext> CreateNewContextOptions()
        {
            var tenantCache = new MockTenantDataCache();
            var optionBuilder = new DbContextOptionsBuilder<AppDbContext>()
              .UseSqlServer(tenantCache.Get(BID).Result.BusinessDBConnectionString);

            return optionBuilder.Options;
        }

        protected void EnumVerification<DBType,CSharpType>()
            where DBType : class
            where CSharpType : struct,IConvertible
        {
            List<EnumTestClass> EnumsFromDb = new List<EnumTestClass>();
            dbContext.Set<DBType>().ToList<DBType>().ForEach(x =>
            {
                var name = (string)(x.GetType().GetProperty("Name").GetValue(x, null));
                var ID = (byte)(x.GetType().GetProperty("ID").GetValue(x, null));
                var correctedName = name.Replace(" ", "");
                var pattern = "\\(.*?\\)";
                correctedName = Regex.Replace(correctedName, pattern, String.Empty);
                Assert.IsTrue(Enum.IsDefined(typeof(CSharpType), correctedName),"Mismatch between Enum in project and values in Db");
                EnumsFromDb.Add(new EnumTestClass()
                {
                    ID = ID,
                    Name = name
                }); ;
            });
            EnumsFromDb.ForEach(x =>
            {
                CSharpType val;
                var correctedName = x.Name.Replace(" ", "");
                var pattern = "\\(.*?\\)";
                correctedName = Regex.Replace(correctedName, pattern, String.Empty);
                Assert.IsTrue(Enum.TryParse<CSharpType>(correctedName, true, out val), "Mismatch between Enum in project and values in Db");
            });
        }
    }

    class EnumTestClass
    {
        public byte ID;
        public string Name;
    }
}
