﻿using Endor.Comm.Infrastructure.Data;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.IntegrationTests
{
    public class TestEfRepository : EfRepository
    {
        public TestEfRepository(AppDbContext dbContext, IHttpContextAccessor httpContextAccessor)
            : base (dbContext,httpContextAccessor,true)
        {
        }

        public void Refresh(object entity)
        {
            _dbContext.Entry(entity).Reload();
        }
    }
}
