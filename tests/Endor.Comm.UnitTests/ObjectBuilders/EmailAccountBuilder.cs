﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.IntegrationTests.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class EmailAccountBuilder
    {
        private EmailAccountData _emailAccount = new EmailAccountData();

        public EmailAccountBuilder()
        {
        }

        public EmailAccountBuilder Id(int id)
        {
            _emailAccount.ID = id;
            return this;
        }

        public EmailAccountBuilder DomainEmailId(short id)
        {
            _emailAccount.DomainEmailID = id;
            return this;
        }

        private string GetTestDomainName()
        {
            var tickString = DateTime.UtcNow.Ticks.ToString();
            return "unitest." + tickString.Substring(tickString.Length - 7, 4) + "." + tickString.Substring(tickString.Length - 3);
        }

        public EmailAccountBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            int teamID = 0;
            short employeeID = 0;
            if (repository != null)
            {
                repository.SetTableNameForClassType((new EmployeeData()).ClassTypeID);
                employeeID = (repository.List<EmployeeData>(new GetValidIDByBIDSpecification<EmployeeData>(BID))).First().IDAsShort;
                repository.SetTableNameForClassType((new EmployeeTeam()).ClassTypeID);
                teamID = (repository.List<EmployeeTeam>(new GetValidIDByBIDSpecification<EmployeeTeam>(BID))).First().ID;
            }
            _emailAccount = new EmailAccountData()
            {
                BID = BID,
                ID = -100,
                IsActive = true,
                StatusType = EmailAccountStatus.Authorized,
                UserName = GetTestDomainName(),
                DomainName = "corebridge.net",
                DomainEmailID = 0,
                IsPrivate = true,
                EmployeeID = employeeID,
                Credentials = "{ 'password': 'teste' }",
                EmailAddress = "test@test.com",
                AliasUserNames = "test",
                DisplayName = "test",
            };
            EmailAccountTeamLink simpleTeamLink = new EmailAccountTeamLink()
            {
                BID = BID,
                EmailAccountID = _emailAccount.IDAsShort,
                TeamID = teamID,
            };
            _emailAccount.EmailAccountTeamLinks = new List<EmailAccountTeamLink>() { simpleTeamLink };
            return this;
        }

        public EmailAccountData Build() => _emailAccount;
    }
}
