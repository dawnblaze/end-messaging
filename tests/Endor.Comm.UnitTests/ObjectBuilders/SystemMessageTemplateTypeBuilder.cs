﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class SystemMessageTemplateTypeBuilder
    {
        private SystemMessageTemplateType _model = new SystemMessageTemplateType();

        public SystemMessageTemplateTypeBuilder Id(byte id)
        {
            _model.ID = id;
            return this;
        }

        public SystemMessageTemplateTypeBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            var newMsgBodyTemplate = DateTime.UtcNow + " SystemMessageTemplateType";
            _model = new SystemMessageTemplateType()
            {
                ID = byte.MaxValue,
                AppliesToClassTypeID = 5000,
                ChannelType = MessageChannelType.Email,
                Name = newMsgBodyTemplate,
            };
            return this;
        }

        public SystemMessageTemplateType Build() => _model;
    }
}
