﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class MessageBodyBuilder
    {
        private MessageBody _messageBody = new MessageBody();

        public MessageBodyBuilder Id(int id)
        {
            _messageBody.ID = id;
            return this;
        }

        public MessageBodyBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            _messageBody = new MessageBody()
            {
                BID = BID,
                ID = -100,
                ModifiedDT = DateTime.UtcNow,
                Subject = "Subject",
                BodyFirstLine = "The quick brown fox ...",
                HasBody = true,
                AttachedFileCount = (byte)0,
                AttachedFileNames = null,
                HasAttachment = false,
                MetaData = null,
                WasModified = false,
                SizeInKB = 20
            };
            return this;
        }

        public MessageBody Build() => _messageBody;
    }
}
