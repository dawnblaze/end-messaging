﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Non_Owned_Entities;
using System;
using System.Collections.Generic;
using System.Text;
using static Endor.Comm.UnitTests.UnitTestHelpers;
using Endor.Comm.IntegrationTests.Specifications;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class MessageHeaderBuilder
    {
        private MessageHeader _messageHeader = new MessageHeader(isRead: false, isDeleted: false);

        public MessageHeaderBuilder Id(int id)
        {
            _messageHeader.ID = id;
            return this;
        }

        public MessageHeaderBuilder MessageParticipantInfoId(int id)
        {
            _messageHeader.ParticipantID = id;
            return this;
        }

        public MessageHeaderBuilder MessageBodyId(int id)
        {
            _messageHeader.BodyID = id;
            return this;
        }

        public MessageHeaderBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            short employeeID = 0;
            if (repository!=null)
            {
                repository.SetTableNameForClassType((new EmployeeData()).ClassTypeID);
                employeeID = (repository.List<EmployeeData>(new GetValidIDByBIDSpecification<EmployeeData>(BID))).First().IDAsShort;
            }
            _messageHeader = new MessageHeader(isRead: false, isDeleted: false)
            {
                BID = BID,
                ID = -100,
                ModifiedDT = DateTime.Now,
                ReceivedDT = DateTime.Now,
                EmployeeID = employeeID,
                IsDeleted = false,
                DeletedOrExpiredDT = DateTime.Now,
                IsExpired = false,
                Channels = MessageChannelType.Message,
                InSentFolder = false
            };
            return this;
        }

        public MessageHeader Build() => _messageHeader;
    }
}
