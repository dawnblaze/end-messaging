﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class MessageParticipantInfoBuilder
    {
        private MessageParticipantInfo _messageParticipantInfo = new MessageParticipantInfo();

        public MessageParticipantInfoBuilder Id(int id)
        {
            _messageParticipantInfo.ID = id;
            return this;
        }

        public MessageParticipantInfoBuilder ParticipantRoleType(MessageParticipantRoleType type)
        {
            _messageParticipantInfo.ParticipantRoleType = type;
            return this;
        }

        public MessageParticipantInfoBuilder MessageBodyId(int id)
        {
            _messageParticipantInfo.BodyID = id;
            return this;
        }

        public MessageParticipantInfoBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            _messageParticipantInfo = new MessageParticipantInfo()
            {
                BID = BID,
                ID = -100,
                ModifiedDT = DateTime.Now,
                ParticipantRoleType = MessageParticipantRoleType.To,
                Channel = MessageChannelType.Email,
                UserName = "Test",
                IsMergeField = false,
                DeliveredDT = DateTime.Now,
                IsDelivered = false
            };
            return this;
        }

        public MessageParticipantInfo Build() => _messageParticipantInfo;
    }
}
