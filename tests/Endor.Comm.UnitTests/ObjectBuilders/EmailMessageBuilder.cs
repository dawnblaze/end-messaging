﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Services.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class EmailMessageBuilder
    {
        private EmailMessage _emailMessage = new EmailMessage();

        public EmailMessageBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            if (repository != null)
            {
              
            }
            _emailMessage = new EmailMessage()
            {
                 Subject = "Test Subject",
                 Content = "Test Content"
            };
            return this;
        }

        public EmailMessage Build() => _emailMessage;
    }
}
