﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using System;
using System.Collections.Generic;
using System.Text;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class LocationBuilder
    {
        private LocationData _locationData = new LocationData();

        public LocationBuilder Id(int id)
        {
            _locationData.ID = id;
            return this;
        }

        public LocationBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            _locationData = new LocationData()
            {
                BID = BID,
                ID = -100,
                ModifiedDT = DateTime.UtcNow
            };
            return this;
        }

        public LocationData Build() => _locationData;
    }
}
