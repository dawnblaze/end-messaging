﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.IntegrationTests.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Endor.Comm.UnitTests.UnitTestHelpers;

namespace Endor.Comm.UnitTests.ObjectBuilders
{
    public class DomainEmailBuilder
    {
        private DomainEmail _domainEmail = new DomainEmail();

        public DomainEmailBuilder Id(int id)
        {
            _domainEmail.ID = id;
            return this;
        }

        public DomainEmailBuilder WithDefaultValues(IDapperReadOnlyRepository repository = null)
        {
            byte locationID = 0;
            if (repository != null)
            {
                repository.SetTableNameForClassType((new LocationData()).ClassTypeID);
                locationID = (repository.List<LocationData>(new GetValidIDByBIDSpecification<LocationData>(BID))).First().IDAsByte;
            }
            _domainEmail = new DomainEmail()
            {
                BID = BID,
                ID = -100,
                IsActive = true,
                ProviderType = EmailProviderType.CustomSMTP,
                CreatedDate = DateTime.Now,
                DomainName = "corebridge.net",
                SMTPAddress = "smtp.gmail.com",
                SMTPPort = 465,
                SMTPSecurityType = EmailSecurityType.SSL,
                SMTPAuthenticateFirst = true
            };
            DomainEmailLocationLink simpleLocation = new DomainEmailLocationLink()
            {
                BID = BID,
                LocationID = locationID,
                DomainID = _domainEmail.IDAsShort
            };
            _domainEmail.LocationLinks = new List<DomainEmailLocationLink>() { simpleLocation };
            return this;
        }

        public DomainEmail Build() => _domainEmail;
    }
}
