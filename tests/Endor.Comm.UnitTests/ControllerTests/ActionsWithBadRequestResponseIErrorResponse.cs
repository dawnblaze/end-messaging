﻿using AutoMapper;
using Endor.Comm.ApiModels;
using Endor.Comm.Core.Configurations;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Factories.Interfaces;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.Web.Common;
using Endor.Comm.Web.Controllers;
using Endor.Security;
using Endor.Tenant;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.UnitTests.ControllerTests
{
    [TestClass]
    public class ActionsWithBadRequestResponseIErrorResponse
    {

        Mock<IRepository> _repository;
        Mock<IHttpContextAccessor> _httpCtx;
        Mock<ITenantDataCache> _tenantCache;
        Mock<IMapper> _mapper;
        Mock<IWebHostEnvironment> _hostingEnvironment;
        Mock<IKeyedFactory<IEmailProvider, EmailProviderType>> _providerFactory;
        Mock<EndorOptions> _options;

        public ActionsWithBadRequestResponseIErrorResponse()
        {
            _repository = new Mock<IRepository>();
            _httpCtx = new Mock<IHttpContextAccessor>();
            _tenantCache = new Mock<ITenantDataCache>();
            _mapper = new Mock<IMapper>();
            _hostingEnvironment = new Mock<IWebHostEnvironment>();
            _options = new Mock<EndorOptions>();
            _providerFactory = new Mock<IKeyedFactory<IEmailProvider, EmailProviderType>>();
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task DomainEmailActionsTest()
        {
            var mockMapper = new MapperConfiguration(cfg => { cfg.AddProfile(new DomainEmailProfile()); });
            var mapper = mockMapper.CreateMapper();

            _repository.Setup(x => x.GetByShortIdAsync<DomainEmail>(It.IsAny<short>())).Returns(Task.FromResult(new DomainEmail()));
            _repository.Setup(x => x.UpdateAsync(It.IsAny<DomainEmail>())).Throws(new Exception());

            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] {
                new Claim("BID", "1"),
                new Claim("EmployeeID", "1")
            }));

            var tenantData = new TenantData();
            tenantData.BusinessDBConnectionString = "";
            _httpCtx.Setup(x => x.HttpContext).Returns(new DefaultHttpContext() { User = user });
            _tenantCache.Setup(x => x.Get(It.IsAny<short>())).Returns(Task.FromResult(tenantData));
            
            var domainEmailController = new DomainEmailController(_repository.Object, _httpCtx.Object, _tenantCache.Object, mapper, _providerFactory.Object, _options.Object);
            var result = await domainEmailController.SetActive(1);
            result.GetType().Should().Be(typeof(ActionErrorResponse));
        }
    }
}
