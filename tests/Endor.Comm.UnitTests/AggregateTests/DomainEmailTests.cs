﻿using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Endor.Comm.UnitTests.AggregateTests
{
    [TestClass]
    public class DomainEmailTests
    {
        [TestMethod]
        public void MarkingAsActive()
        {
            //Arrange
            var domainEmail = new DomainEmailBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            domainEmail.MarkAsActive();

            //Assert
            domainEmail.CheckIsActive().Should().BeTrue();
        }

    }
}
