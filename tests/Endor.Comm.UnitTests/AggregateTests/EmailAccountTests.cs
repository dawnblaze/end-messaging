﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Endor.Comm.Core.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Services.Classes;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace Endor.Comm.UnitTests.AggregateTests
{
    [TestClass]
    public class EmailAccountTestsTests
    {
        IFixture _fixture;
        Mock<IEmailProvider> _emailProvider;
        
        public EmailAccountTestsTests()
        {
            _emailProvider = new Mock<IEmailProvider>();
            _fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });
        }


        [TestMethod]
        public void MarkingAsActive()
        {
            //Arrange
            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            emailAccount.MarkAsActive();

            //Assert
            emailAccount.CheckIsActive().Should().BeTrue();
        }

        [TestMethod]
        public void MarkingAsInActive()
        {
            //Arrange
            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            emailAccount.MarkAsInActive();

            //Assert
            emailAccount.CheckIsActive().Should().BeFalse();
        }

        [TestMethod]
        public void UpdateStatusBasedOnCredentialsPending()
        {
            //Arrange
            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(null)
                .Build();
            emailAccount.Credentials = null;

            //Act
            emailAccount.UpdateStatusBasedOnCredentials(false, false);

            //Assert
            emailAccount.StatusType.Should().Be(EmailAccountStatus.Pending);
        }

        [TestMethod]
        public void UpdateStatusBasedOnCredentialsAuthorized()
        {
            //Arrange
            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            emailAccount.UpdateStatusBasedOnCredentials(true, false);

            //Assert
            emailAccount.StatusType.Should().Be(EmailAccountStatus.Authorized);
        }

        [TestMethod]
        public void UpdateStatusBasedOnCredentialsFailed()
        {
            //Arrange
            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            emailAccount.UpdateStatusBasedOnCredentials(false, false);

            //Assert
            emailAccount.StatusType.Should().Be(EmailAccountStatus.Failed);
        }

        [TestMethod]
        public async Task SendEmailProviderFailsReturnsBooleanResultFalse()
        {
            //Arrange
            var emailMessage = _fixture.Build<EmailMessage>().Create();
            var booleanResult = _fixture.Build<BooleanResult>().Create();
            booleanResult.Success = false;

            _emailProvider.Setup(x => x.Send(It.IsAny<EmailSendInfo>()));
            _emailProvider.Setup(x => x.Validate(It.IsAny<EmailAccountCredentials>())).Returns(Task.FromResult(booleanResult));

            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            var result = await emailAccount.SendEmail(emailMessage, _emailProvider.Object, "");

            //Assert
            result.GetType().Should().Be(typeof(BooleanResult));
            result.Success.Should().Be(false);
        }

        [TestMethod]
        public async Task SendEmailProviderSuccessReturnsBooleanResultTrue()
        {
            //Arrange
            var emailMessage = _fixture.Build<EmailMessage>().Create();
            var booleanResult = _fixture.Build<BooleanResult>().Create();
            booleanResult.Success = true;

            _emailProvider.Setup(x => x.Send(It.IsAny<EmailSendInfo>()));
            _emailProvider.Setup(x => x.Validate(It.IsAny<EmailAccountCredentials>())).Returns(Task.FromResult(booleanResult));

            var emailAccount = new EmailAccountBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            var result = await emailAccount.SendEmail(emailMessage, _emailProvider.Object, "");

            //Assert
            result.GetType().Should().Be(typeof(BooleanResult));
            result.Success.Should().Be(true);
        }
    }
}
