﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Events;
using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Comm.UnitTests.AggregateTests
{
    [TestClass]
    public class MessageTests
    {
        [TestMethod]
        public void MarkingAsRead()
        {
            //Arrange
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            messageHeader.MarkAsRead();

            //Assert
            messageHeader.CheckIsRead().Should().BeTrue();
        }

        [TestMethod]
        public void MarkingAsUnread()
        {
            //Arrange
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            messageHeader.MarkAsUnread();

            //Assert
            messageHeader.CheckIsRead().Should().BeFalse();
        }

        [TestMethod]
        public void MardAsReadRaisesEntityUpdatedEvent()
        {
            //Arrange
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            messageHeader.MarkAsRead();

            //Assert
            messageHeader.Events.Should().HaveCount(1);
            messageHeader.Events.First().Should().BeOfType(typeof(EntityUpdatedEvent));
        }

        [TestMethod]
        public void MarkingAsDeleted()
        {
            //Arrange
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            messageHeader.MarkAsDeleted();

            //Assert
            messageHeader.CheckIsDeleted().Should().BeTrue();
        }

        [TestMethod]
        public void UndeletingAMessage()
        {
            //Arrange
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            messageHeader.Undelete();

            //Assert
            messageHeader.CheckIsDeleted().Should().BeFalse();
        }

        [TestMethod]
        public void MarkingAsDeletedRaisesEntityDeletedEvent()
        {
            //Arrange
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            messageHeader.MarkAsDeleted();

            //Assert
            messageHeader.Events.Should().HaveCount(1);
            messageHeader.Events.First().Should().BeOfType(typeof(EntityDeletedEvent));
        }


        [TestMethod]
        public void UndeletingAMessageRaisesEntityUpdatedEvent()
        {
            //Arrange
            var messageHeader = new MessageHeaderBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            messageHeader.Undelete();

            //Assert
            messageHeader.Events.Should().HaveCount(1);
            messageHeader.Events.First().Should().BeOfType(typeof(EntityUpdatedEvent));
        }
    }
}
