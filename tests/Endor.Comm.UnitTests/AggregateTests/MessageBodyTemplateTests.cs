﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Events;
using Endor.Comm.UnitTests.ObjectBuilders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Comm.UnitTests.AggregateTests
{
    [TestClass]
    public class MessageBodyTemplateTests
    {
        [TestMethod]
        public void MarkingAsActive()
        {
            //Arrange
            var messageBodyTemplate = new MessageBodyTemplateBuilder()
                .WithDefaultValues(null)
                .Build();

            //Act
            messageBodyTemplate.MarkAsActive();

            //Assert
            messageBodyTemplate.CheckIsActive().Should().BeTrue();
        }

        [TestMethod]
        public void AdjustSortIndex()
        {
            //Arrange
            var messageBodyTemplate = new MessageBodyTemplateBuilder()
                .WithDefaultValues(null)
                .Build();
            short adjustmentAmount = 1;
            var startingSortIndex = messageBodyTemplate.CheckSortIndex();
            //Act
            messageBodyTemplate.MoveSortIndexBy(adjustmentAmount);

            //Assert
            messageBodyTemplate.CheckSortIndex().Should().Be((short)(startingSortIndex + adjustmentAmount));
        }

        [TestMethod]
        public void MoveBefore()
        {
            //Arrange
            var messageBodyTemplate = new MessageBodyTemplateBuilder()
                .WithDefaultValues(null)
                .SortIndex(5)
                .Build();
            
            //Act
            var resultTuple = messageBodyTemplate.MoveTo(6);

            //Assert
            messageBodyTemplate.CheckSortIndex().Should().Be((short)6);
            resultTuple.Item1.Should().Be(6);
            resultTuple.Item2.Should().Be(6);
            resultTuple.Item3.Should().Be(-1);
        }

        [TestMethod]
        public void MoveAfter()
        {
            //Arrange
            var messageBodyTemplate = new MessageBodyTemplateBuilder()
                .WithDefaultValues(null)
                .SortIndex(6)
                .Build();

            //Act
            var resultTuple = messageBodyTemplate.MoveTo(5);

            //Assert
            messageBodyTemplate.CheckSortIndex().Should().Be((short)5);
            resultTuple.Item1.Should().Be(5);
            resultTuple.Item2.Should().Be(5);
            resultTuple.Item3.Should().Be(1);
        }

        [TestMethod]
        public void ParticipantListSerializationChecking()
        {
            //Arrange
            var messageBodyTemplate = new MessageBodyTemplateBuilder()
                .WithDefaultValues(null)
                .Build();
            
            Assert.IsNull(messageBodyTemplate.Participants);
            Assert.IsNull(messageBodyTemplate.ParticipantInfoJSON);
            messageBodyTemplate.Participants = new List<MessageParticipantInfo>()
            {
                new MessageParticipantInfoBuilder().WithDefaultValues(null).Build()
            };

            Assert.IsNotNull(messageBodyTemplate.Participants);
            Assert.IsNotNull(messageBodyTemplate.ParticipantInfoJSON);

            messageBodyTemplate.ParticipantInfoJSON = "{\"fail\":\"toconvert\"}";
            try
            {
                var list = messageBodyTemplate.Participants;
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex.Message);
                Assert.AreEqual("Could not deserialize ParticipantInfoJSON.", ex.Message);
            }
            messageBodyTemplate.ParticipantInfoJSON = "[]";
            Assert.IsNotNull(messageBodyTemplate.Participants);
            Assert.IsFalse(messageBodyTemplate.Participants.Count > 0);
        }
    }
}
