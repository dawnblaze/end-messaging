﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.UnitTests
{
    public class UnitTestHelpers
    {
        public static short BID = 1;
        public const byte AID = 1;
        public const short AuthUserLinkID = 1;
        public const int AuthUserID = 1;
        public const string AuthUserName = "TestUser";
        public const short AuthEmployeeID = 1;
        public const short AccessType = 49;
    }
}
