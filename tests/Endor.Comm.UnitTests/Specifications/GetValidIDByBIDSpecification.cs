﻿using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.IntegrationTests.Specifications
{
    public class GetValidIDByBIDSpecification<T> : BaseSpecification<T> where T : BaseIdentityEntity
    {
        public GetValidIDByBIDSpecification(short bid)
            : base()
        {
            AddCriteria(new GetValidIDByBIDCriteria<T>(bid));
            ApplyPaging(0, 1);
        }
    }

    public class GetValidIDByBIDCriteria<T> : Criteria<T> where T : BaseIdentityEntity
    {
        private readonly short _bid;
        public GetValidIDByBIDCriteria(short bid)
        {
            _bid = bid;
        }
        public override Expression<Func<T, bool>> ToExpression()
        {
            return entity => entity.BID==_bid && entity.ID>0;
        }
    }
}
