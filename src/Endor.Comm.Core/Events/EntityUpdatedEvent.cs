﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Events
{
    public class EntityUpdatedEvent : EntityModificationEvent
    {
        public EntityUpdatedEvent(BaseIdentityEntity entity)
            : base(entity)
        {
        }

    }
}
