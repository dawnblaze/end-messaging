﻿using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Handlers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Events
{
    public class EntityAddedEvent : EntityModificationEvent
    {
        public EntityAddedEvent(BaseIdentityEntity entity)
            : base(entity)
        {

        }
    }
}
