﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Events
{
    public abstract class EntityModificationEvent : BaseDomainEvent
    {
        public BaseIdentityEntity entity { get; }
        public EntityModificationEvent(BaseIdentityEntity entity)
        {
            this.entity = entity;
        }
    }
}
