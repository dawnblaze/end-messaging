﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Events
{
    public class EmailSentSuccessEvent : BaseDomainEvent, IDomainEvent
    {
        public MessageHeader MessageHeader { get; }
        
        public EmailSentSuccessEvent(MessageHeader messageHeader)
        {
            MessageHeader = messageHeader;
        }

    }
}
