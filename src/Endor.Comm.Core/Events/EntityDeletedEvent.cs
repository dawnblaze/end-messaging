﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Events
{
    public class EntityDeletedEvent : EntityModificationEvent
    {
        public EntityDeletedEvent(BaseIdentityEntity entity)
            : base (entity)
        {
        }

    }
}
