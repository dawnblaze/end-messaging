﻿using Endor.Comm.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Events
{
    public class MessageHeaderCreatedEvent : EntityModificationEvent
    {
        public MessageHeaderCreatedEvent(MessageHeader entity)
            : base(entity)
        {

        }
    }
}
