﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class EmailAccountsSpecification : BaseSpecification<EmailAccountData>
    {
        public EmailAccountsSpecification(bool? IsActive, string emailAddress, short? domainID = null, short[] domainIDList = null) : base()
        {
            if (IsActive.HasValue)
                AddCriteria(new EmailAccountFilterByIsActive(IsActive));

            AddCriteria(new EmailAccountFilterByEmailAddress(emailAddress));
            AddCriteria(new EmailAccountFilterByDomainEmailID(domainID));
            AddCriteria(new EmailAccountFilterByDomainEmailIDList(domainIDList));
            AddInclude(a => a.EmailAccountTeamLinks);
        }
    }
}
