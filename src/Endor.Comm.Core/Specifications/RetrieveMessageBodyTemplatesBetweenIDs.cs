﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class RetrieveMessageBodyTemplatesBetweenSortIndexes : BaseSpecification<MessageBodyTemplate>
    {
        public RetrieveMessageBodyTemplatesBetweenSortIndexes(short bid, short beginSortIndex, short endSortIndex)
            : base()
        {
            AddCriteria(new MessageBodyTemplatesBetweenIDsCriteria(beginSortIndex, endSortIndex));
        }
    }

    /// <summary>
    /// Filter By EmployeeId if it's defined
    /// </summary>
    public class MessageBodyTemplatesBetweenIDsCriteria : Criteria<MessageBodyTemplate>
    {
        private readonly short _beginSortIndex;
        private readonly short _endSortIndex;
        public MessageBodyTemplatesBetweenIDsCriteria(short beginSortIndex, short endSortIndex)
        {
            _beginSortIndex = beginSortIndex;
            _endSortIndex = endSortIndex;
        }
        public override Expression<Func<MessageBodyTemplate, bool>> ToExpression()
        {
            return h => h.SortIndex >= _beginSortIndex && h.SortIndex <= _endSortIndex;
        }
    }
}
