﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;

namespace Endor.Comm.Core.Specifications
{
    public class EmailAccountTeamLinkFilterByTeamSpecification : BaseSpecification<EmailAccountTeamLink>
    {
        public EmailAccountTeamLinkFilterByTeamSpecification(short emailAccountID, short? teamID) : base()
        {
            AddCriteria(new EmailAccountTeamLinkFilterByTeamID(emailAccountID, teamID));
        }
    }
}
