﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Linq.Expressions;

namespace Endor.Comm.Core.Specifications
{
    /// <summary>
    /// Filter By EmployeeId if it's defined
    /// </summary>
    public class FilterMessageParticipantInfoByEmployeeID : Criteria<MessageParticipantInfo>
    {
        private readonly int _EmployeeID;
        
        public FilterMessageParticipantInfoByEmployeeID(int EmployeeID)
        {
            _EmployeeID = EmployeeID;
        }

        public override Expression<Func<MessageParticipantInfo, bool>> ToExpression()
        {
            return h => h.EmployeeID == _EmployeeID;
        }
    }
}
