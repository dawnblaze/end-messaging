﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.Comm.Core.Specifications
{    
    /// <summary>
     /// Filter By IsActive
     /// </summary>
    public class DomainEmailFilterByIsActive : Criteria<DomainEmail>
        {
            private readonly bool _isActive;

            public DomainEmailFilterByIsActive(bool? isActive)
            {
                _isActive = isActive ?? true;
            }
            public override Expression<Func<DomainEmail, bool>> ToExpression()
            {
                return t => (!_isActive || t.IsActive);
            }
        }

        /// <summary>
        /// Filter By LocationID
        /// </summary>
        public class DomainEmailFilterByLocation : Criteria<DomainEmail>
        {
            private readonly byte? _locationID;

            public DomainEmailFilterByLocation(byte? locationID)
            {
                _locationID = locationID;
            }
            public override Expression<Func<DomainEmail, bool>> ToExpression()
            {
                return t => (_locationID == null || t.IsForAllLocations == true);
            }
        }
}
