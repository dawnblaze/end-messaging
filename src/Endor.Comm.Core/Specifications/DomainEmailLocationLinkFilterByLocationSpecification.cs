﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;

namespace Endor.Comm.Core.Specifications
{
    public class DomainEmailLocationLinkFilterByLocationSpecification : BaseSpecification<DomainEmailLocationLink>
    {
        public DomainEmailLocationLinkFilterByLocationSpecification(byte locationID) : base()
        {
            AddCriteria(new DomainEmailLocationLinkFilterByLocationID(locationID));
        }
    }
}
