﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Linq.Expressions;

namespace Endor.Comm.Core.Specifications
{
        /// <summary>
        /// Filter By IsActive
        /// </summary>
        public class MessageBodyTemplateFilterByIsActive : Criteria<MessageBodyTemplate>
        {
            private readonly bool _isActive;

            public MessageBodyTemplateFilterByIsActive(bool? isActive)
            {
                _isActive = isActive ?? true;
            }
            public override Expression<Func<MessageBodyTemplate, bool>> ToExpression()
            {
                return t => (!_isActive || t.IsActive);
            }
        }

        /// <summary>
        /// Filter By AppliesToClassTypeID
        /// </summary>
        public class MessageBodyTemplateFilterByAppliesToClassTypeID : Criteria<MessageBodyTemplate>
        {
            private readonly int? _appliesToClassTypeID;

            public MessageBodyTemplateFilterByAppliesToClassTypeID(int? AppliesToClassTypeID)
            {
                _appliesToClassTypeID = AppliesToClassTypeID;
            }
            public override Expression<Func<MessageBodyTemplate, bool>> ToExpression()
            {
                return t => (_appliesToClassTypeID==null || (t.AppliesToClassTypeID == _appliesToClassTypeID));
            }
        }

        /// <summary>
        /// Filter By TemplateType
        /// </summary>
        public class MessageBodyTemplateFilterByTemplateType : Criteria<MessageBodyTemplate>
        {
            private readonly int? _templateType;

            public MessageBodyTemplateFilterByTemplateType(int? TemplateType)
            {
                _templateType = TemplateType;
            }
            public override Expression<Func<MessageBodyTemplate, bool>> ToExpression()
            {
                return t => (_templateType == null || (t.MessageTemplateType == _templateType));
            }
        }
}
