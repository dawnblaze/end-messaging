﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;

namespace Endor.Comm.Core.Specifications
{
    public class DomainEmailLocationLinkFilterSpecification : BaseSpecification<DomainEmailLocationLink>
    {
        public DomainEmailLocationLinkFilterSpecification(short bid, short domainID, byte locationID) : base()
        {
            AddCriteria(new DomainEmailLocationLinkFilterByDomainID(domainID));
            AddCriteria(new DomainEmailLocationLinkFilterByLocationID(locationID));
        }
    }
}
