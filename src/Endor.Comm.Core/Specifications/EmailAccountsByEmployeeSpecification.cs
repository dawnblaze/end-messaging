﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class EmailAccountsByEmployeeSpecification : BaseSpecification<EmailAccountData>
    {
        public EmailAccountsByEmployeeSpecification(bool? IsActive, short? employeeID) : base()
        {
            AddCriteria(new EmailAccountFilterByIsActive(IsActive));
            AddCriteria(new EmailAccountFilterByEmployeeID(employeeID));
        }
    }
}
