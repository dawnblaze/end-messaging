﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Endor.Comm.Core.Specifications
{
    /// <summary>
    /// Retrieve all messages by channgel type and employee id (optional)
    /// </summary>
    public class RetrieveAllMessagesInAChannel : BaseSpecification<MessageHeader>
    {
        public RetrieveAllMessagesInAChannel(short bid, MessageChannelType channel, short? employeeID)
            : base()
        {
            AddCriteria(new FilterMessageHeaderByEmployeeIDIfDefined(employeeID));
            AddCriteria(new FilterByMessageChannelType(channel));
        }
    }       
}
