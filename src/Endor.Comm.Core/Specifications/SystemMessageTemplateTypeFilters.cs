﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Linq.Expressions;

namespace Endor.Comm.Core.Specifications
{
    /// <summary>
    /// Filter By AppliesToClassTypeID
    /// </summary>
    public class SystemMessageTemplateTypeFilterByAppliesToClassTypeID : Criteria<SystemMessageTemplateType>
    {
        private readonly int? _appliesToClassTypeID;

        public SystemMessageTemplateTypeFilterByAppliesToClassTypeID(int? AppliesToClassTypeID)
        {
            _appliesToClassTypeID = AppliesToClassTypeID;
        }
        public override Expression<Func<SystemMessageTemplateType, bool>> ToExpression()
        {
            return t => (_appliesToClassTypeID == null || (t.AppliesToClassTypeID == _appliesToClassTypeID));
        }
    }

    /// <summary>
    /// Filter By TemplateType
    /// </summary>
    public class SystemMessageTemplateTypeFilterByChannelType : Criteria<SystemMessageTemplateType>
    {
        private readonly byte? _channelType;

        public SystemMessageTemplateTypeFilterByChannelType(byte? ChannelType)
        {
            _channelType = ChannelType;
        }
        public override Expression<Func<SystemMessageTemplateType, bool>> ToExpression()
        {
            return t => (_channelType == null || (t.ChannelType == (MessageChannelType)_channelType));
        }
    }
}
