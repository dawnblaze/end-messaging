﻿using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class EmployeeTeamLinksByEmployeeSpecification : BaseSpecification<EmployeeTeamLink>
    {
        public EmployeeTeamLinksByEmployeeSpecification(short employeeID) : base()
        {
            AddCriteria(new EmployeeTeamLinkByEmployeeID(employeeID));
        }
    }

    /// <summary>
    /// Filter By EmployeeID
    /// </summary>
    public class EmployeeTeamLinkByEmployeeID : Criteria<EmployeeTeamLink>
    {
        private readonly short _employeeID;

        public EmployeeTeamLinkByEmployeeID(short employeeID)
        {
            _employeeID = employeeID;
        }
        public override Expression<Func<EmployeeTeamLink, bool>> ToExpression()
        {
            return t => (_employeeID == t.EmployeeID);
        }
    }
}
