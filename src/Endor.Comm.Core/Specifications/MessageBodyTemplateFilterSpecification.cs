﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;

namespace Endor.Comm.Core.Specifications
{
    public class MessageBodyTemplateFilterSpecification : BaseSpecification<MessageBodyTemplate>
    {
        public MessageBodyTemplateFilterSpecification(
            bool? IsActive,
            int? AppliesToClassTypeID,
            int? TemplateType
            )
            : base()

        {
            AddCriteria(new MessageBodyTemplateFilterByIsActive(IsActive));
            AddCriteria(new MessageBodyTemplateFilterByAppliesToClassTypeID(AppliesToClassTypeID));
            AddCriteria(new MessageBodyTemplateFilterByTemplateType(TemplateType));
        }
    }
}
