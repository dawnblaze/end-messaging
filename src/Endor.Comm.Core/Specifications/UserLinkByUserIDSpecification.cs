﻿using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class UserLinkByUserIDSpecification : BaseSpecification<UserLink>
    {
        public UserLinkByUserIDSpecification(short bid, int UserID)
            : base()
        {
            AddCriteria(new UserLinkByUserID(bid, UserID));
            ApplyPaging(0, 1);
        }

        public class UserLinkByUserID : Criteria<UserLink>
        {
            private readonly short _bid;
            private readonly int _userID;
            public UserLinkByUserID(short bid, int userID)
            {
                _bid = bid;
                _userID = userID;
            }
            public override Expression<Func<UserLink, bool>> ToExpression()
            {
                return ul => ul.BID == _bid && ul.AuthUserID == _userID;
            }
        }
    }
}
