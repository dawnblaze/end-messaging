﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    /// <summary>
    /// Filter By IsActive
    /// </summary>
    public class EmailAccountFilterByIsActive : Criteria<EmailAccountData>
    {
        private readonly bool _isActive;

        public EmailAccountFilterByIsActive(bool? isActive)
        {
            _isActive = isActive ?? true;
        }
        public override Expression<Func<EmailAccountData, bool>> ToExpression()
        {
            return t => (!_isActive || t.IsActive);
        }
    }

    /// <summary>
    /// Filter By email address
    /// </summary>
    public class EmailAccountFilterByEmailAddress : Criteria<EmailAccountData>
    {
        private readonly string _emailAddress;

        public EmailAccountFilterByEmailAddress(string emailAddress)
        {
            _emailAddress = (emailAddress ?? "").Trim().ToLower();
        }
        public override Expression<Func<EmailAccountData, bool>> ToExpression()
        {
            return t => String.IsNullOrEmpty(_emailAddress) || t.EmailAddress.ToLower() == _emailAddress;
        }
    }    

    /// <summary>
    /// Filter By DomainID
    /// </summary>
    public class EmailAccountFilterByDomainEmailID : Criteria<EmailAccountData>
    {
        private readonly short? _domainEmailID;

        public EmailAccountFilterByDomainEmailID(short? domainEmailID)
        {
            _domainEmailID = domainEmailID;
        }
        public override Expression<Func<EmailAccountData, bool>> ToExpression()
        {
            return t => (!_domainEmailID.HasValue || _domainEmailID == t.DomainEmailID);
        }
    }

    /// <summary>
    /// Filter By DomainID List
    /// </summary>
    public class EmailAccountFilterByDomainEmailIDList : Criteria<EmailAccountData>
    {
        private readonly short[] _domainEmailIDList;

        public EmailAccountFilterByDomainEmailIDList(short[] domainEmailIDList)
        {
            _domainEmailIDList = domainEmailIDList;
        }
        public override Expression<Func<EmailAccountData, bool>> ToExpression()
        {
            return t => (!(_domainEmailIDList != null) || _domainEmailIDList.Contains(t.DomainEmailID));
        }
    }

    /// <summary>
    /// Filter By EmployeeID
    /// </summary>
    public class EmailAccountFilterByEmployeeID : Criteria<EmailAccountData>
    {
        private readonly short? _employeeID;

        public EmailAccountFilterByEmployeeID(short? employeeID)
        {
            _employeeID = employeeID;
        }
        public override Expression<Func<EmailAccountData, bool>> ToExpression()
        {
            return t => (!_employeeID.HasValue || _employeeID == t.EmployeeID);
        }
    }

    /// <summary>
    /// Filter By TeamLink by team
    /// </summary>
    public class EmailAccountTeamLinkFilterByTeamID : Criteria<EmailAccountTeamLink>
    {
        private readonly short? _teamID;
        private readonly short _emailAccountID;

        public EmailAccountTeamLinkFilterByTeamID(short emailAccountID, short? teamID)
        {
            _teamID = teamID;
            _emailAccountID = emailAccountID;
        }
        public override Expression<Func<EmailAccountTeamLink, bool>> ToExpression()
        {
            return t => (_emailAccountID == t.EmailAccountID && (!_teamID.HasValue || _teamID == t.TeamID));
        }
    }
}
