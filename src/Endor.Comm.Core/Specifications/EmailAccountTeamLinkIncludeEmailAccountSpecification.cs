﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class EmailAccountTeamLinkIncludeEmailAccountSpecification : BaseSpecification<EmailAccountTeamLink>
    {
        public EmailAccountTeamLinkIncludeEmailAccountSpecification() : base()
        {
            AddInclude(x => x.EmailAccount);
        }
    }
}
