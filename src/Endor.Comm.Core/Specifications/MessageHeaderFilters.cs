﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Linq.Expressions;

namespace Endor.Comm.Core.Specifications
{
    /// <summary>
    /// Filter By EmployeeId if it's defined
    /// </summary>
    public class FilterMessageHeaderByID : Criteria<MessageHeader>
    {
        private readonly int _ID;
        public FilterMessageHeaderByID(int ID)
        {
            _ID = ID;
        }
        public override Expression<Func<MessageHeader, bool>> ToExpression()
        {
            return h => h.ID == _ID;
        }
    }

    /// <summary>
    /// Filter By EmployeeId if it's defined
    /// </summary>
    public class FilterMessageHeaderByEmployeeIDIfDefined : Criteria<MessageHeader>
    {
        private readonly short? _employeeID;
        public FilterMessageHeaderByEmployeeIDIfDefined(short? employeeID)
        {
            _employeeID = employeeID;
        }
        public override Expression<Func<MessageHeader, bool>> ToExpression()
        {
            return h => _employeeID.HasValue && h.EmployeeID == (short)_employeeID ;
        }
    }

    /// <summary>
    /// Filter By MessageChannelType
    /// </summary>
    public class FilterByMessageChannelType : Criteria<MessageHeader>
    {
        private readonly MessageChannelType _messageChannelType;
        
        public FilterByMessageChannelType(MessageChannelType messageChannelType)
        {
            _messageChannelType = messageChannelType;
        }
        public override Expression<Func<MessageHeader, bool>> ToExpression()
        {
            return h => (_messageChannelType == MessageChannelType.None && h.Channels == MessageChannelType.None) || (_messageChannelType != MessageChannelType.None && h.Channels.HasFlag(_messageChannelType));
        }
    }

    /// <summary>
    /// Filter to include read message
    /// </summary>
    public class FilterByRead : Criteria<MessageHeader>
    {
        private readonly TrueFalseOnly _includeRead;
        public FilterByRead(TrueFalseOnly includeRead)
        {
            _includeRead = includeRead;
        }
        public override Expression<Func<MessageHeader, bool>> ToExpression()
        {
            return header => (_includeRead == TrueFalseOnly.True || (_includeRead == TrueFalseOnly.Only && header.ReadDT.HasValue && header.IsRead) || (_includeRead == TrueFalseOnly.False && !(header.ReadDT.HasValue && header.IsRead)));
        }
    }

    /// <summary>
    /// Filter to include read message
    /// </summary>
    public class FilterBySent : Criteria<MessageHeader>
    {
        private readonly TrueFalseOnly _includeSent;
        public FilterBySent(TrueFalseOnly includeSent)
        {
            _includeSent = includeSent;
        }
        public override Expression<Func<MessageHeader, bool>> ToExpression()
        {
            return header => (_includeSent == TrueFalseOnly.True || (_includeSent == TrueFalseOnly.Only && header.InSentFolder) || (_includeSent == TrueFalseOnly.False && !header.InSentFolder));
        }
    }

    /// <summary>
    /// Filter to include read message
    /// </summary>
    public class FilterByDeleted : Criteria<MessageHeader>
    {
        private readonly TrueFalseOnly _includeDeleted;
        public FilterByDeleted(TrueFalseOnly includeDeleted)
        {
            _includeDeleted = includeDeleted;
        }
        public override Expression<Func<MessageHeader, bool>> ToExpression()
        {
            return header => (_includeDeleted == TrueFalseOnly.True || (_includeDeleted == TrueFalseOnly.Only && header.IsDeleted) || (_includeDeleted == TrueFalseOnly.False && !header.IsDeleted));
        }
    }
}
