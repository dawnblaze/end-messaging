﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;

namespace Endor.Comm.Core.Specifications
{
    public class SystemMessageTemplateTypeFilterSpecification : BaseSpecification<SystemMessageTemplateType>
    {
        public SystemMessageTemplateTypeFilterSpecification(
            int? AppliesToClassTypeID,
            byte? ChannelType
            )
            : base()

        {
            AddCriteria(new SystemMessageTemplateTypeFilterByAppliesToClassTypeID(AppliesToClassTypeID));
            AddCriteria(new SystemMessageTemplateTypeFilterByChannelType(ChannelType));
        }
    }
}
