﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    /// <summary>
    /// Include specification with options to include delivery history or link
    /// </summary>

    public class MessageParticipantInfoByEmployeeSpecification : BaseSpecification<MessageParticipantInfo>
    {
        public MessageParticipantInfoByEmployeeSpecification(int employeeID)
            : base()
        {
            AddCriteria(new FilterMessageParticipantInfoByEmployeeID(employeeID));
        }
    }
}
