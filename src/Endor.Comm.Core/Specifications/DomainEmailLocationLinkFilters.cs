﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.Comm.Core.Specifications
{

    /// <summary>
    /// Filter By DomainID
    /// </summary>
    public class DomainEmailLocationLinkFilterByDomainID : Criteria<DomainEmailLocationLink>
    {
        private readonly short _domainID;

        public DomainEmailLocationLinkFilterByDomainID(short domainID)
        {
            _domainID = domainID;
        }
        public override Expression<Func<DomainEmailLocationLink, bool>> ToExpression()
        {
            return t => (t.DomainID == _domainID);
        }
    }

    /// <summary>
    /// Filter By LocationID
    /// </summary>
    public class DomainEmailLocationLinkFilterByLocationID : Criteria<DomainEmailLocationLink>
    {
        private readonly byte _locationID;

        public DomainEmailLocationLinkFilterByLocationID(byte locationID)
        {
            _locationID = locationID;
        }
        public override Expression<Func<DomainEmailLocationLink, bool>> ToExpression()
        {
            return t => (t.LocationID == _locationID);
        }
    }
}
