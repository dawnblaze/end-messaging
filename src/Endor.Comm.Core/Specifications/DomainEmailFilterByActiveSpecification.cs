﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;

namespace Endor.Comm.Core.Specifications
{
    public class DomainEmailFilterByActiveSpecification : BaseSpecification<DomainEmail>
    {
        public DomainEmailFilterByActiveSpecification(bool? IsActive) : base()
        {
            AddCriteria(new DomainEmailFilterByIsActive(IsActive));
        }
    }
}
