﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class EmailAccountActiveFilterSpecification : BaseSpecification<EmailAccountData>
    {
        public EmailAccountActiveFilterSpecification(bool? IsActive) : base()
        {
            AddCriteria(new EmailAccountFilterByIsActive(IsActive));
        }
    }
}
