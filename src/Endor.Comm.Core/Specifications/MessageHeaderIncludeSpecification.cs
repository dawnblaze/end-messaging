﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    /// <summary>
    /// Include specification with options to include delivery history or link
    /// </summary>

    public class MessageHeaderIncludeSpecification : BaseSpecification<MessageHeader>
    {
        public MessageHeaderIncludeSpecification(short bid, int headerID, bool includeDeliveryHistory = false, bool includeLinks = false)
            : base()
        {
            AddCriteria(new FilterMessageHeaderByID(headerID));
            AddInclude("MessageBody");
            AddInclude("MessageBody.MessageParticipantInfos");
            //AddInclude("MessageBody.MessageParticipantInfos.EmployeeData");
            //AddInclude("MessageBody.MessageParticipantInfos.EmployeeTeam.EmployeeTeamLinks.Employee");
            if (includeDeliveryHistory)
            {
                AddInclude("MessageBody.MessageParticipantInfos.MessageDeliveryRecords");
            }
            if (includeLinks)
            {
                AddInclude("MessageBody.MessageObjectLinks");
            }
        }
    }
}
