﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class MessageHeadersFilterAndIncludeSpecification : BaseSpecification<MessageHeader>
    {
        public MessageHeadersFilterAndIncludeSpecification(short bid,
            MessageChannelType channelID, 
            short employeeID, 
            TrueFalseOnly includeRead, 
            TrueFalseOnly includeSent, 
            TrueFalseOnly includeDeleted,  
            bool includeDeliveryHistory, 
            bool includeLinks,
            int take = 100,
            int skip = 0)
            : base()
        
        {
            AddCriteria(new FilterByMessageChannelType(channelID));
            AddCriteria(new FilterMessageHeaderByEmployeeIDIfDefined(employeeID));
            AddCriteria(new FilterByRead(includeRead));
            AddCriteria(new FilterBySent(includeSent));
            AddCriteria(new FilterByDeleted(includeDeleted));
            AddInclude("MessageBody");
            AddInclude("MessageBody.MessageParticipantInfos");
            //AddInclude("MessageBody.MessageParticipantInfos.EmployeeData");
            //AddInclude("MessageBody.MessageParticipantInfos.EmployeeTeam.EmployeeTeamLinks.Employee");
            if (includeDeliveryHistory)
            {
                AddInclude("MessageBody.MessageParticipantInfos.MessageDeliveryRecords");
            }
            if (includeLinks)
            {
                AddInclude("MessageBody.MessageObjectLinks");
            }
            ApplyPaging(skip,take);

        }
    }
}
