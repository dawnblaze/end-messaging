﻿using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.Core.Specifications
{
    public class TransactionHeaderByIDAndTypeSpecification : BaseSpecification<TransactionHeader>
    {
        public TransactionHeaderByIDAndTypeSpecification(byte TransactionType, int ID)
        {
            AddCriteria(new TransactionHeaderByIDAndTypeFilter(TransactionType, ID));
        }      
    }

    /// <summary>
    /// Filter By EmployeeID
    /// </summary>
    public class TransactionHeaderByIDAndTypeFilter : Criteria<TransactionHeader>
    {
        private readonly byte _transactionType;
        private readonly int _ID;

        public TransactionHeaderByIDAndTypeFilter(byte TransactionType, int ID)
        {
            _ID = ID;
            _transactionType = TransactionType;
        }
        public override Expression<Func<TransactionHeader, bool>> ToExpression()
        {
            return t => t.ID == _ID && _transactionType == t.TransactionType;
        }
    }
}
