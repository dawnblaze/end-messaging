﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Common
{
    public class ObjectResult<TObject> : ResultBase where TObject : class
    {
        /// <summary>
        /// Result value
        /// </summary>
        public TObject Result { get; set; }

        public static ObjectResult<TObject> IsSuccess(TObject value) => new ObjectResult<TObject> { Success = true, Result = value };
        public static ObjectResult<TObject> IsFailure(string errorMessage, TObject value = null) => new ObjectResult<TObject> { Success = true, Result = value, Message = errorMessage };
    }
}
