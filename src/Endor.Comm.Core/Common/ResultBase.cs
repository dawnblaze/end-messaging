﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Common
{
    public abstract class ResultBase
    {

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// If there is an error
        /// </summary>
        public bool IsError { get { return !this.Success; } }
    }
}
