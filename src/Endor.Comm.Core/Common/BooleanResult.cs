﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Common
{
    public class BooleanResult : StructResultBase<bool>
    {
        public static BooleanResult IsSuccess => new BooleanResult()
        { 
            Result = true, 
            Success = true 
        };

        public static BooleanResult IsFailure(string errorMessage) => new BooleanResult() 
        { 
            Result = false, 
            Success = false,
            Message = errorMessage 
        };
    }
}
