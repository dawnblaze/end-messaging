﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Common
{
    public abstract class StructResultBase<TResultType> : ResultBase where TResultType : struct
    {
        /// <summary>
        /// Result value
        /// </summary>
        public TResultType? Result { get; set; }
    }
}
