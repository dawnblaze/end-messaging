﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Interfaces
{
    public interface IActionResponse
    {
        string message { get; set; }
        int? ID { get; set; }
    }
}
