﻿using Endor.Comm.Core.SharedKernel;

namespace Endor.Comm.Core.Interfaces
{
    public interface IDomainEventDispatcher
    {
        void Dispatch(BaseDomainEvent domainEvent);
    }
}