﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Interfaces
{
    public interface IModifyDateTimeTrackable
    {
        DateTime ModifiedDT { get; set; }
    }
}
