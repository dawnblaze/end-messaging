﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Interfaces
{
    /// <summary>
    /// Defines methods that iterate over child objects during Create/Update/Delete operations
    /// </summary>
    /// <remarks>
    /// it's interfaced so that the generic service doesn't need to know the type parameters of each child service
    /// </remarks>
    /// <typeparam name="pM">the Parent Model type</typeparam>
    /// <typeparam name="pI">the Parent ID type</typeparam>
    public interface IChildAssociation<pM>
        where pM : class, IEntity
    {
        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being added
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        Task ForEachChildDoBeforeCreateAsync(IRepository repository, pM parentModel);
        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being updated
        /// </summary>
        /// <param name="parentOldModel"></param>
        /// <param name="parentNewModel"></param>
        /// <returns></returns>
        Task ForEachChildDoBeforeUpdateAsync(IRepository repository, pM parentOldModel, pM parentNewModel);
        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being deleted
        /// </summary>
        /// <param name="parentModel"></param>
        Task ForEachChildDoBeforeDeleteAsync(IRepository repository, pM parentModel);
        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being cloned
        /// </summary>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        Task ForEachChildDoBeforeCloneAsync(IRepository repository, pM parentModel);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="cM"></typeparam>
    /// <typeparam name="pM"></typeparam>
    public interface IDoBeforeCreateUpdateWithParent<cM, pM>
        where cM : class
        where pM : class
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        void DoBeforeCreateWithParent(pM parent);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        void DoBeforeUpdateWithParent(IEntity oldParent, IEntity newParent);
    }

    /// <summary>
    /// Interface for CRUD CloneableChild Service
    /// </summary>
    public interface ICloneableChildCRUDService<pM, cM> : IEntity
        where pM : class
        where cM : class
    {
        /// <summary>
        /// Called before Clone of Parent
        /// </summary>
        Task<int> DoBeforeCloneForParent(pM parentModel, cM childModel);
    }
}
