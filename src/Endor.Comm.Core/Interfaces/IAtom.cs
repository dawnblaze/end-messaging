﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Interfaces
{
    public interface IAtom : IModifyDateTimeTrackable, IEntity
    {
        int ClassTypeID { get; }
        int ID { get; set; }

    }

    public interface IAtomService
    { 
        /// <summary>
        /// Called Before Model is updated
        /// </summary>
        Task DoBeforeUpdateAsync(IRepository repository, IAtom oldModel);

        /// <summary>
        /// Called before Model is Cloned
        /// </summary>
        Task DoBeforeCloneAsync(IRepository repository);

        /// <summary>
        /// Check if the model can be deleted
        /// </summary>
        /// <param name="repository"></param>
        /// <returns></returns>
        Task<Tuple<bool,string>> CanDeleteAsync(IRepository repository);
    }
}
