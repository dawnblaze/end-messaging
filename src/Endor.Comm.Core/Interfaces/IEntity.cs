﻿using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Interfaces
{
    /// <summary>
    /// Interface for CRUD Service
    /// </summary>
    public interface IEntity
    {
        short BID { get; set; }
    }

    public interface IEntityService
    { 
        /// <summary>
        /// Called before Model is Created
        /// </summary>
        Task DoBeforeCreateAsync(IRepository repository, short bid);

        /// <summary>
        /// Called Before Model is deleted
        /// </summary>
        Task DoBeforeDeleteAsync(IRepository repository);
    }
}
