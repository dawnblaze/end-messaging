﻿using Endor.Comm.Core.SharedKernel;

namespace Endor.Comm.Core.Interfaces
{
    public interface IHandle<T> where T : BaseDomainEvent
    {
        void Handle(T domainEvent);
    }
}