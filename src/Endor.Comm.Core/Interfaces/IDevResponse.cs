﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Interfaces
{
    public interface IDevResponse : IErrorResponse
    {
        object innerError { get; set; }
        int? userAccessType { get; set; }
        int? userID { get; set; }
    }
}
