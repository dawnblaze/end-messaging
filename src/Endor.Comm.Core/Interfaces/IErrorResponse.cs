﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Interfaces
{
    public interface IErrorResponse : IActionResponse
    {
        string code { get; set; }
        IErrorResponse[] details { get; set; }
    }
}
