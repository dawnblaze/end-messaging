﻿using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernal;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System;
using System.Runtime.InteropServices;
using System.Data;

namespace Endor.Comm.Core.Interfaces
{
    public interface IRepository : IReadOnlyRepository
    {
        /// <summary>
        /// Add an entity to a repository
        /// </summary>
        /// <typeparam name="T">BaseEntity</typeparam>
        /// <param name="entity">The entity to add</param>
        /// <returns></returns>
        Task<T> AddAsync<T>(T entity) where T : BaseEntity;
        /// <summary>
        /// Update an entity in the repository
        /// </summary>
        /// <typeparam name="T">BaseIdentityEntity</typeparam>
        /// <param name="entity">The entity to update</param>
        /// <returns></returns>
        Task<int> UpdateAsync<T>(T entity) where T : BaseIdentityEntity;
        /// <summary>
        /// Update several entities
        /// </summary>
        /// <typeparam name="T">BaseIdentityEntity</typeparam>
        /// <param name="entities">List of Entities</param>
        /// <returns></returns>
        Task<int> UpdateListAsync<T>(List<T> entities) where T : BaseIdentityEntity;
        /// <summary>
        /// Delete an entity from the repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        Task<int> DeleteAsync<T>(T entity) where T : BaseEntity;
        /// <summary>
        /// Delete List of entities from the repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task<int> DeleteListAsync<T>(List<T> entities) where T : BaseEntity;
        /// <summary>
        /// Clone an entity from the repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ID">The ID to clone from</param>
        /// <param name="nameSelector">Name Property Selector</param>
        /// <returns></returns>
        Task<T> CloneAsync<T>(int ID, Expression<Func<T, string>> nameSelector, [Optional]Type IDtype) where T : BaseIdentityEntity;
        /// <summary>
        /// Retrieve the next available ID value for a given class type
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="classtypeID"></param>
        /// <returns></returns>
        Task<int> RequestIDIntegerAsync(short bid, int classtypeID);
        
        /// <summary>
        /// Execute Raw SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters);

        Task<Tuple<bool, string>> CanDeleteAsync(BaseIdentityEntity entity);
    }

    public interface IReadOnlyRepository
    {
        /// <summary>
        /// Get Object by it's ID - the BID is automatically determined
        /// </summary>
        /// <typeparam name="T">BaseIdentitiyEntity</typeparam>
        /// <param name="id">ID Value</param>
        /// <returns></returns>
        Task<T> GetByIdAsync<T>(int id) where T : BaseIdentityEntity;
        /// <summary>
        /// Get Object by it's ID - the BID is automatically determined
        /// </summary>
        /// <typeparam name="T">BaseIdentitiyEntity</typeparam>
        /// <param name="id">ID Value</param>
        /// <returns></returns>
        Task<T> GetByShortIdAsync<T>(short id) where T : BaseIdentityEntity;
        /// <summary>
        /// Get Object by it's ID - the BID is automatically determined
        /// </summary>
        /// <typeparam name="T">BaseIdentitiyEntity</typeparam>
        /// <param name="id">ID Value</param>
        /// <returns></returns>
        Task<T> GetByByteIdAsync<T>(byte id) where T : BaseIdentityEntity;
        /// <summary>
        /// Get a List of object based upon the given specification
        /// </summary>
        /// <typeparam name="T">class</typeparam>
        /// <param name="spec">Specification to use</param>
        /// <returns></returns>
        Task<List<T>> ListAsync<T>() where T : class;

        /// <summary>
        /// Get a List of object based upon the given specification, Use this one for entities
        /// </summary>
        /// <typeparam name="T">class</typeparam>
        /// <param name="spec">Specification to use</param>
        /// <returns></returns>
        Task<List<T>> ListEntityAsync<T>(ISpecification<T> spec = null) where T : BaseEntity;

    }

    public interface IDapperReadOnlyRepository : IReadOnlyRepository
    {
        public void SetTableNameForClassType(int classTypeID);

        List<T> List<T>(ISpecification<T> spec) where T : class;

        IDbConnection Connection();
    }
}
