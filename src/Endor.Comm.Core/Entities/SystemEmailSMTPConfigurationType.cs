﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Entities
{
    public class SystemEmailSMTPConfigurationType
    {
        public byte ID { get; set; }

        public string Name { get; set; }

        public string SMTPAddress { get; set; }

        public short? SMTPPort { get; set; }

        public byte? SMTPSecurityType { get; set; }

        public bool? SMTPAuthenticateFirst { get; set; }

        public ICollection<DomainEmail> DomainEmails { get; set; }
    }
}
