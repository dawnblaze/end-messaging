﻿using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Entities
{
    public class SystemMessageTemplateType
    {
        public int AppliesToClassTypeID { get; set; }
        public byte ID { get; set; }
        public string Name { get; set; }
        public bool IsSystem { get; set; }
        public MessageChannelType ChannelType { get; set; }
    }
}
