﻿using Endor.Comm.Core.Events;
using Endor.Comm.Core.Exceptions;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.SharedKernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Entities
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1028292825/MessageBodyTemplate+Object
    /// </summary>
    public class MessageBodyTemplate : AggregateRoot<MessageBodyTemplate>
    {
        public override int ClassTypeID { get => 14220; set { } }

        public bool IsActive { get; set; }

        public int AppliesToClassTypeID { get; set; }

        public byte MessageTemplateType { get; set; }

        public MessageChannelType ChannelType { get; set; }

        public byte? LocationID { get; set; }

        public int? CompanyID { get; set; }

        public short? EmployeeID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(2048)]
        public string Subject { get; set; }

        public string Body { get; set; }

        public byte AttachedFileCount { get; set; }

        public string AttachedFileNames { get; set; }

        public int? SizeInKB { get; set; }

        public short SortIndex { get; set; }

        [JsonIgnore]
        public string MergeFieldList { get; set; }

        public bool HasAttachment { get; set; }

        public bool HasMergeFields { get; set; }

        public bool HasBody { get; set; }

        public SystemMessageTemplateType TemplateType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<MessageParticipantInfo> Participants
        {
            get
            {
                List<MessageParticipantInfo> participants = null;
                try
                {
                    if (!String.IsNullOrWhiteSpace(ParticipantInfoJSON))
                        participants = JsonConvert.DeserializeObject<List<MessageParticipantInfo>>(ParticipantInfoJSON);
                }
                catch (Exception ex)
                {
                    throw new InvalidCastException("Could not deserialize ParticipantInfoJSON.", ex);
                }

                return participants;
            }
            set
            {
                string json = JsonConvert.SerializeObject(value);
                ParticipantInfoJSON = json;
            }
        }

        [JsonIgnore]
        public string ParticipantInfoJSON { get; set; }

        protected override IChildAssociation<MessageBodyTemplate>[] GetChildAssociations()
        {
            return new IChildAssociation<MessageBodyTemplate>[0] { };
        }


        //Business Logic
        #region PUBLIC METHODS

        public void MarkAsActive()
        {
            _SetActiveState(true);
            Events.Add(new EntityUpdatedEvent(this));
        }

        public void MarkAsInActive()
        {
            _SetActiveState(false);
            Events.Add(new EntityUpdatedEvent(this));
        }


        /// <summary>
        /// Moves the specified record before the target
        /// </summary>
        /// <param name="target"></param>
        /// <returns>Tuple of (startindex of Template to adjust, endindex of Template to adjust, shiftamount)</returns>
        public Tuple<short, short, short> MoveTo(short targetSortIndex)
        {
            var src = this;
            
            if (src.SortIndex >= targetSortIndex)
            {
                short shiftAmount = 1;
                short startIndex = targetSortIndex;
                short endIndex = (short)(src.SortIndex-1);
                src.SortIndex = targetSortIndex;
                return new Tuple<short, short, short>(startIndex, endIndex, shiftAmount);
            }
            else
            {
                short shiftAmount = -1;
                short startIndex = (short)(src.SortIndex+1);
                short endIndex = targetSortIndex;
                src.SortIndex = targetSortIndex;
                return new Tuple<short, short, short>(startIndex, endIndex, shiftAmount);
            }
        }

        public void MoveSortIndexBy(short shiftAmount)
        {
            this.SortIndex += shiftAmount;
        }

        public bool CheckIsActive()
        {
            return IsActive;
        }

        public short CheckSortIndex()
        {
            return SortIndex;
        }

        public override async Task DoBeforeCreateAsync(IRepository repository, short bid)
        {
            await this.SetSortIndexAsync(repository);
            this.SetMergeFieldList();
            await base.DoBeforeCreateAsync(repository, bid);
        }

        public override async Task DoBeforeUpdateAsync(IRepository repository, IAtom oldModel)
        {
            this.SetMergeFieldList();
            await base.DoBeforeUpdateAsync(repository, oldModel);
        }

        #endregion

        #region PRIVATE METHODS

        private void _SetActiveState(bool isActive)
        {
            IsActive = isActive;
        }

        private async Task SetSortIndexAsync(IRepository repository)
        {
            this.SortIndex = (short)((await repository.ListAsync<MessageBodyTemplate>()).Count + 1);
        }

        /// <summary>
        /// Extacts the MergeFields from Subject, Body, and AttachedFileName and sets the MergeFieldList
        /// </summary>
        /// <param name="messageTemplate"></param>
        private void SetMergeFieldList()
        {
            string input = this.Subject + this.Body + this.AttachedFileNames;

            Dictionary<string, string> mf = new Dictionary<string, string>(StringComparer.InvariantCulture);
            var regex = new Regex("{{(.*?)}}");
            var matches = regex.Matches(input);
            foreach (Match match in matches)
            {
                // value with curly braces
                // var val = match.Value;

                // value without curly braces
                var val = match.Groups[1].Value;
                if (!mf.ContainsKey(val.ToUpperInvariant()))
                    mf.Add(val.ToUpperInvariant(), val);
            }
            this.MergeFieldList = String.Join(",", mf.Select(x => x.Value).ToArray());
        }

        public async Task ValidateParticipants(IDapperReadOnlyRepository repository)
        {
            List<string> invalidIDMessages = new List<string>();
            foreach (var participant in Participants)
            {
                if (participant.ContactID != null)
                {
                    repository.SetTableNameForClassType(ContactData.ClassTypeIDValue);
                    try
                    {
                        var contactCheckResult = await repository.GetByIdAsync<ContactData>(participant.ContactID.Value);
                        if (contactCheckResult == null)
                            invalidIDMessages.Add($"Contact with ID `{participant.ContactID}` not found. ");
                    }
                    catch (EntityNotFoundException ex)
                    {
                        invalidIDMessages.Add(ex.Message);
                    }
                }

                if (participant.EmployeeID != null)
                {
                    repository.SetTableNameForClassType(EmployeeData.ClassTypeIDValue);
                    try
                    {
                        var employeeCheckResult = await repository.GetByIdAsync<EmployeeData>(participant.EmployeeID.Value);
                        if (employeeCheckResult == null)
                            invalidIDMessages.Add($"Employee with ID `{participant.EmployeeID}` not found. ");
                    }
                    catch (EntityNotFoundException ex)
                    {
                        invalidIDMessages.Add(ex.Message);
                    }
                }

                if (participant.TeamID != null)
                {
                    repository.SetTableNameForClassType(EmployeeTeam.ClassTypeIDValue);
                    try
                    {
                        var teamCheckResult = await repository.GetByIdAsync<EmployeeTeam>(participant.TeamID.Value);
                        if (teamCheckResult == null)
                            invalidIDMessages.Add($"EmployeeTeam with ID `{participant.TeamID}` not found. ");
                    }
                    catch (EntityNotFoundException ex)
                    {
                        invalidIDMessages.Add(ex.Message);
                    }
                }
            }

            if (invalidIDMessages.Count > 0)
            {
                string errors = "";
                foreach (var m in invalidIDMessages)
                    errors += m;

                throw new EntityNotFoundException(errors);
            }
        }

        #endregion
    }
}
