﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Events;
using Endor.Comm.Core.SharedKernal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Entities
{
    public class MessageHeader : AggregateRoot<MessageHeader>
    {
        public override int ClassTypeID { get => 14111; set { } }

        public DateTime ReceivedDT { get; set; }

        public short EmployeeID { get; set; }

        public int ParticipantID { get; set; }

        public int BodyID { get; set; }

        public bool IsRead { get; set; }

        public DateTime? ReadDT { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOrExpiredDT { get; set; }

        public bool IsExpired { get; set; }

        [Required]
        public MessageChannelType Channels { get; set; }

        public bool InSentFolder { get; set; }

        //navprops
        [DefaultIncludes]
        public MessageBody MessageBody { get; set; }

        [DefaultIncludes]
        public MessageParticipantInfo ParticipantInfo { get; set; }

        //unmapped properties
        /// <summary>
        /// Not part of EF or any DB column. Only used for lucene indexing
        /// </summary>
        public string _messageBodyString { get; set; }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        protected override IChildAssociation<MessageHeader>[] GetChildAssociations()
        {
            return new IChildAssociation<MessageHeader>[2] {
                CreateChildAssociation<MessageHeader,MessageBody>((e) => e.MessageBody),
                CreateChildAssociation<MessageHeader,MessageParticipantInfo>((e) => e.ParticipantInfo),
            };
        }

        public MessageHeader(bool isRead, bool isDeleted)
        {
            _SetReadState(isRead);
            _SetDeletedState(isDeleted);
        }
        

        //CUD Helpers
        public override async Task DoBeforeCreateAsync(IRepository repository, short bid)
        {
            await base.DoBeforeCreateAsync(repository, bid);
            Events.Add(new EntityAddedEvent(this));
            Events.Add(new MessageHeaderCreatedEvent(this));
        }


        //Business Logic

        #region PUBLIC METHODS

        public void MarkAsRead()
        {
            _SetReadState(true);
            Events.Add(new EntityUpdatedEvent(this));
        }

        public void MarkAsUnread()
        {
            _SetReadState(false);
            Events.Add(new EntityUpdatedEvent(this));
        }


        public void MarkAsDeleted()
        {
            _SetDeletedState(true);
            Events.Add(new EntityDeletedEvent(this));
        }

        public void Undelete()
        {
            _SetDeletedState(false);
            Events.Add(new EntityUpdatedEvent(this));
        }

        public bool CheckIsRead()
        {
            return IsRead && ReadDT.HasValue;
        }

        public bool CheckIsDeleted()
        {
            return IsDeleted && DeletedOrExpiredDT.HasValue;
        }

        /// <summary>
        /// TODO: This is more complex than this, because it has a lot of logic envolved
        /// </summary>
        public void SaveDeliveryRecords()
        {
            this.AddDomainEvent(new EmailSentSuccessEvent(this));
        }

        #endregion



        #region PRIVATE METHODS
        private void _SetDeletedState(bool isDeleted)
        {
            IsDeleted = isDeleted;
            DeletedOrExpiredDT = isDeleted ? DateTime.UtcNow : (DateTime?)null;
        }

        private void _SetReadState(bool isRead)
        {
            IsRead = isRead;
            ReadDT = isRead ? DateTime.UtcNow : (DateTime?)null;
        }
        #endregion
    }
}
