﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Entities
{
    public class EmailAccountTeamLink : BaseEntity, IDoBeforeCreateUpdateWithParent<EmailAccountTeamLink, EmailAccountData>
    {
        /// <summary>
        /// Email Account ID
        /// </summary>
        public short EmailAccountID { get; set; }
        /// <summary>
        /// Team ID
        /// </summary>
        public int TeamID { get; set; }

        /// <summary>
        /// Navigation property to the EmailAccount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmailAccountData EmailAccount { get; set; }

        public void DoBeforeCreateWithParent(EmailAccountData parent)
        {
            this.EmailAccountID = parent.IDAsShort;
        }
        public void DoBeforeUpdateWithParent(IEntity oldParent, IEntity newParent)
        {

        }
    }
}
