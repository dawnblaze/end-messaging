﻿using Endor.Comm.Common.Documents;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.SharedKernel;
using Endor.Tenant;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Entities
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/845578283/MessageBody+Object
    /// </summary>
    public partial class MessageBody : BaseIdentityService<MessageBody> //, IParent<MessageBody>
    {

        public override int ClassTypeID { get => 14110; set { } }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(250)]
        public string Subject { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(100)]
        public string BodyFirstLine { get; set; }

        public bool HasBody { get; set; }
        public byte AttachedFileCount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AttachedFileNames { get; set; }

        public bool HasAttachment { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string MetaData { get; set; }

        public bool WasModified { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? SizeInKB { get; set; }

        //navprops
        public ICollection<MessageObjectLink> MessageObjectLinks { get; set; }
        public ICollection<MessageParticipantInfo> MessageParticipantInfos { get; set; }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        protected override IChildAssociation<MessageBody>[] GetChildAssociations()
        {
            var test = new IChildAssociation<MessageBody>[2] {
                CreateChildLinkAssociation<MessageBody,MessageObjectLink>((e) => e.MessageObjectLinks),
                CreateChildAssociation<MessageBody,MessageParticipantInfo>((e) => e.MessageParticipantInfos),
            };
            return test;
        }

        //Validations

        //CUD Helpers
        public override async Task DoBeforeCreateAsync(IRepository repository, short bid)
        {
            await base.DoBeforeCreateAsync(repository, bid);
        }

        /// <summary>
        /// Upload message body to document manager
        /// </summary>
        /// <param name="htmlString"></param>
        /// <param name="msgBody"></param>
        /// <returns></returns>
        public async Task SetHtmlMessageBody(string htmlString, ITenantDataCache cache, IDocumentManager docman)
        {
            await docman.UploadTextAsync("body.html", htmlString, this.ClassTypeID, this.ID, "text/html");
        }
    }
}
