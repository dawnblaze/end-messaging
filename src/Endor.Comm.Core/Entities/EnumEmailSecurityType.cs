﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Entities
{
    public class EnumEmailSecurityType
    {
        public EmailSecurityType ID { get; set; }

        public string Name { get; set; }
    }

    public enum EmailSecurityType : byte
    {
        None = 0,
        SSL = 1,
        TLS = 2
    }
}
