﻿using Endor.Comm.Core.Common;
using Endor.Comm.Core.Events;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.Services.Classes;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.SharedKernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Entities
{
    public class EmailAccountData : AggregateRoot<EmailAccountData>
    {
        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 1022.
        /// </summary>
        public override int ClassTypeID { get => 1023; set { } }
        public bool IsActive { get; set; }
        public EmailAccountStatus StatusType { get; set; }
        public short DomainEmailID { get; set; }
        public string UserName { get; set; }
        public string DomainName { get; set; }
        public bool IsPrivate { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? EmployeeID { get; set; }
        public string Credentials { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? CredentialsExpDT { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastEmailSuccessDT { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastEmailFailureDT { get; set; }
        public string EmailAddress { get; set; }
        public string AliasUserNames { get; set; }
        public string DisplayName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [DefaultIncludes]
        public DomainEmail DomainEmail { get; set; }

        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public EmployeeData Employee { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumEmailAccountStatus EnumEmailAccountStatus { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        [DefaultIncludes]
        public ICollection<EmailAccountTeamLink> EmailAccountTeamLinks { get; set; }

        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public ICollection<EmployeeTeam> Teams { get; set; }

        protected override IChildAssociation<EmailAccountData>[] GetChildAssociations()
        {
            return new IChildAssociation<EmailAccountData>[1] {
                CreateChildLinkAssociation<EmailAccountData,EmailAccountTeamLink>((e) => e.EmailAccountTeamLinks)
            };
        }

        
        #region PUBLIC METHODS

        public void MarkAsActive()
        {
            StatusType = EmailAccountStatus.Authorized;
            IsActive = true;
            Events.Add(new EntityUpdatedEvent(this));
        }

        public void MarkAsInActive()
        {
            StatusType = EmailAccountStatus.Inactive;
            IsActive = false;
            Events.Add(new EntityUpdatedEvent(this));
        }

        public bool CheckIsActive()
        {
            return IsActive || StatusType == EmailAccountStatus.Authorized;
        }

        public void UpdateStatusBasedOnCredentials(bool isCredentialsValid, bool isCreating = false)
        {
            if (Credentials == null)
                StatusType = EmailAccountStatus.Pending;
            else if (isCredentialsValid)
            {
                StatusType = EmailAccountStatus.Authorized;
                LastEmailSuccessDT = DateTime.UtcNow;
            }
            else
            {
                StatusType = EmailAccountStatus.Failed;
                LastEmailFailureDT = DateTime.UtcNow;
            }

            if (!isCreating)
                Events.Add(new EntityUpdatedEvent(this));
        }

       

        public async Task<BooleanResult> CanUpdateDomain(int domainEmailId, IRepository repository)
        {
            var domainEmail = await repository.GetByShortIdAsync<DomainEmail>(this.DomainEmailID);
            var message = "";
            bool IsValid = true;

            var isValidDomain = domainEmail.IsDomainValid();

            if (!isValidDomain.IsError)
                return isValidDomain;
            if (domainEmail == null)
            {
                message = "DomainEmailID " + DomainEmailID + " is not found in the [Domain.Email.Data]";
                IsValid = false;
            }

            string userEmail = string.Format("{0}@{1}", UserName, DomainName);
            var emailAccount = (await repository.ListAsync<EmailAccountData>()).Where(EA => EA.EmailAddress.ToLowerInvariant() == userEmail.ToLowerInvariant() && EA.ID != this.ID).FirstOrDefault();

            if (emailAccount != null)
            {
                IsValid = false;
                message = "Email address already exists.";
            }

            if (DomainName.ToLowerInvariant() != domainEmail.DomainName.ToLowerInvariant())
            {
                IsValid = false;
                message = "Invalid Request - The Domain Name must match the Domain in the Specified DomainEmail setup.";
            }
            if (IsValid)
            {
                return BooleanResult.IsSuccess;
            }
            else
            {
                return BooleanResult.IsFailure(message);
            }
        }

        public async Task<ObjectResult<EmailAccountCredentials>> IsValidCredentials(IEmailProvider emailProvider, string encryptionPassPhrase)
        {
            if (this.DomainEmail != null)
            {
                //This is needed only for Gmail or Microsoft
                var emailAccountCredentials = JsonConvert.DeserializeObject<EmailAccountCredentials>(this.Credentials ?? string.Empty) ?? new EmailAccountCredentials();

                var validationResult = await emailProvider.Validate(emailAccountCredentials);

                emailAccountCredentials.Password = StringEncryptor.Encrypt(emailAccountCredentials.Password, encryptionPassPhrase);

                this.Credentials = JsonConvert.SerializeObject(emailAccountCredentials);

                return validationResult.Success ?
                    ObjectResult<EmailAccountCredentials>.IsSuccess(emailAccountCredentials) :
                    ObjectResult<EmailAccountCredentials>.IsFailure($"The requested credentials is not valid with the domain. {validationResult.Message}", emailAccountCredentials);
            }

            return ObjectResult<EmailAccountCredentials>.IsFailure("The requested domain is invalid.");
        }

        public async Task<BooleanResult> SendEmail(EmailMessage message, IEmailProvider provider, string encryptionPassPhrase)
        {
            if (string.IsNullOrWhiteSpace(Credentials))
                return BooleanResult.IsFailure("Email Account Credentials are not valid");

            var credentials = JsonConvert.DeserializeObject<EmailAccountCredentials>(this.Credentials);

            var providerValidationResult = await provider.Validate(credentials);

            if (providerValidationResult.IsError)
                return BooleanResult.IsFailure(providerValidationResult.Message);

            await provider.Send(new EmailSendInfo(message, credentials, encryptionPassPhrase));

            return BooleanResult.IsSuccess;
        }

        public async Task<BooleanResult> IsValidDomain(IRepository repository)
        {
            var domainEmail = await repository.GetByShortIdAsync<DomainEmail>(this.DomainEmailID);
            var message = "";
            bool IsValid = true;

            var isValidDomain = domainEmail.IsDomainValid();

            if (!isValidDomain.IsError)
                return isValidDomain;

            if (domainEmail == null)
            {
                message = "DomainEmailID " + DomainEmailID + " is not found in the [Domain.Email.Data]";
                IsValid = false;
            }

            string userEmail = string.Format("{0}@{1}", UserName, DomainName);
            var emailAccount = (await repository.ListAsync<EmailAccountData>()).Where(EA => EA.EmailAddress.ToLowerInvariant() == userEmail.ToLowerInvariant()).FirstOrDefault();

            if (emailAccount != null)
            {
                IsValid = false;
                message = "Email address already exists.";
            }

            if (DomainName.ToLowerInvariant() != domainEmail.DomainName.ToLowerInvariant())
            {
                IsValid = false;
                message = "Invalid Request - The Domain Name must match the Domain in the Specified DomainEmail setup.";
            }

            if (IsValid)
            {
                return BooleanResult.IsSuccess;
            }
            else
            {
                return BooleanResult.IsFailure(message);
            }
        }

        public async Task<BooleanResult> IsValidEmailAccountForDomain(IRepository repository)
        {
            var domainEmail = await repository.GetByShortIdAsync<DomainEmail>(this.DomainEmailID);

            var isValidDomain = domainEmail.IsDomainValid();

            if (!isValidDomain.Result.Value)
                return isValidDomain;

            if (domainEmail == null)
                return BooleanResult.IsFailure($"DomainEmailID {DomainEmailID} is not found in the [Domain.Email.Data]");

            var userEmail = string.Format("{0}@{1}", UserName, DomainName);
            var emailAccount = (await repository.ListAsync<EmailAccountData>()).Where(EA => EA.EmailAddress.ToLowerInvariant() == userEmail.ToLowerInvariant()).FirstOrDefault();

            if (emailAccount != null)
                return BooleanResult.IsFailure("Email address already exists.");

            if (DomainName.ToLowerInvariant() != domainEmail.DomainName.ToLowerInvariant())
                return BooleanResult.IsFailure("The Domain Name must match the Domain in the Specified DomainEmail setup.");

            return BooleanResult.IsSuccess;
        }

        public async Task CheckIfValid(IDapperReadOnlyRepository repository)
        {
            var isValid = ((this.EmployeeID != null) ^ ((this.EmailAccountTeamLinks != null) && (this.EmailAccountTeamLinks.Count > 0)));
            if (!isValid)
            {
                this.AddError("Email Account Error", "Email Account must define either an EmployeeID or EmailAccountTeamLinks");
            }

            if (this.IsValid)
            {
                if (this.EmployeeID != null)
                {
                    repository.SetTableNameForClassType(EmployeeData.ClassTypeIDValue);
                    var existingEmployee = await repository.GetByShortIdAsync<EmployeeData>((short)this.EmployeeID);
                    if (existingEmployee == null)
                    {
                        this.AddError("Email Account Error", $"Employee {this.EmployeeID} does not exist");
                    }
                }
                else
                {
                    repository.SetTableNameForClassType(EmployeeTeam.ClassTypeIDValue);
                    foreach (var employeeAccountTeamLink in this.EmailAccountTeamLinks)
                    {
                        var existingteams = await repository.GetByIdAsync<EmployeeTeam>(employeeAccountTeamLink.TeamID);
                        if (existingteams == null)
                        {
                            this.AddError("Email Account Error", $"TeamId  {employeeAccountTeamLink.TeamID} does not exist");
                        }
                    }
                }
            }
        }
        #endregion
    }
}
