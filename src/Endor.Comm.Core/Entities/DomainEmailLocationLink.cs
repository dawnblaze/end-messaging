﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Entities
{
    public class DomainEmailLocationLink : BaseEntity, IDoBeforeCreateUpdateWithParent<DomainEmailLocationLink, DomainEmail>
    {
        /// <summary>
        /// The ID of the DomainEmail Object being linked.
        /// </summary>
        public short DomainID { get; set; }

        /// <summary>
        /// The ID of the Location Object being linked.
        /// </summary>
        public byte LocationID { get; set; }

        public virtual DomainEmail DomainEmail { get; set; }

        //public LocationData Location { get; set; }

        public void DoBeforeCreateWithParent(DomainEmail parent)
        {
            this.DomainID = parent.IDAsShort;
        }
        public void DoBeforeUpdateWithParent(IEntity oldParent, IEntity newParent)
        {

        }
    }
}
