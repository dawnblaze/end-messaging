﻿using Endor.Comm.Core.Common;
using Endor.Comm.Core.Events;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.Services;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Entities
{
    public class DomainEmail : AggregateRoot<DomainEmail>
    {
        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 1022.
        /// </summary>
        public override int ClassTypeID { get => 1022; set { } }

        /// <summary>
        /// The date the domain was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Flag indicating if the record is Active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// The email domain name, without the '@' at the front.
        /// </summary>
        public string DomainName { get; set; }

        /// <summary>
        /// The Email ProviderType enum for this domain
        /// </summary>
        public EmailProviderType ProviderType { get; set; }

        /// <summary>
        /// The SMTPConfigurationType enum for this email
        /// This is only used when the ProviderType is Custom SMTP.
        /// </summary>
        public byte? SMTPConfigurationType { get; set; }

        /// <summary>
        /// The SMTP Address (IP or URL) for the SMTP Provider.
        /// This is only used when the ProviderType is Custom SMTP but it is required in that case.
        /// </summary>
        public string SMTPAddress { get; set; }

        /// <summary>
        /// The SMTP Port.
        ///
        /// This is only used when the ProviderType is Custom SMTP but it is required in that case.
        /// </summary>
        public short? SMTPPort { get; set; }

        /// <summary>
        /// The SMTPSecurityType enum for the email provider
        /// 
        /// This is only used when the ProviderType is Custom SMTP but it is required in that case.
        /// </summary>
        public EmailSecurityType? SMTPSecurityType { get; set; }

        /// <summary>
        /// Flag that indicates if the SMTP connection should authenticate first before trying to retrieve email.
        /// This is only used when the ProviderType is Custom SMTP but it is required in that case.
        /// </summary>
        public bool? SMTPAuthenticateFirst { get; set; }

        /// <summary>
        /// The last time verification was attempted on this domain.
        /// </summary>
        public DateTime? LastVerificationAttemptDT { get; set; }

        /// <summary>
        /// Flag indicating if the last verification attempt was successful.	
        /// </summary>
        public bool? LastVerificationSuccess { get; set; }

        /// <summary>
        /// The Result Text for the last verification attempt.
        /// </summary>
        public string LastVerificationResult { get; set; }

        /// <summary>
        /// The DateTime of the last successful verification for this domain.
        /// </summary>
        public DateTime? LastVerifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if this Email Domain is valid for all locations or must be individually specified.
        /// </summary>
        public bool IsForAllLocations { get; set; }

        [DefaultIncludes]
        public virtual List<DomainEmailLocationLink> LocationLinks { get; set; }

        public virtual EnumEmailSecurityType SecurityType { get; set; }

        public virtual EnumEmailProviderType Provider { get; set; }

        public virtual SystemEmailSMTPConfigurationType ConfigurationType { get; set; }

        protected override IChildAssociation<DomainEmail>[] GetChildAssociations()
        {
            return new IChildAssociation<DomainEmail>[1] { 
                CreateChildLinkAssociation<DomainEmail,DomainEmailLocationLink>((e) => e.LocationLinks)
            };
        }

        //Business Logic

        #region PUBLIC METHODS

        public void MarkAsActive()
        {
            _SetActiveState(true);
            Events.Add(new EntityUpdatedEvent(this));
        }

        public void MarkAsInActive()
        {
            _SetActiveState(false);
            Events.Add(new EntityUpdatedEvent(this));
        }

        public bool CheckIsActive()
        {
            return IsActive;
        }

        public BooleanResult IsDomainValid()
        {
            if (IsActive == false)
                return BooleanResult.IsFailure("The DomainName supplied is inactive.");
            return BooleanResult.IsSuccess;
        }

        public override Task DoBeforeCreateAsync(IRepository repository, short bid)
        {
            this.CreatedDate = DateTime.UtcNow;
            return base.DoBeforeCreateAsync(repository, bid);
        }

        public async override Task DoBeforeDeleteAsync(IRepository repository)
        {
            //need to do this because delete is incorrectly set to restrict
            var links = this.LocationLinks.ToArray();
            var linkCount = this.LocationLinks.Count;
            for (int count = 0; count < linkCount; count++)
            {
                var link = links[count];
                await repository.DeleteAsync<DomainEmailLocationLink>(link);
            }
            await base.DoBeforeDeleteAsync(repository);
        }

        #endregion

        #region PRIVATE METHODS

        private void _SetActiveState(bool isActive)
        {
            IsActive = isActive;
        }

        #endregion
    }
}
