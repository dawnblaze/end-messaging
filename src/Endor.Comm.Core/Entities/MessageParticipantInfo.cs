﻿using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace Endor.Comm.Core.Entities
{
    public class MessageParticipantInfo : BaseIdentityService<MessageParticipantInfo>
    {
        public override int ClassTypeID { get => 14112; set { } }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int BodyID { get; set; }
        [Required]
        public MessageParticipantRoleType ParticipantRoleType { get; set; }
        [Required]
        public MessageChannelType Channel { get; set; }
        public string UserName { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsMergeField { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public short? EmployeeID { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int? TeamID { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int? ContactID { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime? DeliveredDT { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? IsDelivered { get; set; }

        //navprops
        [DefaultIncludes]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MessageBody MesssageBody { get; set; }
        [DefaultIncludes]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<MessageDeliveryRecord> MessageDeliveryRecords { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<MessageHeader> MessageHeaders { get; set; }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        protected override IChildAssociation<MessageParticipantInfo>[] GetChildAssociations()
        {
            return new IChildAssociation<MessageParticipantInfo>[1] {
                CreateChildAssociation<MessageParticipantInfo,MessageDeliveryRecord>((e) => e.MessageDeliveryRecords),
            };
        }
    }
}
