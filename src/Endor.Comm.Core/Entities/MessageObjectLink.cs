﻿using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Endor.Comm.Core.Entities
{
    public class MessageObjectLink : BaseEntity
    {
        public int ID { get; set; }

        public int BodyID { get; set; }
        public int LinkedObjectClassTypeID { get; set; }
        public int LinkedObjectID { get; set; }
        public bool IsSourceOfMessage { get; set; }

        [StringLength(100)]
        public string DisplayText { get; set; }

    }
}
