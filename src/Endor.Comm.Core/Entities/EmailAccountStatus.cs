﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Entities
{
    public enum EmailAccountStatus : byte
    {
        Pending = 1,
        Authorized = 2,
        Failed = 3,
        Expired = 4,
        Inactive = 5
    }

    public partial class EnumEmailAccountStatus
    {
        public EmailAccountStatus ID { get; set; }

        public string Name { get; set; }
    }
}
