﻿using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Endor.Comm.Core.Entities
{
    public class MessageDeliveryRecord : BaseIdentityEntity
    {
        public override int ClassTypeID { get => 14114; set { } }
        public int ParticipantID { get; set; }
        public short AttemptNumber { get; set; }
        public bool IsPending { get; set; }
        public DateTime? ScheduledDT { get; set; }
        public DateTime? AttemptedDT { get; set; }
        public bool WasSuccessful { get; set; }
        public string FailureMessage { get; set; }
        public string MetaData { get; set; }

    }
}
