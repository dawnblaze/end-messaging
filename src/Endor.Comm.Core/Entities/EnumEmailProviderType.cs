﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Entities
{
    public class EnumEmailProviderType
    {
        public EmailProviderType ID { get; set; }

        public string Name { get; set; }

    }

    public enum EmailProviderType : byte
    {
        None = 0,
        GoogleGSuite = 1,
        Microsoft365 = 2,
        CustomSMTP = 3
    }
}
