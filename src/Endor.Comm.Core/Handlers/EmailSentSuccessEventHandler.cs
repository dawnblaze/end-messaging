﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Events;
using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Comm.Core.Handlers
{
    public class EmailSentSuccessEventHandler : IHandle<EmailSentSuccessEvent>
    {
        private readonly IRepository _repository;

        public EmailSentSuccessEventHandler(IRepository repository)
        {
            this._repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public void Handle(EmailSentSuccessEvent domainEvent)
        {
            var participants = (domainEvent.MessageHeader.MessageBody.MessageParticipantInfos?.ToList() ?? new List<MessageParticipantInfo>());
            
            participants.Add(domainEvent.MessageHeader.ParticipantInfo);

            var avaiableDeliveryTypes = new MessageParticipantRoleType[] { 
                MessageParticipantRoleType.To, 
                MessageParticipantRoleType.CC, 
                MessageParticipantRoleType.BCC
            };

            participants = participants.Where(a => avaiableDeliveryTypes.Contains(a.ParticipantRoleType)).ToList();

            if (domainEvent.MessageHeader.MessageBody.MessageParticipantInfos.Any())

            foreach (var participant in participants)
            {
                _repository.AddAsync(new MessageDeliveryRecord
                {
                    BID = domainEvent.MessageHeader.BID,
                    ParticipantID = participant.ID,
                    AttemptNumber = 1,
                    AttemptedDT = domainEvent.DateOccurred,
                    ModifiedDT = domainEvent.DateOccurred,
                    WasSuccessful = true,
                    IsPending = false
                }).ConfigureAwait(false).GetAwaiter().GetResult();
            }
        }
    }
}
