﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Events;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using System;
using System.Collections.Generic;

namespace Endor.Comm.Core.Handlers
{
    public class EntityAddedRefreshEventHandler : BaseEntityChangeEventHandler, IHandle<EntityAddedEvent>
    {

        public EntityAddedRefreshEventHandler(ITaskQueuer taskQueuer, IRTMPushClient rtmClient, RemoteLogger logger)
            : base(taskQueuer, rtmClient, logger)
        {

        }

        public async void Handle(EntityAddedEvent entryAddedEvent)
        {
            try
            {
                await _rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnCreate(entryAddedEvent.entity) });
            }
            catch (Exception ex)
            {
                await _logger.Error(entryAddedEvent.entity.BID, "Refresh failed after Add", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public virtual List<RefreshEntity> DoGetRefreshMessagesOnCreate(BaseIdentityEntity newModel)
        {
            return new List<RefreshEntity>()
            {
                new RefreshEntity()
                {
                    ClasstypeID = newModel.ClassTypeID,
                    DateTime = newModel.ModifiedDT,
                    RefreshMessageType = RefreshMessageType.New,
                    BID = newModel.BID,
                }
            };
        }
    }
}
