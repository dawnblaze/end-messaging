﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Events;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Handlers
{
    public class MessageHeaderCreatedEventHandler : BaseEntityChangeEventHandler, IHandle<MessageHeaderCreatedEvent>
    {
        public MessageHeaderCreatedEventHandler(ITaskQueuer taskQueuer, IRTMPushClient rtmClient, RemoteLogger logger)
            : base(taskQueuer, rtmClient, logger)
        {

        }

        public async void Handle(MessageHeaderCreatedEvent messageHeaderCreatedEvent)
        {
            try
            {
                var header = (MessageHeader)messageHeaderCreatedEvent.entity;
                if (!header.InSentFolder && header.Channels!= MessageChannelType.Email) //only do for To addresses
                    await _rtmClient.SendEmployeeMessage(new Endor.RTM.Models.EmployeeMessage()
                    {
                        BID = header.BID,
                        EmployeeID = header.EmployeeID,
                        Message = "You've got mail!",
                        Data = JsonConvert.SerializeObject(new
                        {
                            Subject = header.MessageBody?.Subject
                        })
                    });
            }
            catch (Exception ex)
            {
                await _logger.Error(messageHeaderCreatedEvent.entity.BID, "Refresh failed after Add", ex);
            }
        }
    }
}
