﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Events;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Handlers
{
    public class EntityUpdatedIndexModelEventHandler : BaseEntityChangeEventHandler, IHandle<EntityUpdatedEvent>
    {

        public EntityUpdatedIndexModelEventHandler(ITaskQueuer taskQueuer, IRTMPushClient rtmClient, RemoteLogger logger)
            : base(taskQueuer, rtmClient, logger)
        {

        }

        public async void Handle(EntityUpdatedEvent entryAddedEvent)
        {
            try {
                await QueueIndexForModel(entryAddedEvent.entity);
            }
            catch (Exception ex)
            {
                await _logger.Error(entryAddedEvent.entity.BID, "Indexing failed after Update", ex);
            }
        }

    }
}
