﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Events;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using System;
using System.Collections.Generic;

namespace Endor.Comm.Core.Handlers
{
    public class EntityUpdatedRefreshEventHandler : BaseEntityChangeEventHandler, IHandle<EntityUpdatedEvent>
    {
        public EntityUpdatedRefreshEventHandler(ITaskQueuer taskQueuer, IRTMPushClient rtmClient, RemoteLogger logger)
            : base(taskQueuer, rtmClient, logger)
        {

        }

        public async void Handle(EntityUpdatedEvent entryAddedEvent)
        {
            try
            {
                await _rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnUpdate(entryAddedEvent.entity) });
            }
            catch (Exception ex)
            {
                await _logger.Error(entryAddedEvent.entity.BID, "Refresh failed after Update", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="update"></param>
        /// <returns></returns>
        public virtual List<RefreshEntity> DoGetRefreshMessagesOnUpdate(BaseIdentityEntity update)
        {
            return new List<RefreshEntity>()
            {
                new RefreshEntity()
                {
                    ClasstypeID = update.ClassTypeID,
                    ID = Convert.ToInt32(update.ID),
                    DateTime = update.ModifiedDT,
                    RefreshMessageType = RefreshMessageType.Change,
                    BID = update.BID,
                },
                new RefreshEntity()
                {
                    ClasstypeID = update.ClassTypeID,
                    DateTime = update.ModifiedDT,
                    RefreshMessageType = RefreshMessageType.Change,
                    BID = update.BID
                }
            };
        }
    }
}
