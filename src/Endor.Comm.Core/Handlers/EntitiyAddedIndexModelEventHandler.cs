﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Events;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Handlers
{
    public class EntitiyAddedIndexModelEventHandler : BaseEntityChangeEventHandler,IHandle<EntityAddedEvent>
    {
        
        public EntitiyAddedIndexModelEventHandler(ITaskQueuer taskQueuer, IRTMPushClient rtmClient, RemoteLogger logger)
            : base(taskQueuer, rtmClient, logger)
        {

        }

        public async void Handle(EntityAddedEvent entryAddedEvent)
        {
            try {
                await QueueIndexForModel(entryAddedEvent.entity);
            }
            catch (Exception ex)
            {
                await _logger.Error(entryAddedEvent.entity.BID, "Indexing failed after Add", ex);
            }
        }

    }
}
