﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Events;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using System;
using System.Collections.Generic;

namespace Endor.Comm.Core.Handlers
{
    public class EntityDeletedRefreshEventHandler : BaseEntityChangeEventHandler, IHandle<EntityDeletedEvent>
    {
        public EntityDeletedRefreshEventHandler(ITaskQueuer taskQueuer, IRTMPushClient rtmClient, RemoteLogger logger)
            : base(taskQueuer, rtmClient, logger)
        {

        }

        public async void Handle(EntityDeletedEvent entryAddedEvent)
        {
            try
            {
                await _rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnDelete(entryAddedEvent.entity) });
            }
            catch (Exception ex)
            {
                await _logger.Error(entryAddedEvent.entity.BID, "Refresh failed after Delete", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public virtual List<RefreshEntity> DoGetRefreshMessagesOnDelete(BaseIdentityEntity deleted)
        {
            return new List<RefreshEntity>()
            {
                new RefreshEntity()
                {
                    BID = deleted.BID,
                    ClasstypeID = deleted.ClassTypeID,
                    ID = Convert.ToInt32(deleted.ID),
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Delete,
                },
                new RefreshEntity()
                {
                    BID = deleted.BID,
                    ClasstypeID = deleted.ClassTypeID,
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Delete,
                }
            };
        }

    }
}
