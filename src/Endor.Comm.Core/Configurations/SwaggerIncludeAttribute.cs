﻿using System;

namespace Endor.Comm.Core
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SwaggerIncludeAttribute : Attribute
    {
    }
}
