﻿using Endor.Tenant;

namespace Endor.Comm.Core.Configurations
{
    /// <summary>
    /// Endor Options
    /// </summary>
    public class EndorOptions : IEnvironmentOptions
    {
        /// <summary>
        /// Origin of the Auth
        /// </summary>
        public string AuthOrigin { get; set; }
        /// <summary>
        /// Tenant Secret
        /// </summary>
        public string TenantSecret { get; set; }
        /// <summary>
        /// Messaging Server URL
        /// </summary>
        public string MessagingServerURL { get; set; }
        /// <summary>
        /// Peer Host Server URL
        /// </summary>
        public string PeerHostUrl { get; set; }
        /// <summary>
        /// Client Options
        /// </summary>    
        public ClientOptions Client { get; set; }
        /// <summary>
        /// Encryption Passphrase (256-bit encryption)
        /// </summary>    
        public string EncryptionPassPhrase { get; set; }

    }

    /// <summary>
    /// Client Options
    /// </summary>
    public class ClientOptions
    {
        /// <summary>
        /// Google API Key
        /// </summary>
        public string GoogleAPIKey { get; set; }
        /// <summary>
        /// Used to define whether to use assisted address lookup
        /// </summary>
        public bool UseAssistedAddressLookup { get; set; }
    }
}
