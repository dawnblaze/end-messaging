﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Services.Classes
{
    public class EmailAddress
    {
        public EmailAddress(string name, string address)
        {
            Name = name;
            Address = address;
        }

        public string Name { get; set; }
        public string Address { get; set; }
    }
}
