﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Services.Classes
{
    public class EmailAccountCredentials
    {
        /// <summary>
        /// access token
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// refresh token
        /// </summary>
        public string RefreshToken { get; set; }
        /// <summary>
        /// expiration date time
        /// </summary>
        public DateTime ExpirationDT { get; set; }
        /// <summary>
        /// Passphase for SMTP Auth encrypted in AES
        /// </summary>
        public string Password { get; set; }
    }
}
