﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Services.Classes
{
    public class EmailSendInfo
    {
        public EmailSendInfo(EmailMessage message, EmailAccountCredentials credentials, string encryptionPassPhrase)
        {
            Message = message;
            Credentials = credentials;
            EncryptionPassPhrase = encryptionPassPhrase;
        }

        public EmailMessage Message { get; set; }
        public EmailAccountCredentials Credentials { get; set; }
        public string EncryptionPassPhrase { get; set; }
    }
}
