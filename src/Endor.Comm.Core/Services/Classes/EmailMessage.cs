﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Services.Classes
{
    public class EmailMessage
    {
        public EmailMessage()
        {
            ToAddresses = new List<EmailAddress>();
            FromAddresses = new List<EmailAddress>();
        }

        public List<EmailAddress> ToAddresses { get; private set; }
        public List<EmailAddress> FromAddresses { get; private set; }
        public string EmailAccountEmailAddress { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}
