﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Services.Interfaces
{
    public interface ICommunicationProvider<TSendInfo> where TSendInfo : class
    {
        Task Send(TSendInfo sendInfo);
    }
}
