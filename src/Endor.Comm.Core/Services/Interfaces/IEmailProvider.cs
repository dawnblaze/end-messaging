﻿using Endor.Comm.Core.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Services.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Services.Interfaces
{
    public interface IEmailProvider : ICommunicationProvider<EmailSendInfo>
    {
        Task<BooleanResult> Validate(EmailAccountCredentials credentials);
    }
}
