﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.Specifications;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Endor.Comm.Common.Documents;

namespace Endor.Comm.Core.Services
{
    public class MessageBreakdownService
    {
        public MessageHeader MessageHeader { get; set; } = new MessageHeader(isRead: false, isDeleted: false);
        public MessageBody MessageBody { get; set; } = new MessageBody();

        public string BodyString { get; set; } = "";

        public ICollection<MessageParticipantInfo> MessageParticipantInfos { get; set; }
        public ICollection<MessageObjectLink> MessageObjectLinks { get; set; }
        public ICollection<MessageDeliveryRecord> MessageDeliveryRecords { get; set; }

        public async Task PopulateMessageBreakdownWithMessageHeader(MessageHeader msg, IDocumentManager documentManager)
        {
            MessageHeader = msg;
            MessageBody = msg.MessageBody;
            MessageParticipantInfos = msg.MessageBody.MessageParticipantInfos;
            MessageObjectLinks = msg.MessageBody.MessageObjectLinks;
            MessageDeliveryRecords = msg.MessageBody.MessageParticipantInfos
                                            .Where(p => p.MessageDeliveryRecords != null)
                                            .SelectMany(p => p.MessageDeliveryRecords)
                                            .ToList();
            BodyString = await GetHtmlMessageBody(msg, documentManager);
        }

        // <summary>
        /// GetHtmlMessageBody
        /// </summary>
        /// <param name="messageBodyID"></param>
        /// <returns></returns>
        public async Task<string> GetHtmlMessageBody(MessageHeader msg, IDocumentManager documentManager)
        {
            var bodyHtml = (await documentManager.GetDocumentsAsync()).Where(doc => doc.Name.EndsWith("body.html")).FirstOrDefault();
            if (bodyHtml != null)
            {
                var str = await documentManager.ReadTextAsync(bodyHtml);
                return str;
            }

            return "";
        }

        /// <summary>
        /// Get all MessageHeaders for the message breakdown 
        /// It requires that MessageBody.BID has a correct value
        /// </summary>
        /// <returns></returns>
        public async Task<List<MessageHeader>> GetAllHeadersAsync(List<EmployeeTeam> employeeTeamWithEmployeeTeamLinks)
        {
            var allHeaders = new List<MessageHeader>();
            var employeeHeaders = GetEmployeeHeaders();
            var teamHeaders = await GetTeamHeadersAsync(employeeTeamWithEmployeeTeamLinks);
            // get the MessageParticipantRoleType.From participant
            var headerFrom = employeeHeaders.Where(h => h.InSentFolder).FirstOrDefault();

            // get all the MessageParticipantRoleType.To participants
            var headersTo = employeeHeaders.Where(h => !h.InSentFolder).ToList();
            // add headersTo and teamHeaders to allHeaders
            allHeaders.AddRange(headersTo);
            allHeaders.AddRange(teamHeaders);
            // remove duplicate Employees on allHeaders
            allHeaders = allHeaders.GroupBy(h => h.EmployeeID).Select(h => h.First()).ToList();

            // Insert headerFrom to the list of headers
            if (headerFrom!=null)
                allHeaders.Add(headerFrom);
            
            return allHeaders;
        }

        private async Task<List<MessageHeader>> GetTeamHeadersAsync(List<EmployeeTeam> employeeTeamWithEmployeeTeamLinks)
        {
            // get team participants
            var teamParticipants = this.MessageBody.MessageParticipantInfos.Where(p => p.TeamID.HasValue);
            var teamMessageToParticipants = teamParticipants.Where(p => p.ParticipantRoleType == MessageParticipantRoleType.To).ToList();
            // get team participants IDs
            var teamParticipantIDs = teamParticipants.Select(p => p.TeamID).ToList();
            // get EmployeeTeam Data filtered by the team ID on the team Participants
            
            var teamHeaders = new List<MessageHeader>();
            var teamParticipantData = employeeTeamWithEmployeeTeamLinks.Where(et => teamParticipantIDs.Contains(et.ID));
            foreach (var team in teamParticipants)
            {
                // get employeeTeam Data filtered by teamID
                var employeeTeam = teamParticipantData.Where(tpd => tpd.ID == team.TeamID).FirstOrDefault();

                // check if employeeTeam is not null and if employeeTeam has employee links
                if (employeeTeam != null && employeeTeam.EmployeeTeamLinks.Count > 0)
                {
                    foreach (var employee in employeeTeam.EmployeeTeamLinks)
                    {
                        teamHeaders.Add(new MessageHeader(isRead: false, isDeleted: false)
                        {
                            BID = this.MessageBody.BID,
                            ClassTypeID = MessageHeader.ClassTypeID,
                            EmployeeID = (short)employee.EmployeeID,
                            Channels = team.Channel,
                            InSentFolder = team.ParticipantRoleType == MessageParticipantRoleType.From,
                            IsRead = false,
                            ParticipantID = team.ID,
                            ModifiedDT = DateTime.UtcNow,
                            ReceivedDT = DateTime.UtcNow,
                            BodyID = this.MessageBody.ID
                        });
                    }
                }
            }
            return await Task.FromResult(teamHeaders);
        }

        private List<MessageHeader> GetEmployeeHeaders()
        {
            var employeeMessageParticipants = this.MessageBody.MessageParticipantInfos.Where(p => p.EmployeeID.HasValue && p.ParticipantRoleType == MessageParticipantRoleType.To).ToList();
            //create headers filtered by EmployeeID
            //handled differently instead of using child services
            //already tried using headerservice as child link of participantinfoservice but bodyID is still `0` prior to parent's creation
            var employeeHeaders = this.MessageBody.MessageParticipantInfos.Where(p => p.EmployeeID.HasValue)
                .Select(p => new MessageHeader(isRead: false,isDeleted: false)
            {
                BID = this.MessageBody.BID,
                ClassTypeID = MessageHeader.ClassTypeID,
                EmployeeID = (short)p.EmployeeID,
                Channels = p.Channel,
                InSentFolder = p.ParticipantRoleType == MessageParticipantRoleType.From,
                ParticipantID = p.ID,
                ModifiedDT = DateTime.UtcNow,
                ReceivedDT = DateTime.UtcNow,
                BodyID = this.MessageBody.ID
            }).ToList();
            return employeeHeaders;
        }

    }
}
