﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Factories.Interfaces
{
    public interface IKeyedFactory<TInstanceType, TKey> where TKey : struct
    {
        TInstanceType Create(TKey key, params object[] additionalData);
    }
}
