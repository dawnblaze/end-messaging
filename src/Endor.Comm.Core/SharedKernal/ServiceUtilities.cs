﻿using Endor.Comm.Common.Documents;
using Endor.Comm.Core.Interfaces;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.SharedKernal
{
    public static class ServiceUtilities
    {
        /// <summary>
        /// Helper method to iterate over an array of child service associations and invoke Add operations. This method allows null arrays.
        /// </summary>
        /// <typeparam name="pM">Parent Model type</typeparam>
        /// <param name="associations">A (possibly null) array of associations</param>
        /// <param name="parentModel">The parent model that is the root of the operation</param>
        /// <returns></returns>
        public static async Task DoBeforeAddAsync<pM>(this IChildAssociation<pM>[] associations, IRepository repository, pM parentModel)
            where pM : class, IAtom
        {
            if (associations != null)
            {
                foreach (IChildAssociation<pM> association in associations)
                {
                    await association.ForEachChildDoBeforeCreateAsync(repository, parentModel);
                }
            }
        }
        
        /// <summary>
        /// Helper method to iterate over an array of child service associations and invoke Update operations. This method allows null arrays.
        /// </summary>
        /// <typeparam name="pM">Parent Model type</typeparam>
        /// <param name="associations">A (possibly null) array of associations</param>
        /// <param name="parentOldModel">The old parent model that is the root of the operation</param>
        /// <param name="parentNewModel">The new parent model that is the root of the operation</param>
        /// <returns></returns>
        public static async Task DoBeforeUpdateAsync<pM>(this IChildAssociation<pM>[] associations, IRepository repository, pM parentOldModel, pM parentNewModel)
            where pM : class, IAtom
        {
            if (associations != null)
            {
                foreach (IChildAssociation<pM> association in associations)
                {
                    await association.ForEachChildDoBeforeUpdateAsync(repository, parentOldModel, parentNewModel);
                }
            }
        }

        /// <summary>
        /// Helper method to iterate over an array of child service associations and invoke Delete operations. This method allows null arrays.
        /// </summary>
        /// <typeparam name="pM">Parent Model type</typeparam>
        /// <param name="associations">A (possibly null) array of associations</param>
        /// <param name="parentModel">The parent model that is the root of the operation</param>
        /// <returns></returns>
        public static async Task DoBeforeDeleteAsync<pM>(this IChildAssociation<pM>[] associations, IRepository repository, pM parentModel)
            where pM : class, IAtom
        {
            if (associations != null)
            {
                foreach (IChildAssociation<pM> association in associations)
                {
                    await association.ForEachChildDoBeforeDeleteAsync(repository, parentModel);
                }
            }
        }

        /// <summary>
        /// Helper method to iterate over an array of child service associations and invoke Clone operations. This method allows null arrays.
        /// </summary>
        /// <typeparam name="pM">Parent Model type</typeparam>
        /// <param name="associations">A (possibly null) array of associations</param>
        /// <param name="parentModel">The parent model that is the root of the operation</param>
        /// <returns></returns>
        public static async Task DoBeforeCloneAsync<pM>(this IChildAssociation<pM>[] associations, IRepository repository, pM parentModel)
            where pM : class, IAtom
        {
            if (associations != null)
            {
                foreach (IChildAssociation<pM> association in associations)
                {
                    await association.ForEachChildDoBeforeCloneAsync(repository, parentModel);
                }
            }
        }
    }
}
