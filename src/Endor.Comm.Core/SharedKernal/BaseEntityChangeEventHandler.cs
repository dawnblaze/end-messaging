﻿using Endor.Comm.Core.SharedKernel;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.Handlers
{
    public abstract class BaseEntityChangeEventHandler
    {
        /// <summary>
        /// object to queue tasks with
        /// </summary>
        protected readonly ITaskQueuer _taskQueuer;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected readonly IRTMPushClient _rtmClient;
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly RemoteLogger _logger;

        public BaseEntityChangeEventHandler(ITaskQueuer taskQueuer, IRTMPushClient rtmClient, RemoteLogger logger)
        {
            _taskQueuer = taskQueuer;
            _rtmClient = rtmClient;
            _logger = logger;
        }
        /// <summary>
        /// Re-index the the model
        /// </summary>
        /// <param name="id">Model Id</param>
        /// <returns></returns>
        public async Task QueueIndexForModel(BaseIdentityEntity entity)
        {
            await this._taskQueuer.IndexModel(entity.BID, entity.ClassTypeID, entity.ID);
        }
    }
}
