﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Core.SharedKernal
{
    /// <summary>
    /// a class that represents parent-child relationships
    /// this exposes properties that allow one service to call another service
    /// <para>
    /// this is best illustrated by Employee and EmployeeLocators.
    /// an Employee save may save EmployeeLocators and should call the EmployeeLocator service's methods instead of managing another object
    /// </para>
    /// </summary>
    /// <typeparam name="pM">Parent Model</typeparam>
    /// <typeparam name="cM">Child Model</typeparam>
    public abstract class ChildAssociation<pM, cM> : IChildAssociation<pM>
        where pM : class, IEntity
        where cM : class, IEntity, IEntityService
    {
        /// <summary>
        /// Children
        /// </summary>
        public readonly Func<pM, ICollection<cM>> ChildCollection;

        /// <summary>
        /// Child Service Association Constructor
        /// </summary>
        public ChildAssociation(Func<pM, ICollection<cM>> childCollection)
        {
            this.ChildCollection = childCollection;
        }

        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being added
        /// </summary>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        public async Task ForEachChildDoBeforeCreateAsync(IRepository repository, pM parentModel)
        {
            var children = ChildCollection(parentModel);
            if (children != null)
            {
                var myChildren = children.ToList();
                foreach (var child in myChildren)
                {
                    if (child != null)
                    {
                        if (typeof(cM) is IDoBeforeCreateUpdateWithParent<cM, pM> s)
                            s.DoBeforeCreateWithParent(parentModel);

                        await child.DoBeforeCreateAsync(repository, parentModel.BID);
                    }
                }
            }
        }

        public abstract Task ForEachChildDoBeforeUpdateAsync(IRepository repository, pM parentOldModel, pM parentNewModel);

        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being cloned
        /// </summary>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        public async Task ForEachChildDoBeforeCloneAsync(IRepository repository, pM parentModel)
        {
            if (typeof(cM) is ICloneableChildCRUDService<pM, cM> cloneableService)
            {
                var children = ChildCollection(parentModel);
                if (children != null)
                {
                    foreach (var child in children)
                    {
                        await cloneableService.DoBeforeCloneForParent(parentModel, child);
                    }
                }
            }
        }

        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being deleted
        /// </summary>
        /// <param name="parentModel"></param>
        public async Task ForEachChildDoBeforeDeleteAsync(IRepository repository, pM parentModel)
        {
            ICollection<cM> children = ChildCollection(parentModel);

            if (children != null)
            {
                foreach (cM childModel in children)
                {
                    await childModel.DoBeforeDeleteAsync(repository);
                }
            }
        }
    }
    
    /// <summary>
    /// Concrete class for IChildServiceAssociation that iterates over child objects during Create/Update/Delete operations in order to cascade business logic
    /// </summary>
    /// <typeparam name="pM">Parent Model type</typeparam>
    /// <typeparam name="cM">Child Model type</typeparam>
    public class ChildIdentityServiceAssociation<pM, cM> : ChildAssociation<pM, cM>
        where pM : class, IEntity
        where cM : BaseIdentityEntity, IAtom, IEntity, IEntityService
    {

        /// <summary>
        /// ChildAtomServiceAssociation Constructor
        /// </summary>
        public ChildIdentityServiceAssociation(Func<pM, ICollection<cM>> childCollectionAccessor)
            : base(childCollectionAccessor)
        {
        }
        
        /// <summary>
        /// deleting children that exist in the DB (parentOldModel) and do not exist in the new object/save packet (parentNewModel)
        /// propagating doBeforeUpdate onto children
        /// </summary>
        /// <param name="parentOldModel"></param>
        /// <param name="parentNewModel"></param>
        /// <returns></returns>
        public override async Task ForEachChildDoBeforeUpdateAsync(IRepository repository, pM parentOldModel, pM parentNewModel)
        {
            ICollection<cM> children = ChildCollection(parentNewModel);
            if (children != null)
            {
                List<cM> newChildren = children.ToList();
                if (parentOldModel != null)
                {
                    ICollection<cM> oldChildren = ChildCollection(parentOldModel);

                    if (oldChildren != null)
                    {
                        foreach (cM oldC in oldChildren.ToList())
                        {
                            if (!newChildren.Any(newC => Convert.ToInt32(newC.ID) == Convert.ToInt32(oldC.ID)))
                            {
                                // Added DoBeforeDelete to Delete Child Data Before Parent
                                await oldC.DoBeforeDeleteAsync(repository);
                                await repository.DeleteAsync(oldC);
                            }
                        }
                    }
                }
                foreach (cM child in newChildren)
                {

                    cM oldchild = await repository.GetByIdAsync<cM>(child.ID);

                    if (oldchild == null)
                    {
                        if (child is IDoBeforeCreateUpdateWithParent<cM, pM>)
                        {
                            (child as IDoBeforeCreateUpdateWithParent<cM, pM>).DoBeforeCreateWithParent(parentNewModel);
                        }

                        await child.DoBeforeCreateAsync(repository, oldchild.BID);
                    }
                    else
                    {
                        if (child is IDoBeforeCreateUpdateWithParent<cM, pM>)
                        {
                            (child as IDoBeforeCreateUpdateWithParent<cM, pM>).DoBeforeUpdateWithParent(parentOldModel, parentNewModel);
                        }
                        await child.DoBeforeUpdateAsync(repository, oldchild);
                    }
                }
            }
        }
    }

    /// <summary>
    /// ChildAtomServiceAssociation Constructor
    /// </summary>
    /// <typeparam name="pM">Parent Model type</typeparam>
    /// <typeparam name="cM">Child Model type</typeparam>
    public class ChildLinkAssociation<pM, cM> : ChildAssociation<pM, cM>
        where pM : class, IAtom
        where cM : BaseEntity, IEntity, IEntityService
    {
        /// <summary>
        /// ChildLinkServiceAssociation Constructor
        /// </summary>
        public ChildLinkAssociation(Func<pM, ICollection<cM>> childCollectionAccessor)
            : base(childCollectionAccessor)
        {
        }

        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being updated
        /// </summary>
        /// <param name="parentOldModel"></param>
        /// <param name="parentNewModel"></param>
        /// <returns></returns>
        public override async Task ForEachChildDoBeforeUpdateAsync(IRepository repository, pM parentOldModel, pM parentNewModel)
        {
            var children = ChildCollection(parentNewModel);
            if (children != null)
            {
                var myChildren = children.ToList();
                foreach (var child in myChildren)
                {
                    if (typeof(cM) is IDoBeforeCreateUpdateWithParent<cM, pM> s)
                        s.DoBeforeCreateWithParent(parentNewModel);

                    await child.DoBeforeCreateAsync(repository, parentNewModel.BID);
                }
            }
            await Task.CompletedTask;
        }
    }
}
