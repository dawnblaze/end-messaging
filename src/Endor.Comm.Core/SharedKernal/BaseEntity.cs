﻿using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Endor.Comm.Core.SharedKernal
{
    public class BaseEntity : IEntity, IEntityService
    {
        [JsonIgnore]
        public short BID { get; set; }

        public virtual Task DoBeforeCreateAsync(IRepository repository, short bid)
        {
            this.BID = bid;
            return Task.CompletedTask;
        }

        public virtual Task DoBeforeDeleteAsync(IRepository repository)
        {
            return Task.FromResult(0);
        }

        
    }
}
