﻿using Endor.DocumentStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Common.Documents
{
    /// <summary>
    /// Bucket request object for documents,data,reports
    /// </summary>
    public enum BucketRequest
    {
        /// <summary>
        /// Using all types of buckets
        /// </summary>
        All = -1,
        /// <summary>
        /// Using document type buckets
        /// </summary>
        Documents = 0,
        /// <summary>
        /// Using data type buckets
        /// </summary>
        Data = 1,
        /// <summary>
        /// Using report type buckets
        /// </summary>
        Reports = 2,
    }
}
