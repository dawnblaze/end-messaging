﻿using System;

namespace Endor.Comm.Core.SharedKernel
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DefaultIncludesAttribute : Attribute
    {
    }
}