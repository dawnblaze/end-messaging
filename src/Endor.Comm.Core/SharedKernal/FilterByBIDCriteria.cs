﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.Core.SharedKernal
{
    /// <summary>
    /// Basic Filter to filter on BID value
    /// </summary>
    /// <typeparam name="T">BaseEntity</typeparam>
    public class FilterByBIDCriteria<T> : Criteria<T> where T : BaseEntity
    {
        private readonly short _bid;
        public FilterByBIDCriteria(short bid)
        {
            _bid = bid;
        }
        public override Expression<Func<T, bool>> ToExpression()
        {
            return ul => ul.BID == _bid;
        }
    }

    /// <summary>
    /// Retrieve all messages by channgel type and employee id (optional)
    /// </summary>
    public class FilterByBIDSpecification<T> : BaseSpecification<T> where T : BaseEntity
    {
        public FilterByBIDSpecification(short bid)
            : base()
        {
            AddCriteria(new FilterByBIDCriteria<T>(bid));
        }
    }
}
