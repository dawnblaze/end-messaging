﻿using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Endor.Comm.Core.SharedKernal;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Endor.Comm.Core.SharedKernel
{
    public abstract class BaseIdentityEntity : BaseEntity, IAtom, IAtomService
    {
        public int ID { get; set; }
        public abstract int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        [JsonIgnore]
        [NotMapped]
        public short IDAsShort
        {
            get => (short)ID;
            set => ID = value;
        }
        [JsonIgnore]
        [NotMapped]
        public byte IDAsByte
        {
            get => (byte)ID;
            set => ID = value;
        }

        public virtual IEnumerable<string> IncludeDefaults<TEntity>()
            where TEntity : class
        {
            return new string[0];
        }

        public override async Task DoBeforeCreateAsync(IRepository repository, short bid)
        {
            await base.DoBeforeCreateAsync(repository, bid);
            SetModifiedDT();
            this.ID = await repository.RequestIDIntegerAsync(bid, this.ClassTypeID);
        }


        public virtual Task DoBeforeUpdateAsync(IRepository repository, IAtom oldModel)
        {
            SetModifiedDT();
            return Task.CompletedTask;
        }

        public virtual async Task DoBeforeCloneAsync(IRepository repository)
        {
            SetModifiedDT();
            this.ID = await repository.RequestIDIntegerAsync(this.BID, this.ClassTypeID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        public void SetModifiedDT()
        {
            this.ModifiedDT = DateTime.UtcNow;
        }

        /// <summary>  
        /// Determine if two entities are equal 
        /// </summary>  
        public override bool Equals(object obj)
        {
            var other = obj as BaseIdentityEntity;

            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (GetType() != other.GetType())
                return false;

            if ((ID == 0) || (other.ID == 0))
                return false;

            return (ID == other.ID) && (BID == other.BID);
        }

        public static bool operator ==(BaseIdentityEntity a, BaseIdentityEntity b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(BaseIdentityEntity a, BaseIdentityEntity b)
        {
            return !(a == b);
        }

        /// <summary>  
        /// Used to generate hashcode for entity 
        /// </summary>  
        public override int GetHashCode()
        {
            return (GetType().ToString() + BID + " " + ID).GetHashCode();
        }

        public Task<Tuple<bool, string>> CanDeleteAsync(IRepository repository)
        {
            return repository.CanDeleteAsync(this);
        }

        public List<BaseDomainEvent> Events = new List<BaseDomainEvent>();
    }


    public abstract class BaseIdentityService<T> : BaseIdentityEntity
        where T : class, IAtom
    {
        /// <summary>
        /// An array of child associations. The array may be null
        /// </summary>
        protected IChildAssociation<T>[] childAssociations => GetChildAssociations();

        /// <summary>
        /// Get child associations (services that handle business logic for child collections). May return null.
        /// </summary>
        /// <returns>IChildServiceAssociation or null</returns>
        protected abstract IChildAssociation<T>[] GetChildAssociations();

        /// <summary>
        /// A helper method to construct child associations
        /// </summary>
        /// <remarks>
        /// This mostly exists so concrete classes do not need to specify M and I every time
        /// </remarks>
        /// <typeparam name="pM">Child Model type</typeparam>
        /// <typeparam name="cM">Child Model type</typeparam>
        // <param name="accessor">A function that returns the list of type cM</param>
        /// <returns></returns>
        protected IChildAssociation<pM> CreateChildAssociation<pM, cM>(Func<pM, ICollection<cM>> accessor)
            where pM : class, IAtom
            where cM : BaseIdentityEntity
        {
            return new ChildIdentityServiceAssociation<pM, cM>(accessor);
        }

        /// <summary>
        /// A helper method to construct child associations
        /// </summary>
        /// <remarks>
        /// This mostly exists so concrete classes do not need to specify M and I every time
        /// </remarks>
        /// <typeparam name="pM">Child Model type</typeparam>
        /// <typeparam name="cM">Child Model type</typeparam>
        // <param name="accessor">A function that returns the list of type cM</param>
        /// <returns></returns>
        protected IChildAssociation<pM> CreateChildAssociation<pM, cM>(Func<pM, cM> accessor)
            where pM : class, IAtom
            where cM : BaseIdentityEntity
        {
            Func<cM, ICollection<cM>> convert = x => new List<cM>() { x };
            Func<pM, ICollection<cM>> exp = x => convert(accessor(x));
            return new ChildIdentityServiceAssociation<pM, cM>(exp);
        }

        /// <summary>
        /// A helper method to construct child associations to link tables
        /// </summary>
        /// <remarks>
        /// This mostly exists so concrete classes do not need to specify M and I every time
        /// </remarks>
        /// <typeparam name="pM">Child Model type</typeparam>
        /// <typeparam name="cM">Child Model type</typeparam>
        /// <param name="accessor">A function that returns the list of type cM</param>
        /// <returns></returns>
        protected IChildAssociation<pM> CreateChildLinkAssociation<pM, cM>(Func<pM, ICollection<cM>> accessor)
            where pM : class, IAtom
            where cM : BaseEntity, IEntity, IEntityService
        {
            return new ChildLinkAssociation<pM, cM>(accessor);
        }

        public override async Task DoBeforeCreateAsync(IRepository repository, short bid)
        {
            await base.DoBeforeCreateAsync(repository, bid);
            Task childAdd = childAssociations?.DoBeforeAddAsync(repository, this as T);
            if (childAdd != null)
                await childAdd;
        }

        public override async Task DoBeforeUpdateAsync(IRepository repository, IAtom oldModel)
        {
            await base.DoBeforeUpdateAsync(repository, oldModel);
            Task childUpdate = childAssociations?.DoBeforeUpdateAsync(repository, oldModel as T, this as T);
            if (childUpdate != null)
                await childUpdate;
        }

        public override async Task DoBeforeCloneAsync(IRepository repository)
        {
            await base.DoBeforeCloneAsync(repository);
            Task childClone = childAssociations?.DoBeforeCloneAsync(repository, this as T);
            if (childClone != null)
                await childClone;
        }

        public override async Task DoBeforeDeleteAsync(IRepository repository)
        {
            await base.DoBeforeDeleteAsync(repository);
            Task childDelete = childAssociations?.DoBeforeDeleteAsync(repository, this as T);
            if (childDelete != null)
                await childDelete;
        }
    }       
}
