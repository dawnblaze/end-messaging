﻿using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Interfaces;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endor.Comm.Core.Events;
using System.Threading.Tasks;

namespace Endor.Comm.Core.SharedKernal
{
    public abstract class AggregateRoot<T> : BaseIdentityService<T>
        where T : class, IAtom
    {
        private ModelStateDictionary _modelState = new ModelStateDictionary();

        /// <summary>  
        /// Add a domain event to be processed during the next dispatch call 
        /// </summary>  
        protected virtual void AddDomainEvent(BaseDomainEvent newEvent)
        {
            Events.Add(newEvent);
        }

        /// <summary>  
        /// Remove all queued events 
        /// </summary>  
        public virtual void ClearEvents()
        {
            Events.Clear();
        }

        /// <summary>
        /// Is Model State valid currently
        /// </summary>
        public bool IsValid
        {
            get { return _modelState.IsValid; }
        }

        /// <summary>
        /// Add model error
        /// </summary>
        /// <param name="key"></param>
        /// <param name="errorMessage"></param>
        protected void AddError(string key, string errorMessage)
        {
            _modelState.AddModelError(key, errorMessage);
        }

        public string GetModelStateErrorsString()
        {
            var errorMessages = _modelState.Values.SelectMany(x => x.Errors);
            return String.Join(" ", errorMessages.Select(x => x.ErrorMessage));
        }

        public override async Task DoBeforeCreateAsync(IRepository repository, short bid)
        {
            await base.DoBeforeCreateAsync(repository, bid);
            Events.Add(new EntityAddedEvent(this));
        }

        public override async Task DoBeforeUpdateAsync(IRepository repository, IAtom oldModel)
        {
            await base.DoBeforeUpdateAsync(repository, oldModel);
            Events.Add(new EntityUpdatedEvent(this));
        }

        public override async Task DoBeforeDeleteAsync(IRepository repository)
        {
            await base.DoBeforeDeleteAsync(repository);
            Events.Add(new EntityDeletedEvent(this));
        }
    }
}
