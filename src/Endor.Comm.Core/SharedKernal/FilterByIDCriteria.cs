﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.Comm.Core.SharedKernal
{
    /// <summary>
    /// Basic Filter to filter on ID value
    /// </summary>
    /// <typeparam name="T">BaseEntity</typeparam>
    public class FilterByIDCriteria<T> : Criteria<T> where T : BaseIdentityEntity
    {
        private readonly int _id;
        public FilterByIDCriteria(int id)
        {
            _id = id;
        }
        public override Expression<Func<T, bool>> ToExpression()
        {
            return ul => ul.ID == _id;
        }
    }

    /// <summary>
    /// Retrieve all messages by channgel type and employee id (optional)
    /// </summary>
    public class FilterByIDSpecification<T> : BaseSpecification<T> where T : BaseIdentityEntity
    {
        public FilterByIDSpecification(int id)
            : base()
        {
            AddCriteria(new FilterByIDCriteria<T>(id));
        }
    }
}
