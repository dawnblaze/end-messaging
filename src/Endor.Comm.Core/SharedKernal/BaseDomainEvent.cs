﻿using Endor.Comm.Core.Interfaces;
using System;

namespace Endor.Comm.Core.SharedKernel
{
    public abstract class BaseDomainEvent : IDomainEvent
    {
        public DateTime DateOccurred { get; protected set; } = DateTime.UtcNow;
    }
}