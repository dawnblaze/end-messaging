﻿using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class EmployeeData : BaseIdentityEntity
    {
        public override int ClassTypeID { get => ClassTypeIDValue; set { } }

        public const int ClassTypeIDValue = 5000;
        public byte LocationID { get; set; }

        public short? DefaultEmailAccountID { get; set; }
        public ICollection<EmployeeLocator> EmployeeLocators { get; set; }
        public static string TableName = "[dbo].[Employee.Data]";

    }
}
