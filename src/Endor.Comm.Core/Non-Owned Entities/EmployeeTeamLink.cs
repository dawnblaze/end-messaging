﻿using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class EmployeeTeamLink : BaseEntity
    {
        public int TeamID { get; set; }
        public short EmployeeID { get; set; }
        public short RoleID { get; set; }

        public EmployeeData Employee { get; set; }

        public EmployeeTeam EmployeeTeam { get; set; }

        //public EmployeeRole Role { get; set; }
    }
}
