﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class TransactionHeader : BaseIdentityEntity
    {
        public override int ClassTypeID { get => ClassTypeIDValue; set { } }

        public const int ClassTypeIDValue = 10000;

        /// <summary>
        /// The type of transaction this is.  Valid TransactionType values are for this object are:
        /// 1 = Estimate
        /// 2 = Order
        /// 4 = PO
        /// </summary>
        public byte TransactionType { get; set; }

        public byte LocationID { get; set; }

        public const byte OrderTransactionType = 2;
        public const byte EstimateTransactionType = 1;
        public static string TableName = "[dbo].[Order.Data]";


    }


}
