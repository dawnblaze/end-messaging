﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class EmployeeLocator : BaseLocator<short, EmployeeData>
    {
        public override short ParentID { get; set; }
        public override EmployeeData Parent { get; set; }
        public override EnumLocatorSubType LocatorSubTypeNavigation { get; set; }
        public override EnumLocatorType LocatorTypeNavigation { get; set; }

        public static List<EmployeeData> GetEmployeeLocatorsForEmployeeID(IDbConnection connection, short bid, int ID)
        {
            var sql = $"Select ED.ID,ED.DefaultEmailAccountID,EL.* From [dbo].[Employee.Data] AS ED Left OUTER JOIN [dbo].[Employee.Locator] AS EL ON EL.ParentID=ED.ID WHERE ED.ID = {ID.ToString()} AND EL.BID = {bid.ToString()}";
            var lookup = new Dictionary<int, EmployeeData>();
            connection.Query<EmployeeData, EmployeeLocator, EmployeeData>(sql,
                ((ED, EL) =>
                {
                    EmployeeData employeeObject;
                    if (!lookup.TryGetValue(ED.ID, out employeeObject))
                        lookup.Add(ED.ID, employeeObject = ED);
                    if (employeeObject.EmployeeLocators == null)
                        employeeObject.EmployeeLocators = new List<EmployeeLocator>();
                    if (EL.ParentID != 0)
                    {
                        employeeObject.EmployeeLocators.Add(EL);
                        EL.Parent = employeeObject;
                    }

                    return employeeObject;
                }), splitOn: "DefaultEmailAccountID").AsQueryable();

            return lookup.Values.ToList();
        }
    }
}
