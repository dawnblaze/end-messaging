﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public abstract class BaseLocator<I, P> 
    {
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public string Locator { get; set; }
        public string RawInput { get; set; }

        /// <summary>
        /// The locator type (phone, address, etc)
        /// </summary>
        public byte LocatorType { get; set; }
        public LocatorType eLocatorType { get { return (LocatorType)this.LocatorType; } }
        public virtual EnumLocatorType LocatorTypeNavigation { get; set; }

        public byte LocatorSubType { get; set; }
        public virtual EnumLocatorSubType LocatorSubTypeNavigation { get; set; }

        public short SortIndex { get; set; }
        public bool IsVerified { get; set; }
        public bool IsValid { get; set; }
        public bool HasImage { get; set; }

        public abstract I ParentID { get; set; }
        public abstract P Parent { get; set; }
    }
}
