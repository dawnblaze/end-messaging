﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class LocationData : BaseIdentityEntity
    {
        public override int ClassTypeID { get => ClassTypeIDValue; set { } }

        public const int ClassTypeIDValue = 1005;
        public static string TableName = "[dbo].[Location.Data]";
        public short? DefaultEmailAccountID { get; set; }

    }
}
