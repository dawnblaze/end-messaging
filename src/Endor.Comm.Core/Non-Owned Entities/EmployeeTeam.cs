﻿using Dapper;
using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class EmployeeTeam : BaseIdentityEntity
    {
        public const int ClassTypeIDValue = 5020;
        public override int ClassTypeID { get => ClassTypeIDValue; set { } }
        public string Name { get; set; }
        public bool IsAdHocTeam { get; set; }
        public ICollection<EmployeeTeamLink> EmployeeTeamLinks { get; set; } = new List<EmployeeTeamLink>();
        public static string TableName = "[dbo].[Employee.Team]";

        public static List<EmployeeTeam> GetEmployeeTeamWithEmployeeTeamLinks(IDbConnection connection, short bid)
        {
            var sql = $"Select * From [dbo].[Employee.Team] AS ET LEFT OUTER JOIN [dbo].[Employee.TeamLink] AS ETL ON ET.ID = ETL.TeamID WHERE ET.BID = {bid.ToString()}";
            //var sql2 = $"Select ET.* From [dbo].[Employee.Team] ET WHERE ET.BID = {bid.ToString()}";
            var lookup = new Dictionary<int, EmployeeTeam>();
            //return new List<EmployeeTeam>(connection.Query<EmployeeTeam>(sql));
            connection.Query<EmployeeTeam, EmployeeTeamLink, EmployeeTeam>(sql,
                ((ET,ETL) =>
                {
                    EmployeeTeam employeeTeamObject;
                    if (!lookup.TryGetValue(ET.ID, out employeeTeamObject))
                        lookup.Add(ET.ID, employeeTeamObject = ET);
                    if (employeeTeamObject.EmployeeTeamLinks == null)
                        employeeTeamObject.EmployeeTeamLinks = new List<EmployeeTeamLink>();
                    if (ETL.TeamID != 0)
                    {
                        employeeTeamObject.EmployeeTeamLinks.Add(ETL);
                        ETL.EmployeeTeam = employeeTeamObject;
                    }

                    return employeeTeamObject;
                }),splitOn: "IsAdHocTeam").AsQueryable();

            return lookup.Values.ToList();
        }

        public static EmployeeTeam GetEmployeeTeamWithEmployeeTeamID(IDbConnection connection, short bid, short teamID)
        {
            var sql = $"Select * From [dbo].[Employee.Team] AS ET WHERE ET.ID = {teamID}";
            var employeeTeam = connection.Query<EmployeeTeam>(sql).FirstOrDefault();
            return employeeTeam;
        }
    }
}
