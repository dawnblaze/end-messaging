﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class CompanyData : BaseIdentityEntity
    {
        public override int ClassTypeID { get => ClassTypeIDValue; set { } }

        public const int ClassTypeIDValue = 2000;
        public byte LocationID { get; set; }
        public static string TableName = "[dbo].[Company.Data]";

    }
}
