﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class ContactLocator : BaseLocator<int, ContactData>
    {
        public override int ParentID { get; set; }
        public override ContactData Parent { get; set; }
        public override EnumLocatorSubType LocatorSubTypeNavigation { get; set; }
        public override EnumLocatorType LocatorTypeNavigation { get; set; }

        public static List<ContactLocator> GetContactLocatorsForContactID(IDbConnection connection, short bid, int ID)
        {
            var sql = $"Select CL.* From [dbo].[Contact.Locator] AS CL WHERE CL.ParentID = {ID.ToString()} AND CL.BID = {bid.ToString()}";
            var lookup = new Dictionary<int, EmployeeTeam>();
            //return new List<EmployeeTeam>(connection.Query<EmployeeTeam>(sql));
            var results = connection.Query<ContactLocator>(sql).ToList<ContactLocator>();
            return results;
        }
    }
}
