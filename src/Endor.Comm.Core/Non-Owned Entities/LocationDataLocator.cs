﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class LocationDataLocator : BaseLocator<byte, LocationData>
    {
        public override byte ParentID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public override LocationData Parent { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public static List<LocationData> GetLocationLocatorsForLocationID(IDbConnection connection, short bid, byte ID)
        {
            var sql = $"Select * FROM [dbo].[Location.Data] WHERE ID = {ID.ToString()} AND BID = {bid.ToString()}";
            var lookup = new Dictionary<int, LocationData>();
            connection.Query<LocationData, EmployeeLocator, LocationData>(sql,
                (ED, EL) =>
                {
                    if (!lookup.TryGetValue(ED.ID, out LocationData locationObject))
                        lookup.Add(ED.ID, locationObject = ED);
                    return locationObject;
                }).AsQueryable();

            return lookup.Values.ToList();
        }
    }
}
