﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public enum LocatorType
    {
        Unknown = 0,
        Address,
        Phone,
        Email,
        WebAddress,
        Twitter,
        Facebook,
        LinkedIn,
        FTP,
        IP
    }

    public partial class EnumLocatorType
    {
        public EnumLocatorType()
        {

        }

        public byte ID { get; set; }
        public string Name { get; set; }
        public string ValidityRegEx { get; set; }
        public string PossibilityRegEx { get; set; }
        public byte? PossibilitySortOrder { get; set; }
    }

    public partial class EnumLocatorSubType
    {
        public EnumLocatorSubType()
        {
        }

        public byte LocatorType { get; set; }
        public byte ID { get; set; }
        public string Name { get; set; }
        public string LinkFormatString { get; set; }
    }
}
