﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Core.Non_Owned_Entities
{
    public class UserLink
    {
        public int? AuthUserID { get; set; }
        public short BID { get; set; }
        public short ID { get; set; }
        public short? EmployeeID { get; set; }
        public int ClassTypeID { get => ClassTypeIDValue; set { } }
        public byte UserAccessType { get; set; }
        public const int ClassTypeIDValue = 1012;

    }
}
