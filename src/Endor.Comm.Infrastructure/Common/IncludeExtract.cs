﻿using Endor.Comm.Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Endor.Comm.Infrastructure.Common
{
    public static class IncludeExtract
    {
        public static IEnumerable<string> IterateProps(Type baseType)
        {
            var results = IteratePropsInner(baseType, "");
            return results.Select(x => x.TrimStart('.')).ToArray();
        }

        private static IEnumerable<string> IteratePropsInner(Type baseType, string baseName)
        {
            PropertyInfo[] propertyInfo = baseType.GetProperties();

            foreach (var property in propertyInfo)
            {
                var name = property.Name;
                if (property.GetCustomAttributes(typeof(DefaultIncludesAttribute), false).Any())
                {
                    var type = ListArgumentOrSelf(property.PropertyType);
                    foreach (var info in IteratePropsInner(type, 
                        string.Format("{0}.{1}", baseName, property.Name)))
                        yield return info;
                    yield return string.Format("{0}.{1}", baseName, property.Name);
                }
            }
        }

        public static Type ListArgumentOrSelf(Type type)
        {
            if (!type.IsGenericType)
                return type;
            var listType = typeof(List<>);
            var collectionType = typeof(ICollection<>);
            var thisType = type.GetGenericTypeDefinition();
            if ((listType != thisType)&& (collectionType != thisType))
                throw new Exception("Only List<T> are allowed");
            return type.GetGenericArguments()[0];
        }
    }
}
