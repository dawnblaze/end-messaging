﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Comm.Infrastructure.Common
{
    public class EfSpecificationEvaluator<T> where T : class
    {
        /// <summary>  
        /// Code to add includes to the query - from both the specification and a list of strings
        /// </summary>  
        public static IQueryable<T> GetQuery(IQueryable<T> inputQuery, ISpecification<T> specification, IEnumerable<string> includes)
        {
            var query = inputQuery;

            query = SpecificationEvaluator<T>.GetQuery(query, specification);

            // Includes all expression-based includes
            query = specification.Includes.Aggregate(query,
                                    (current, include) => current.Include(include));

            // Include any string-based include statements
            query = specification.IncludeStrings.Aggregate(query,
                                    (current, include) => current.Include(include));

            query = includes.Aggregate(query,
                                    (current, include) => current.Include(include));

            return query;
        }
    }
}
