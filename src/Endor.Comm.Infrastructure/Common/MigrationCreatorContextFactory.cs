﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Infrastructure.Common
{
    public class MigrationCreatorContextFactory<T> : IDesignTimeDbContextFactory<T>
    where T : DbContext
    {
        private const string migrationOptionsFileName = "MigrationOptions.json";
        public T CreateDbContext(string[] args)
        {
            if (args == null || args.Length < 1)
            {
                while (!File.Exists(migrationOptionsFileName))
                {
                    Console.WriteLine("Please type in a connection string for your BusinessDB");
                    string inputString = Console.ReadLine();
                    System.IO.File.WriteAllText(migrationOptionsFileName,
$@"{{
  ""ConnectionStrings"": {{
    ""Business"": ""{inputString.Replace("\\", "\\\\").Replace("\"", "\\\"")}""
  }}
}}");
                }
                IConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Environment.CurrentDirectory)
                    .AddJsonFile(migrationOptionsFileName);
                IConfigurationRoot config = configurationBuilder.Build();
                args = new string[] { config.GetConnectionString("DefaultConnection") };

            }

            if (args == null || args.Length < 1)
            {
                throw new ArgumentNullException(nameof(args), "No arguments passed, the first argument must be the connectionString for the database");
            }

            string connString = args[0];
            var optionsBuilder = new DbContextOptionsBuilder<T>();
            Console.WriteLine($"using connection string of: {connString}");
            optionsBuilder.UseSqlServer<T>(connString);

            var dbContext = (T)Activator.CreateInstance(
                typeof(T),
                optionsBuilder.Options,
                null,
                null,
                null);

            return dbContext;
        }
    }
}
