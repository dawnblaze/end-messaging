﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Dapper;
using Endor.Comm.Core.SharedKernal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Comm.Infrastructure.Common
{
    public class DapperRepository : IDapperReadOnlyRepository
    {
        protected string _tableName;
        protected short _BID;
        private string _connectionString;
        public virtual IDbConnection Connection()
        {
            return new SqlConnection(_connectionString);
        }

        public DapperRepository(short BID, string tableName, string connectionString)
        {
            this._BID = BID;
            this._tableName = tableName;
            this._connectionString = connectionString;
        }

        public virtual async Task<T> GetByIdAsync<T>(int id) where T : BaseIdentityEntity
        {
            T item = default(T);
            using (IDbConnection cn = Connection())
            {
                cn.Open();
                item = cn.Query<T>("SELECT * FROM " + _tableName + " WHERE BID=@BID AND ID=@ID", new { ID = id, BID = _BID }).SingleOrDefault();
            }

            if (item == null)
                throw new Core.Exceptions.EntityNotFoundException($"{typeof(T).Name.Replace("Data","")} with ID `{id}` not found. ");

            return await Task.FromResult(item);
        }

        public virtual async Task<T> GetByShortIdAsync<T>(short id) where T : BaseIdentityEntity
        {
            return await GetByIdAsync<T>(id);
        }

        public virtual async Task<T> GetByByteIdAsync<T>(byte id) where T : BaseIdentityEntity
        {
            return await GetByIdAsync<T>(id);
        }

        public virtual async Task<List<T>> ListAsync<T>() where T : class
        {
            IEnumerable<T> items = null;

            using (IDbConnection cn = Connection())
            {
                cn.Open();
                items = cn.Query<T>("SELECT * FROM " + _tableName);
            }

            var query = items.AsQueryable();
            return await Task.FromResult(query.ToList());
        }

        public virtual async Task<List<T>> ListEntityAsync<T>(ISpecification<T> spec = null) where T : BaseEntity
        {
            IEnumerable<T> items = null;

            using (IDbConnection cn = Connection())
            {
                cn.Open();
                items = cn.Query<T>("SELECT * FROM " + _tableName);
            }

            var query = items.AsQueryable();
            if (spec != null)
            {
                ((BaseSpecification<T>)spec).AddCriteria(new FilterByBIDCriteria<T>(_BID));
            } else
            {
                spec = new FilterByBIDSpecification<T>(_BID);
            }
            var applySpecification = ApplySpecification(query, spec);
            return await Task.FromResult(applySpecification.ToList());
        }

        protected IQueryable<T> ApplySpecification<T>(IQueryable<T> inputQuery, ISpecification<T> spec) where T : class
        {
            var query = SpecificationEvaluator<T>.GetQuery(inputQuery, spec);

            return query;
        }

        public IEnumerable<T> GetHierarchy<T, TChild>(string sql, Func<T, TChild, T> map)
        {
            using (IDbConnection cn = Connection())
            {
                return cn.Query<T, TChild, T>(sql, map);
            }
        }

        public void SetTableNameForClassType(int classTypeID)
        {
            switch (classTypeID)
            {
                case 1005: _tableName = "[dbo].[Location.Data]"; break;
                case 1012: _tableName = "[dbo].[User.Link]"; break;
                case 1023: _tableName = "[dbo].[Email.Account.Data]"; break;
                case 5000: _tableName = "[dbo].[Employee.Data]"; break;
                case 5020: _tableName = "[dbo].[Employee.Team]"; break;
                case 2000: _tableName = "[dbo].[Company.Data]"; break;
                case 3000: _tableName = "[dbo].[Contact.Data]"; break;
                default: throw new Exception("Could not find table name for classtypeId:" + classTypeID.ToString());
            }
        }

        public virtual List<T> List<T>(ISpecification<T> spec = null)
            where T : class
        {
            IEnumerable<T> items = null;

            using (IDbConnection cn = Connection())
            {
                cn.Open();
                items = cn.Query<T>("SELECT * FROM " + _tableName);
            }

            return ApplySpecification(items.AsQueryable(), spec).ToList();
        }
    }

}
