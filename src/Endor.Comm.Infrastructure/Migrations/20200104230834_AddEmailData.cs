﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Comm.Infrastructure.Migrations
{
    public partial class AddEmailData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.EmailAccountStatus",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.EmailAccountStatus", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Email.Account.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(type: "smallint", nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1023))"),
                    IsActive = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [StatusType]=(2) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    StatusType = table.Column<byte>(nullable: false),
                    DomainEmailID = table.Column<short>(nullable: false),
                    UserName = table.Column<string>(type: "varchar(255)", nullable: false),
                    DomainName = table.Column<string>(type: "varchar(255)", nullable: false),
                    IsPrivate = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [EmployeeID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    EmployeeID = table.Column<short>(nullable: true),
                    Credentials = table.Column<string>(type: "varchar(max)", nullable: true),
                    CredentialsExpDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    LastEmailSuccessDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    LastEmailFailureDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    EmailAddress = table.Column<string>(nullable: true, computedColumnSql: "(concat([UserName],'@',[DomainName]))"),
                    AliasUserNames = table.Column<string>(type: "varchar(1024)", nullable: true),
                    DisplayName = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Email.Account.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Email.Account.Data_enum.Email.AccountStatus",
                        column: x => x.StatusType,
                        principalTable: "enum.EmailAccountStatus",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Email.Account.Data_Domain.Email.Data",
                        columns: x => new { x.BID, x.DomainEmailID },
                        principalTable: "Domain.Email.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Email.Account.TeamLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    EmailAccountID = table.Column<short>(nullable: false),
                    TeamID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Email.Account.TeamLink", x => new { x.BID, x.EmailAccountID, x.TeamID });
                    table.ForeignKey(
                        name: "FK_Email.Account.TeamLink_Email.Account.Data",
                        columns: x => new { x.BID, x.EmailAccountID },
                        principalTable: "Email.Account.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.Data_StatusType",
                table: "Email.Account.Data",
                column: "StatusType");

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.Data_BID_DomainEmailID",
                table: "Email.Account.Data",
                columns: new[] { "BID", "DomainEmailID" });

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.Data_Employee",
                table: "Email.Account.Data",
                columns: new[] { "BID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.Data_Email",
                table: "Email.Account.Data",
                columns: new[] { "BID", "EmailAddress", "IsActive", "IsPrivate" });

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.TeamLink_Team",
                table: "Email.Account.TeamLink",
                columns: new[] { "BID", "TeamID" });

            migrationBuilder.Sql(@"
                ALTER TABLE [Email.Account.Data] WITH CHECK
                ADD CONSTRAINT [FK_Email.Account.Data_Business.Data] FOREIGN KEY([BID])
                REFERENCES [Business.Data] ([ID])
                ;

                ALTER TABLE [Email.Account.Data] WITH CHECK
                ADD CONSTRAINT [FK_Email.Account.Data_Employee.Data] FOREIGN KEY([BID], [EmployeeID])
                REFERENCES [Employee.Data] ([BID], [ID])
                ;

                ALTER TABLE [Email.Account.TeamLink] WITH CHECK
                ADD CONSTRAINT [FK_Email.Account.TeamLink_Employee.Team] FOREIGN KEY([BID], [TeamID])
                REFERENCES [Employee.Team] ([BID], [ID])
                ;

                INSERT [enum.EmailAccountStatus] ([ID], [Name])
                VALUES (1, N'Pending')
                    , (2, N'Authorized')
                    , (3, N'Failed')
                    , (4, N'Expired')
                    , (5, N'Inactive')
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Email.Account.TeamLink");

            migrationBuilder.DropTable(
                name: "Email.Account.Data");

            migrationBuilder.DropTable(
                name: "enum.EmailAccountStatus");
        }
    }
}
