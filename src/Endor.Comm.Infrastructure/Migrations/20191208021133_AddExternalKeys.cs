﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Comm.Infrastructure.Migrations
{
    public partial class AddExternalKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Message.Body] WITH CHECK
ADD CONSTRAINT [FK_Message.Content_Business.Data]
FOREIGN KEY([BID]) REFERENCES [Business.Data] ([BID])
;
 
ALTER TABLE [Message.Body]
CHECK CONSTRAINT [FK_Message.Content_Business.Data]
;
");
            migrationBuilder.Sql(@"
CREATE INDEX [IX_Message.Delivery.Record_Pending]
ON [Message.Delivery.Record] ( [BID], [IsPending], [ScheduledDT] )
;
");

            migrationBuilder.Sql(@"
CREATE INDEX [IX_Message.Header_Employee]
ON [Message.Header] ( [BID], [EmployeeID], [ReceivedDT] )
;
 
CREATE INDEX [IX_Message.Header_Participant]
ON [Message.Header] ( [BID], [ParticipantID] )
;
 
CREATE INDEX [IX_Message.Header_EmployeeChannels]
ON [Message.Header] ( [BID], [EmployeeID], [Channels], [IsRead] )
;
 
ALTER TABLE [Message.Header] WITH CHECK
ADD CONSTRAINT [FK_Message.Inbox_Employee.Data]
FOREIGN KEY([BID], [EmployeeID]) REFERENCES [Employee.Data] ([BID], [ID])
;
");

            migrationBuilder.Sql(@"
CREATE INDEX [IX_Message.Object.Link_LinkedObject]
ON [Message.Object.Link] ( [BID], [LinkedObjectClassTypeID], [LinkedObjectID] )
;
");

            migrationBuilder.Sql(@"
CREATE INDEX [IX_Message.Participant.Info_Body] ON
[Message.Participant.Info] ( [BID], [BodyID], [ParticipantRoleType] )
;
 
CREATE INDEX [IX_Message.Participant.Info_Contact]
ON [Message.Participant.Info] ( [BID], [ContactID]  )
WHERE ([ContactID] IS NOT NULL)
;
 
CREATE INDEX [IX_Message.Participant.Info_Employee]
ON [Message.Participant.Info] ( [BID], [EmployeeID]  )
WHERE ([EmployeeID] IS NOT NULL)
;
 
CREATE INDEX [IX_Message.Participant.Info_Team]
ON [Message.Participant.Info] ( [BID], [TeamID]  )
WHERE ([TeamID] IS NOT NULL)
;
 
ALTER TABLE [Message.Participant.Info] WITH CHECK
ADD CONSTRAINT [FK_Message.Participant.Info_Employee]
FOREIGN KEY([BID], [EmployeeID]) REFERENCES [Employee.Data] ([BID], [ID])
;
 
ALTER TABLE [Message.Participant.Info]
CHECK CONSTRAINT [FK_Message.Participant.Info_Employee]
;
 
ALTER TABLE [Message.Participant.Info] WITH CHECK
ADD CONSTRAINT [FK_Message.Participant.Info_Team]
FOREIGN KEY([BID], [TeamID]) REFERENCES [Employee.Team] ([BID], [ID])
;
 
ALTER TABLE [Message.Participant.Info]
CHECK CONSTRAINT [FK_Message.Participant.Info_Team]
;
 
 
ALTER TABLE [Message.Participant.Info]
CHECK CONSTRAINT [FK_Message.Participant.Info_Message.Content]
;
 
ALTER TABLE [Message.Participant.Info] WITH CHECK
ADD CONSTRAINT [FK_Notification.Participant.Info_Contact.Data]
FOREIGN KEY([BID], [ContactID]) REFERENCES [Contact.Data] ([BID], [ID])
;
 
ALTER TABLE [Message.Participant.Info]
CHECK CONSTRAINT [FK_Notification.Participant.Info_Contact.Data]
;
");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (0, \"None\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (1, \"Message\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (2, \"Alert\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (4, \"Email\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (8, \"SMS\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (16, \"Slack\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (32, \"WebHook\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (64, \"Zapier\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.ChannelType](ID, Name) VALUES (128, \"System Notification\");");

            migrationBuilder.Sql("INSERT INTO [enum.Message.Participant.RoleType](ID, Name) VALUES (1, \"From\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.Participant.RoleType](ID, Name) VALUES (2, \"To\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.Participant.RoleType](ID, Name) VALUES (3, \"CC\");");
            migrationBuilder.Sql("INSERT INTO [enum.Message.Participant.RoleType](ID, Name) VALUES (4, \"BCC\");");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"ALTER TABLE [Message.Body] DROP CONSTRAINT [FK_Message.Content_Business.Data];");
            
            migrationBuilder.Sql(@"ALTER TABLE [Message.Delivery.Record] DROP INDEX [IX_Message.Delivery.Record_Pending];");
            
            migrationBuilder.Sql(@"ALTER TABLE [Message.Header] DROP INDEX [IX_Message.Header_Employee];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Header] DROP INDEX [IX_Message.Header_Participant];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Header] DROP INDEX [IX_Message.Header_EmployeeChannels];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Header] DROP CONSTRAINT [FK_Message.Inbox_Employee.Data];");
         
            migrationBuilder.Sql(@"ALTER TABLE [Message.Object.Link] DROP INDEX [IX_Message.Object.Link_LinkedObject];");
            
            migrationBuilder.Sql(@"ALTER TABLE [Message.Participant.Info] DROP INDEX [IX_Message.Participant.Info_Body];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Participant.Info] DROP INDEX [IX_Message.Participant.Info_Contact];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Participant.Info] DROP INDEX [IX_Message.Participant.Info_Employee];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Participant.Info] DROP INDEX [IX_Message.Participant.Info_Team];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Participant.Info] DROP CONSTRAINT [FK_Message.Participant.Info_Employee];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Participant.Info] DROP CONSTRAINT [FK_Message.Participant.Info_Team];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Participant.Info] DROP CONSTRAINT [FK_Message.Participant.Info_Message.Content];");
            migrationBuilder.Sql(@"ALTER TABLE [Message.Participant.Info] DROP CONSTRAINT [FK_Notification.Participant.Info_Contact.Data];");

            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 0;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 1;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 2;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 4;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 8;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 16;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 32;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 64;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.ChannelType] WHERE ID = 128;");

            migrationBuilder.Sql("DELETE FROM [enum.Message.Participant.RoleType] WHERE ID = 1;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.Participant.RoleType] WHERE ID = 2;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.Participant.RoleType] WHERE ID = 3;");
            migrationBuilder.Sql("DELETE FROM [enum.Message.Participant.RoleType] WHERE ID = 4;");
        }
    }
}
