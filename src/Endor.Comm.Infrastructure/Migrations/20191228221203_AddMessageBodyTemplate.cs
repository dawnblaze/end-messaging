﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Comm.Infrastructure.Migrations
{
    public partial class AddMessageBodyTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDT",
                table: "Message.Delivery.Record",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "System.Message.TemplateType",
                columns: table => new
                {
                    AppliesToClassTypeID = table.Column<int>(type: "int", nullable: false),
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    ChannelType = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Message.TemplateType", x => new { x.AppliesToClassTypeID, x.ID });
                    table.ForeignKey(
                        name: "FK_System.Message.TemplateType_enum.Message.ChannelType",
                        column: x => x.ChannelType,
                        principalTable: "enum.Message.ChannelType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Message.Body.Template",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(GETUTCDATE())"),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14220))"),
                    IsActive = table.Column<bool>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false),
                    MessageTemplateType = table.Column<byte>(nullable: false),
                    ChannelType = table.Column<byte>(nullable: false),
                    LocationID = table.Column<byte>(type: "TINYINT", nullable: true),
                    CompanyID = table.Column<int>(type: "INT", nullable: true),
                    EmployeeID = table.Column<short>(type: "SMALLINT", nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Subject = table.Column<string>(maxLength: 2048, nullable: true),
                    Body = table.Column<string>(nullable: true),
                    AttachedFileCount = table.Column<byte>(nullable: false),
                    AttachedFileNames = table.Column<string>(type: "VARCHAR(MAX) SPARSE", nullable: true),
                    SizeInKB = table.Column<int>(nullable: true),
                    SortIndex = table.Column<short>(nullable: false),
                    MergeFieldList = table.Column<string>(nullable: true),
                    HasAttachment = table.Column<bool>(nullable: false, computedColumnSql: "(ISNULL(CASE WHEN [AttachedFileCount]>(0) THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))"),
                    HasMergeFields = table.Column<bool>(nullable: false, computedColumnSql: "(ISNULL(CASE WHEN LEN(MergeFieldList)>0 THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))"),
                    HasBody = table.Column<bool>(nullable: false, computedColumnSql: "(ISNULL(CASE WHEN LEN(Body)>0 THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Body.Template", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Message.Body.Template_System.Message.TemplateType",
                        columns: x => new { x.AppliesToClassTypeID, x.MessageTemplateType },
                        principalTable: "System.Message.TemplateType",
                        principalColumns: new[] { "AppliesToClassTypeID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Body.Template_AppliesToClassTypeID_MessageTemplateType",
                table: "Message.Body.Template",
                columns: new[] { "AppliesToClassTypeID", "MessageTemplateType" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Body.Template_Name",
                table: "Message.Body.Template",
                columns: new[] { "BID", "AppliesToClassTypeID", "MessageTemplateType", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Body.Template_Type",
                table: "Message.Body.Template",
                columns: new[] { "BID", "AppliesToClassTypeID", "MessageTemplateType", "SortIndex", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_System.Message.TemplateType_ChannelType",
                table: "System.Message.TemplateType",
                column: "ChannelType");

            migrationBuilder.Sql(@"
                ALTER TABLE [Message.Body.Template] WITH CHECK
                ADD CONSTRAINT [FK_Message.Body.Template_Business.Data]
                FOREIGN KEY([BID]) REFERENCES [Business.Data] ([BID])
                ;

                ALTER TABLE [Message.Body.Template] WITH CHECK
                ADD CONSTRAINT [FK_Message.Body.Template_Location.Data]
                FOREIGN KEY([BID], [LocationID]) REFERENCES [Location.Data] ([BID], [ID])
                ;

                ALTER TABLE [Message.Body.Template] WITH CHECK
                ADD CONSTRAINT [FK_Message.Body.Template_Company.Data]
                FOREIGN KEY([BID], [CompanyID]) REFERENCES [Company.Data] ([BID], [ID])
                ;

                ALTER TABLE [Message.Body.Template] WITH CHECK
                ADD CONSTRAINT [FK_Message.Body.Template_Employee.Data]
                FOREIGN KEY([BID], [EmployeeID]) REFERENCES [Employee.Data] ([BID], [ID])
                ;

                INSERT INTO [System.Message.TemplateType] ( [AppliesToClassTypeID], [ID], [Name], [IsSystem], [ChannelType] )
                VALUES
                       (  1012, 1, 'New Employee Login Email', 1, 4 )
                     , (  1012, 2, 'Employee Password Reset Email', 1, 4 )
                     , (  1012, 11, 'New Contact Login Email', 1, 4 )
                     , (  1012, 12, 'Contact Password Reset Email', 1, 4 )
                     , (  2000, 0, 'General Contact Email', 0, 4 )
                     , (  2000, 1, 'Send Statement Email', 0, 4 )
                     , (  3000, 0, 'General Company Email', 0, 4 )
                     , (  5000, 0, 'General Email', 0, 4 )
                     , ( 10000, 0, 'General Order Email', 0, 4 )
                     , ( 10000, 1, 'Send Invoice Email', 0, 4 )
                     , ( 10000, 2, 'Send Work Order Email', 0, 4 )
                     , ( 10000, 3, 'Send Packing Slip Email', 0, 4 )
                     , ( 10000, 4, 'New Order Confirmation Email', 0, 4 )
                     , ( 10000, 5, 'New Ecommerce Order Confirmation Email', 0, 4 )
                     , ( 10000, 6, 'Order Ready for Pickup Email', 0, 4 )
                     , ( 10000, 7, 'Shipping Confirmation Email', 0, 4 )
                     , ( 10000, 8, 'Dunning Email', 0, 4 )
                     , ( 10200, 0, 'General Estimate Email', 0, 4 )
                     , ( 10200, 1, 'Send Estimate Email', 0, 4 )
                     , ( 10200, 2, 'Estimate Followup Email', 0, 4 )
                     , ( 10300, 0, 'General Credit Memo Email', 0, 4 )
                     , ( 10300, 1, 'Send Credit Memo Email', 0, 4 )
                     , ( 10400, 0, 'General Purchase Order Email', 0, 4 )
                     , ( 10400, 1, 'Send Purchase Order Email', 0, 4 )
                     ;

                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Message.Body.Template");

            migrationBuilder.DropTable(
                name: "System.Message.TemplateType");

            migrationBuilder.DropColumn(
                name: "ModifiedDT",
                table: "Message.Delivery.Record");
        }
    }
}
