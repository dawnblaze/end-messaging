﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Comm.Infrastructure.Migrations
{
    public partial class AddDomainEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Email.SMTP.ProviderType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Email.SMTP.ProviderType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Email.SMTP.SecurityType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Email.SMTP.SecurityType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "System.Email.SMTP.ConfigurationType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    SMTPAddress = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    SMTPPort = table.Column<short>(type: "smallint", nullable: true),
                    SMTPSecurityType = table.Column<byte>(type: "tinyint", nullable: true),
                    SMTPAuthenticateFirst = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Email.SMTP.ConfigurationType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Domain.Email.Data",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    ID = table.Column<short>(type: "smallint", nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1022))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DomainName = table.Column<string>(type: "nvarchar(255)", nullable: false),
                    ProviderType = table.Column<byte>(type: "tinyint", nullable: false),
                    SMTPConfigurationType = table.Column<byte>(type: "tinyint", nullable: true),
                    SMTPAddress = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    SMTPPort = table.Column<short>(type: "smallint", nullable: true),
                    SMTPSecurityType = table.Column<byte>(type: "tinyint", nullable: true),
                    SMTPAuthenticateFirst = table.Column<bool>(nullable: true),
                    LastVerificationAttemptDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    LastVerificationSuccess = table.Column<bool>(nullable: true),
                    LastVerificationResult = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    LastVerifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    IsForAllLocations = table.Column<bool>(nullable: false, defaultValueSql: "((0))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domain.Email.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_enum.Email.SMTP.ProviderType",
                        column: x => x.ProviderType,
                        principalTable: "enum.Email.SMTP.ProviderType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_System.Email.SMTP.ConfigurationType",
                        column: x => x.SMTPConfigurationType,
                        principalTable: "System.Email.SMTP.ConfigurationType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_enum.Email.SMTP.SecurityType",
                        column: x => x.SMTPSecurityType,
                        principalTable: "enum.Email.SMTP.SecurityType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Domain.Email.LocationLink",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    DomainID = table.Column<short>(type: "smallint", nullable: false),
                    LocationID = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domain.Email.LocationLink", x => new { x.BID, x.DomainID, x.LocationID });
                    table.ForeignKey(
                        name: "FK_Domain.Email.LocationLink_Domain.Email.Data",
                        columns: x => new { x.BID, x.DomainID },
                        principalTable: "Domain.Email.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_ProviderType",
                table: "Domain.Email.Data",
                column: "ProviderType");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_SMTPConfigurationType",
                table: "Domain.Email.Data",
                column: "SMTPConfigurationType");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_SMTPSecurityType",
                table: "Domain.Email.Data",
                column: "SMTPSecurityType");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_Domain",
                table: "Domain.Email.Data",
                columns: new[] { "BID", "DomainName", "IsActive" });

            migrationBuilder.Sql(@"
                ALTER TABLE [Domain.Email.LocationLink] WITH CHECK
                ADD CONSTRAINT [FK_Domain.Email.LocationLink_Location.Data] FOREIGN KEY([BID], [LocationID])
                REFERENCES [Location.Data] ([BID], [ID])
                ;
                ALTER TABLE [Domain.Email.LocationLink]
                CHECK CONSTRAINT [FK_Domain.Email.LocationLink_Location.Data]
                ;
                ALTER TABLE [Domain.Email] WITH CHECK
                ADD CONSTRAINT [FK_Domain.Email.Data_Business.Data] FOREIGN KEY([BID])
                REFERENCES [Business.Data] ([ID])
                ;

                INSERT INTO [System.Email.SMTP.ConfigurationType] ([ID], [Name], [SMTPAddress], [SMTPPort], [SMTPSecurityType], [SMTPAuthenticateFirst])
                VALUES (0, N'None', NULL, NULL, NULL, NULL)
                     , (1, N'Yahoo', N'smtp.mail.yahoo.com', 587, 1, 1)
                     , (2, N'AOL', N'smtp.aol.com', 587, 2, 1)
                     , (3, N'iCloud', N'smtp.mail.me.com', 587, 1, 1)
                     , (4, N'Gmail via SMTP', N'', , , 1)
                     , (5, N'Office365 via SMTP', N'', , , 1)
                ;
                
                INSERT [enum.Email.SMTP.ProviderType] ([ID], [Name]) VALUES (0, N'None')
                     , (1, N'Google G Suite')
                     , (2, N'Microsoft 365')
                     , (3, N'Custom SMTP')
                ;

                INSERT [enum.Email.SMTP.SecurityType] ([ID], [Name]) VALUES (0, N'None (Not Secure)')
                    , (1, N'SSL')
                    , (2, N'TLS')
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Domain.Email.LocationLink");

            migrationBuilder.DropTable(
                name: "Domain.Email.Data");

            migrationBuilder.DropTable(
                name: "enum.Email.SMTP.ProviderType");

            migrationBuilder.DropTable(
                name: "System.Email.SMTP.ConfigurationType");

            migrationBuilder.DropTable(
                name: "enum.Email.SMTP.SecurityType");
        }
    }
}
