﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.Comm.Infrastructure.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Message.ChannelType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Message.ChannelType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Message.Participant.RoleType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Message.Participant.RoleType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Message.Body",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    BID = table.Column<short>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14110))"),
                    Subject = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    BodyFirstLine = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    HasBody = table.Column<bool>(nullable: false),
                    AttachedFileCount = table.Column<byte>(type: "tinyint", nullable: false, defaultValue: (byte)0),
                    AttachedFileNames = table.Column<string>(type: "varchar(max)", nullable: true),
                    HasAttachment = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [AttachedFileCount]>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,CONVERT([bit],(0))))"),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    WasModified = table.Column<bool>(nullable: false),
                    SizeInKB = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Body", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Message.Object.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    BodyID = table.Column<int>(type: "int", nullable: false),
                    LinkedObjectClassTypeID = table.Column<int>(type: "int", nullable: false),
                    LinkedObjectID = table.Column<int>(type: "int", nullable: false),
                    ID = table.Column<int>(type: "int", nullable: false),
                    SourceMessage = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    DisplayText = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Object.Link", x => new { x.BID, x.BodyID, x.LinkedObjectClassTypeID, x.LinkedObjectID });
                    table.ForeignKey(
                        name: "FK_Message.Related.Records_Message.Data",
                        columns: x => new { x.BID, x.BodyID },
                        principalTable: "Message.Body",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Message.Participant.Info",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    BID = table.Column<short>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14112))"),
                    BodyID = table.Column<int>(type: "int", nullable: false),
                    ParticipantRoleType = table.Column<byte>(type: "tinyint", nullable: false),
                    Channel = table.Column<byte>(type: "tinyint", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(512)", nullable: true),
                    IsMergeField = table.Column<bool>(type: "bit", nullable: false),
                    EmployeeID = table.Column<short>(type: "smallint", nullable: true),
                    TeamID = table.Column<int>(type: "int", nullable: true),
                    ContactID = table.Column<int>(type: "int", nullable: true),
                    DeliveredDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    IsDelivered = table.Column<bool>(nullable: true, computedColumnSql: "(case when [DeliveredDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Participant.Info", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Message.Participant.Info_enum.Message.ChannelType",
                        column: x => x.Channel,
                        principalTable: "enum.Message.ChannelType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Message.Participant.Info_enum.Message.Participant.RoleType",
                        column: x => x.ParticipantRoleType,
                        principalTable: "enum.Message.Participant.RoleType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Message.Participant.Info_Message.Body",
                        columns: x => new { x.BID, x.BodyID },
                        principalTable: "Message.Body",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Message.Delivery.Record",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ParticipantID = table.Column<int>(type: "int", nullable: false),
                    AttemptNumber = table.Column<short>(type: "smallint", nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14114))"),
                    IsPending = table.Column<bool>(nullable: false, computedColumnSql: @"(IsNull(case when [AttemptedDT] IS NOT NULL OR [ScheduledDT] IS NULL 
                                                        then CONVERT([bit],(0)) 
                                                        else CONVERT([bit],(1)) end, 
                                            CONVERT([bit],(1))))"),
                    ScheduledDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    AttemptedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    WasSuccessful = table.Column<bool>(type: "bit", nullable: false),
                    FailureMessage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Delivery.Record", x => new { x.BID, x.ParticipantID, x.AttemptNumber });
                    table.ForeignKey(
                        name: "FK_Message.Delivery.Record_Message.Participant",
                        columns: x => new { x.BID, x.ParticipantID },
                        principalTable: "Message.Participant.Info",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Message.Header",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false),
                    BID = table.Column<short>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14111))"),
                    ReceivedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    EmployeeID = table.Column<short>(type: "smallint", nullable: false),
                    ParticipantID = table.Column<int>(type: "int", nullable: false),
                    BodyID = table.Column<int>(type: "int", nullable: false),
                    IsRead = table.Column<bool>(type: "bit", nullable: false),
                    ReadDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletedOrExpiredDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    IsExpired = table.Column<bool>(type: "bit", nullable: false),
                    Channels = table.Column<byte>(type: "tinyint", nullable: false),
                    InSentFolder = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Header", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Message.Inbox_Message.Content",
                        columns: x => new { x.BID, x.BodyID },
                        principalTable: "Message.Body",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Message.Header_Message.Participant.Info",
                        columns: x => new { x.BID, x.ParticipantID },
                        principalTable: "Message.Participant.Info",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Header_BID_BodyID",
                table: "Message.Header",
                columns: new[] { "BID", "BodyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Header_Participant",
                table: "Message.Header",
                columns: new[] { "BID", "ParticipantID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Header_Employee",
                table: "Message.Header",
                columns: new[] { "BID", "EmployeeID", "ReceivedDT" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Header_EmployeeChannels",
                table: "Message.Header",
                columns: new[] { "BID", "EmployeeID", "Channels", "IsRead" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Object.Link_LinkedObject",
                table: "Message.Object.Link",
                columns: new[] { "BID", "LinkedObjectClassTypeID", "LinkedObjectID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Channel",
                table: "Message.Participant.Info",
                column: "Channel");

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_ParticipantRoleType",
                table: "Message.Participant.Info",
                column: "ParticipantRoleType");

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Body",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "BodyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Contact",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "ContactID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Employee",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Team",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "TeamID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Message.Delivery.Record");

            migrationBuilder.DropTable(
                name: "Message.Header");

            migrationBuilder.DropTable(
                name: "Message.Object.Link");

            migrationBuilder.DropTable(
                name: "Message.Participant.Info");

            migrationBuilder.DropTable(
                name: "enum.Message.ChannelType");

            migrationBuilder.DropTable(
                name: "enum.Message.Participant.RoleType");

            migrationBuilder.DropTable(
                name: "Message.Body");
        }
    }
}
