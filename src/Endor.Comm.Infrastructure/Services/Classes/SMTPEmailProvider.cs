﻿using Endor.Comm.Core.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Services.Classes;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.Core.SharedKernal;
using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using System;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Endor.Comm.Infrastructure.Services.Classes
{
    /// <summary>
    /// SMTP Provider to handle SMTP 
    /// </summary>
    public class SMTPEmailProvider : IEmailProvider
    {
        private DomainEmail DomainEmail { get; }

        public SMTPEmailProvider(DomainEmail domainEmail)
        {
            DomainEmail = domainEmail ?? throw new ArgumentNullException(nameof(domainEmail));
        }

        public async Task<BooleanResult> Validate(EmailAccountCredentials credentials)
        {
            try
            {
                var (serverAdress, port, security) = SMTPConfiguration();

                SmtpClient emailClient = await ConnectSMTPServer(serverAdress, port, security);

                return BooleanResult.IsSuccess;
            }
            catch (Exception ex)
            {
                return BooleanResult.IsFailure(ex.ToString());
            }
        }

        private (string serverAdress, short port, EmailSecurityType security) SMTPConfiguration()
        {
            if (!DomainEmail.SMTPPort.HasValue && String.IsNullOrWhiteSpace(DomainEmail.SMTPAddress) && !DomainEmail.SMTPSecurityType.HasValue)
                throw new ArgumentNullException($"The configuration needed to connect to SMTP server is not present.");

            return (
                DomainEmail.SMTPAddress,
                DomainEmail.SMTPPort.Value,
                DomainEmail.SMTPSecurityType ?? EmailSecurityType.None
            );
        }

        private static async Task<SmtpClient> ConnectSMTPServer(string serverAdress, short port, EmailSecurityType security)
        {
            var emailClient = new SmtpClient();

            if (security != EmailSecurityType.None)
            {
                if (security == EmailSecurityType.SSL)
                    await emailClient.ConnectAsync(serverAdress, port, true);

                if (security == EmailSecurityType.TLS)
                    await emailClient.ConnectAsync(serverAdress, port, SecureSocketOptions.StartTls);
            }

            return emailClient;
        }

        public async Task Send(EmailSendInfo sendInfo)
        {
            var (serverAdress, port, security) = SMTPConfiguration();

            var messageInfo = new MimeMessage();

            messageInfo.To.AddRange(sendInfo.Message?.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
            messageInfo.From.AddRange(sendInfo.Message?.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));

            messageInfo.Subject = sendInfo.Message.Subject;

            messageInfo.Body = new TextPart(TextFormat.Html)
            {
                Text = sendInfo.Message.Content
            };

            SmtpClient emailClient = await ConnectSMTPServer(serverAdress, port, security);

            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

            if (DomainEmail.SMTPAuthenticateFirst ?? false)
                emailClient.Authenticate(sendInfo.Message.EmailAccountEmailAddress, StringEncryptor.Decrypt(sendInfo.Credentials.Password, sendInfo.EncryptionPassPhrase));

            emailClient.Send(messageInfo);

            emailClient.Disconnect(true);
        }
    }


}
