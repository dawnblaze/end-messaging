﻿using Endor.Comm.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Infrastructure;
using Endor.Comm.Core.Entities;
using Endor.Comm;
using Microsoft.AspNetCore.Http;
using Endor.Tenant;
using Endor.Security;
using System;
using System.Threading;
using System.Threading.Tasks;
using Endor.Comm.Infrastructure.Common;

namespace Endor.Comm.Infrastructure.Data
{
    public class AppDbContext : DbContext
    {
        private readonly IDomainEventDispatcher _dispatcher;
        private readonly IHttpContextAccessor _httpCtxAccess;
        private readonly ITenantDataCache _iTenantCache;

        //public AppDbContext(DbContextOptions options) : base(options)
        //{
        //}

        public AppDbContext(DbContextOptions<AppDbContext> options, IDomainEventDispatcher dispatcher, IHttpContextAccessor httpCtx, ITenantDataCache iTenantCache)
            : base(options)
        {
            _dispatcher = dispatcher;
            _httpCtxAccess = httpCtx;
            _iTenantCache = iTenantCache;            
        }

        public DbSet<EnumMessageParticipantRoleType> EnumMessageParticipantRoleType { get; set; }
        public DbSet<EnumMessageChannelType> EnumMessageChannelType { get; set; }
        public DbSet<MessageHeader> MessageHeader { get; set; }
        public DbSet<MessageObjectLink> MessageObjectLink { get; set; }
        public DbSet<MessageDeliveryRecord> MessageDeliveryRecord { get; set; }
        public DbSet<MessageParticipantInfo> MessageParticipantInfo { get; set; }
        public DbSet<MessageBody> MessageBody { get; set; }
        public DbSet<SystemMessageTemplateType> SystemMessageTemplateType { get; set; }
        public DbSet<MessageBodyTemplate> MessageBodyTemplate { get; set; }
        public DbSet<DomainEmailLocationLink> DomainEmailLocationLink { get; set; }
        public DbSet<DomainEmail> DomainEmail { get; set; }
        public DbSet<SystemEmailSMTPConfigurationType> SystemEmailSMTPConfigurationType { get; set; }
        public DbSet<EnumEmailProviderType> EmailProviderType { get; set; }
        public DbSet<EnumEmailSecurityType> EmailSecurityType { get; set; }
        public DbSet<EmailAccountData> EmailAccountData { get; set; }
        public DbSet<EnumEmailAccountStatus> EmailAccountStatus { get; set; }
        public DbSet<EmailAccountTeamLink> EmailAccountTeamLink { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyAllConfigurationsFromCurrentAssembly();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            short? bid = _httpCtxAccess?.HttpContext.User.BID();
            //if (bid == null && _httpCtxAccess == null)
            //    throw new InvalidOperationException("Context has no way to get BID. Please pass a BID or IHttpContextAccessor at constructor time. Aborting configuration.");

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_iTenantCache.Get(bid.Value).Result.BusinessDBConnectionString);
                optionsBuilder.EnableSensitiveDataLogging(false);
            }

        }
        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
                       
            // dispatch events only if save was successful
            var entitiesWithEvents = ChangeTracker.Entries<BaseIdentityEntity>()
                .Select(e => e.Entity)
                .Where(e => e.Events.Any())
                .ToArray();

            int result = await base.SaveChangesAsync();

            // ignore events if no dispatcher provided
            if (_dispatcher == null) return result;

            foreach (var entity in entitiesWithEvents)
            {
                var events = entity.Events.ToArray();
                entity.Events.Clear();
                foreach (var domainEvent in events)
                {
                    _dispatcher.Dispatch(domainEvent);
                }
            }

            return result;
        }
    }

    public class AppDbContextFactory : MigrationCreatorContextFactory<AppDbContext>
    {
    }
}