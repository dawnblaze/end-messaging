﻿using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Infrastructure;
using Endor.Comm.Infrastructure.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Endor.Security;
using Endor.Comm.Core.Exceptions;
using Endor.Comm.Core.SharedKernal;
using Microsoft.Data.SqlClient;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using Endor.Comm.Core.Entities;
using Endor.Comm.Infrastructure.Common;

namespace Endor.Comm.Infrastructure.Data
{
    public class EfRepository : IRepository
    {
        protected readonly AppDbContext _dbContext;
        private readonly short _BID;
        ConcurrentDictionary<string, IEnumerable<string>> includesDict = new ConcurrentDictionary<string, IEnumerable<string>>();
        protected readonly bool _isForTest = false;
        public EfRepository(AppDbContext dbContext, IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = dbContext;
            _BID = httpContextAccessor.HttpContext.User.BID().Value;          
        }

        public EfRepository(AppDbContext dbContext, IHttpContextAccessor httpContextAccessor, bool IsTestOnly)
        {
            _dbContext = dbContext;
            _isForTest = IsTestOnly;
            _BID = httpContextAccessor.HttpContext.User.BID().Value;
        }

        public async Task<T> GetByIdAsync<T>(int id)
            where T : BaseIdentityEntity
        {
            var includes = GetIncludesForType(typeof(T));
            var query = _dbContext.Set<T>().AsQueryable();
            query = includes.Aggregate(query,
                                    (current, include) => current.Include(include));
            var result = await query.SingleOrDefaultAsync(e => e.ID == id && e.BID == _BID);
            if (result == null)
            {
                throw new EntityNotFoundException($"Could not find {typeof(T).Name} with ID of {id}" );
            }
            return result;
        }

        public async Task<T> GetByShortIdAsync<T>(short id)
            where T : BaseIdentityEntity
        {
            var includes = GetIncludesForType(typeof(T));
            var query = _dbContext.Set<T>().AsQueryable();
            query = includes.Aggregate(query,
                                    (current, include) => current.Include(include));
            var result = await query.SingleOrDefaultAsync(e => e.IDAsShort == id && e.BID == _BID);
            if (result == null)
            {
                throw new EntityNotFoundException($"Could not find {typeof(T).Name} with ID of {id}");
            }
            return result;
        }

        public async Task<T> GetByByteIdAsync<T>(byte id)
            where T : BaseIdentityEntity
        {
            var includes = GetIncludesForType(typeof(T));
            var query = _dbContext.Set<T>().AsQueryable();
            query = includes.Aggregate(query,
                                    (current, include) => current.Include(include));
            var result = await query.SingleOrDefaultAsync(e => e.IDAsByte == id && e.BID == _BID);
            if (result == null)
            {
                throw new EntityNotFoundException($"Could not find {typeof(T).Name} with ID of {id}");
            }
            return result;
        }

        public async Task<List<T>> ListAsync<T>() where T : class
        {
            var includes = GetIncludesForType(typeof(T));
            var query = _dbContext.Set<T>().AsQueryable();
            query = includes.Aggregate(query,
                                    (current, include) => current.Include(include));
            return await query.ToListAsync();
        }

        public async Task<List<T>> ListEntityAsync<T>(ISpecification<T> spec = null) where T : BaseEntity
        {
            if (spec != null) ((BaseSpecification<T>)spec).AddCriteria(new FilterByBIDCriteria<T>(_BID));
            return await ApplySpecification(spec).ToListAsync();
        }

        public async Task<T> AddAsync<T>(T entity) where T : BaseEntity
        {
            if (!this._isForTest) await entity.DoBeforeCreateAsync(this,_BID);
            _dbContext.Set<T>().Add(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        private async Task DoBeforeDeleteAsync<T>(T entity) 
            where T : BaseEntity
        {
            await entity.DoBeforeDeleteAsync(this);
        }

        public async Task<int> DeleteAsync<T>(T entity) where T : BaseEntity
        {
            await DoBeforeDeleteAsync(entity);
            _dbContext.Set<T>().Remove(entity);
            await _dbContext.SaveChangesAsync();
            return 0;
        }

        public async Task<int> DeleteListAsync<T>(List<T> entities) where T : BaseEntity
        {
            foreach (var entity in entities)
            {
                await DoBeforeDeleteAsync(entity);
                _dbContext.Set<T>().Remove(entity);
            };
            await _dbContext.SaveChangesAsync();
            return 0;
        }

        private async Task DoBeforeUpdateAsync<T>(T entity)
            where T : BaseIdentityEntity//class, IAtom
        {
            //var oldModel = await GetByIdAsync<T>(entity.ID);
            await entity.DoBeforeUpdateAsync(this, entity);
        }

        public async Task<int> UpdateAsync<T>(T entity) where T : BaseIdentityEntity
        {
            await DoBeforeUpdateAsync(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
            return 0;
        }

        public async Task<int> UpdateListAsync<T>(List<T> entities) where T : BaseIdentityEntity
        {
            foreach (var entity in entities) {
                await DoBeforeUpdateAsync(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            };
            await _dbContext.SaveChangesAsync();
            return 0;
        }

        public async Task<T> CloneAsync<T>(int ID, Expression<Func<T, string>> nameSelector, [Optional]Type IDtype) where T : BaseIdentityEntity
        {
            if (IDtype == null) IDtype = typeof(int);
            var clone = await this.GetClone<T>(ID, IDtype);
            await clone.DoBeforeCloneAsync(this);
            var newName = this.GetNextClonedName(_dbContext.Set<T>().AsNoTracking().Where(t => t.BID == _BID).ToList(), clone, nameSelector.Compile());
            //https://stackoverflow.com/questions/5075484/property-selector-expressionfunct-how-to-get-set-value-to-selected-property
            var prop = (PropertyInfo)((MemberExpression)nameSelector.Body).Member;
            prop.SetValue(clone, newName, null);

            await clone.DoBeforeCreateAsync(this, _BID);
            _dbContext.Set<T>().Add(clone);
            await _dbContext.SaveChangesAsync();
            return clone;
        }

        /// <summary>
        /// Gets an untracked copy of the model by ID
        /// </summary>
        /// <param name="ID">Model ID</param>
        /// <returns></returns>
        public async Task<T> GetClone<T>(int ID, Type IDtype) where T : BaseIdentityEntity
        {
            var includes = GetIncludesForType(typeof(T));
            
            //trick with EF to clone:
            //https://stackoverflow.com/questions/42504782/clone-and-link-child-using-asnotracking-in-entity-framework
            //loads entities without tracking them so changing the IDs out and calling Add makes them insert new records
            var query = _dbContext.Set<T>().AsNoTracking().AsQueryable();
            query = query.Where(a => a.BID == _BID);
            if (IDtype == typeof(short))
            {
                query = query.Where(a => a.IDAsShort == ID);
            }
            else
            {
                query = query.Where(a => a.ID == ID);
            }
            query = includes.Aggregate(query,
                                    (current, include) => current.Include(include));
            return await query.FirstOrDefaultAsync();
        }

        /// <summary>
        /// Requests an ID from SQL via SPROC as an INT
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classtypeID">Class Type ID</param>
        /// <returns></returns>
        public async Task<int> RequestIDIntegerAsync(short bid, int classtypeID)
        {
            string sqlExecute = "EXEC @Result = dbo.[Util.ID.GetID] @BID, @ClassTypeID, @Count";

            List<object> myParams = new List<object>(){
                new SqlParameter("@BID", bid),
                new SqlParameter("@ClassTypeID", classtypeID),
                new SqlParameter("@Count", 1)
            };

            SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };
            myParams.Add(resultParam);

            await _dbContext.Database.ExecuteSqlRawAsync(sqlExecute, parameters: myParams);

            return Convert.ToInt32(resultParam.Value);
        }


        private IQueryable<T> ApplySpecification<T>(ISpecification<T> spec) where T : class
        {
            var includes = GetIncludesForType(typeof(T));
            if (spec == null)
            {
                var query = _dbContext.Set<T>().AsQueryable();
                query = includes.Aggregate(query,
                                    (current, include) => current.Include(include));
                return query;
            }
            else return EfSpecificationEvaluator<T>.GetQuery(_dbContext.Set<T>().AsQueryable(), spec, includes);

        }

        private IEnumerable<string> GetIncludesForType(System.Type type)
        {
            var typeName = type.Name;
            IEnumerable<string> includes;
            if (!includesDict.TryGetValue(typeName, out includes))
            {
                includes = IncludeExtract.IterateProps(type).ToArray();
                includesDict.TryAdd(typeName, includes);
            }
            return includes;
        }

        protected PropertyInfo SetPropertyIfExists(object obj, string propertyName, object newValue)
        {
            var prop = obj.GetType().GetProperty(propertyName);
            if (prop != null && prop.CanWrite)
            {
                prop.SetValue(obj, newValue);
            }
            return prop;
        }

        public Task<int> ExecuteSqlRawAsync(string sql, params object[] parameters)
        {
            return _dbContext.Database.ExecuteSqlRawAsync(sql, parameters);
        }

        /// <summary>
        /// Gets the Next Name for a Cloned entity
        /// </summary>
        /// <param name="query"></param>
        /// <param name="originalName"></param>
        /// <param name="nameSelector"></param>
        /// <returns></returns>
        private string GetNextClonedName<T>(IEnumerable<dynamic> query, T objectToCloneFrom, Func<T, string> nameSelector) where T : class
        {
            string originalName = nameSelector(objectToCloneFrom);
            var names = query.Select(t =>
            {
                if (nameSelector(t) == originalName)
                    return new { Name = nameSelector(t), LastCloneNumber = (int?)0 };

                if (nameSelector(t) == (originalName + " (Clone)"))
                    return new { Name = nameSelector(t), LastCloneNumber = (int?)1 };

                if ((nameSelector(t).StartsWith(originalName + " (Clone) ") && nameSelector(t).EndsWith("(Clone)")))
                    return new { Name = nameSelector(t), LastCloneNumber = (int?)null };
                bool hasDataPastClone = nameSelector(t).Length > originalName.Length + "(Clone) (".Length;
                if (hasDataPastClone)
                {
                    int startIndex = originalName.Length + " (Clone) (".Length;
                    int lengthSub = nameSelector(t).Length - originalName.Length - " (Clone) (".Length - 1;
                    if (lengthSub > 0)
                    {
                        string substringNumberSelection = nameSelector(t).Substring(startIndex, lengthSub);
                        bool matchedNameAndCloneWithNumber = substringNumberSelection.All(c => (char.IsNumber(c)));
                        if (matchedNameAndCloneWithNumber)
                        {

                            int _parseIntLastCloneNumber;

                            if(!Int32.TryParse(substringNumberSelection, out _parseIntLastCloneNumber))
                            {
                                throw new Exception($"Error parsing {substringNumberSelection} to int.");
                            }
                            else
                            {
                                return new { Name = nameSelector(t), LastCloneNumber = (int?)_parseIntLastCloneNumber };
                            }

                            
                        }
                            
                    }
                }

                return new { Name = nameSelector(t), LastCloneNumber = (int?)null };
            }).OrderByDescending(t => t.LastCloneNumber).ToList();

            var lastOfTheNames = names.FirstOrDefault();

            if (lastOfTheNames != null && lastOfTheNames.LastCloneNumber.GetValueOrDefault(0) > 0)
            {
                return originalName + " (Clone)" + $" ({lastOfTheNames.LastCloneNumber + 1})";
            }

            return originalName + " (Clone)";
        }

        public virtual async Task<Tuple<bool, string>> CanDeleteAsync(BaseIdentityEntity entity)
        {
            try
            {
                var bidParam = new SqlParameter("@BID", entity.BID);
                var idParam = new SqlParameter("@ID", entity.ID);
                var classTypeIdParam = new SqlParameter("@ClassTypeID", entity.ClassTypeID);
                var showCantDeleteReasonParam = new SqlParameter("@ShowCantDeleteReason", true);
                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Bit);
                var cantDeleteReasonParam = new SqlParameter("@CantDeleteReason", System.Data.SqlDbType.VarChar, 1024);

                resultParam.Direction = System.Data.ParameterDirection.Output;
                cantDeleteReasonParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    bidParam,
                    idParam,
                    classTypeIdParam,
                    showCantDeleteReasonParam,
                    cantDeleteReasonParam,
                    resultParam
                };

                int rowResult = await ExecuteSqlRawAsync("EXEC dbo.[IAtom.Action.CanDelete] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT, @CantDeleteReason OUTPUT;", parameters: myParams);
                if (resultParam.Value != null)
                    if ((bool)resultParam.Value)
                        return new Tuple<bool, string>(true, $"Can Delete: {entity.ID}");
                    else
                        return new Tuple<bool, string>(false, cantDeleteReasonParam.Value.ToString());
                else
                    return new Tuple<bool, string>(false, cantDeleteReasonParam.Value.ToString());
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

    }
}
