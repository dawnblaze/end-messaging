﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    class MessageBodyConfiguration : IEntityTypeConfiguration<MessageBody>
    {
        public void Configure(EntityTypeBuilder<MessageBody> entity)
        {
            entity.ToTable("Message.Body");

            //Columns
            entity.Property(e => e.BID).IsRequired();
            entity.Property(e => e.ID).IsRequired();
            entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14110))");
            entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()");
            entity.Property(e => e.BodyFirstLine).HasColumnType("nvarchar(100)");
            entity.Property(e => e.Subject).HasColumnType("nvarchar(250)"); ;
            entity.Property(e => e.HasBody).IsRequired();
            entity.Property(e => e.AttachedFileCount).HasColumnType("tinyint").IsRequired().HasDefaultValue("0");
            entity.Property(e => e.AttachedFileNames).HasColumnType("varchar(max)");
            entity.Property(e => e.HasAttachment)
                .HasComputedColumnSql(
                    "(isnull(case when [AttachedFileCount]>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,CONVERT([bit],(0))))");

            entity.Property(e => e.MetaData).HasColumnType("xml");
            entity.Property(e => e.WasModified).IsRequired();
            entity.Property(e => e.SizeInKB).HasColumnType("int");

            //primary key
            entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Message.Body");

            //foreign keys
            //This would be created by custom SQL command
            /*entity.HasOne<BusinessData>().WithMany()
                .HasForeignKey(e => e.BID)
                .HasConstraintName("FK_Message.Content_Business.Data");*/

        }
    }
}
