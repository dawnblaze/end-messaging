﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    class MessageParticipantInfoConfiguration : IEntityTypeConfiguration<MessageParticipantInfo>
    {
        public void Configure(EntityTypeBuilder<MessageParticipantInfo> entity)
        {
            entity.ToTable("Message.Participant.Info");

            //Columns
            entity.Property(e => e.BID).IsRequired();
            entity.Property(e => e.ID).IsRequired();
            entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14112))");
            entity.Property(e => e.BodyID).HasColumnType("int").IsRequired();
            entity.Property(e => e.ParticipantRoleType).HasColumnType("tinyint").IsRequired();
            entity.Property(e => e.Channel).HasColumnType("tinyint").IsRequired();
            entity.Property(e => e.UserName).HasColumnType("nvarchar(512)");
            entity.Property(e => e.IsMergeField).HasColumnType("bit").IsRequired();
            entity.Property(e => e.EmployeeID).HasColumnType("smallint");
            entity.Property(e => e.TeamID).HasColumnType("int");
            entity.Property(e => e.ContactID).HasColumnType("int");
            entity.Property(e => e.DeliveredDT).HasColumnType("datetime2(2)");
            entity.Property(e => e.IsDelivered)
                .HasComputedColumnSql("(case when [DeliveredDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)");


            //primary key
            entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Message.Participant.Info");

            // indexes
            entity.HasIndex(e => new { e.BID, e.BodyID }).HasName("IX_Message.Participant.Info_Body");
            entity.HasIndex(e => new { e.BID, e.ContactID }).HasName("IX_Message.Participant.Info_Contact");
            entity.HasIndex(e => new { e.BID, e.EmployeeID }).HasName("IX_Message.Participant.Info_Employee");
            entity.HasIndex(e => new { e.BID, e.TeamID }).HasName("IX_Message.Participant.Info_Team");

            //foreign keys
            entity.HasOne<EnumMessageChannelType>().WithMany()
                .HasForeignKey(e => e.Channel)
                .HasConstraintName("FK_Message.Participant.Info_enum.Message.ChannelType");

            entity.HasOne<EnumMessageParticipantRoleType>().WithMany()
                .HasForeignKey(e => e.ParticipantRoleType)
                .HasConstraintName("FK_Message.Participant.Info_enum.Message.Participant.RoleType");

            entity.HasOne(e => e.MesssageBody).WithMany(body => body.MessageParticipantInfos)
                .HasForeignKey(e => new { e.BID, e.BodyID })
                .HasConstraintName("FK_Message.Participant.Info_Message.Body");

            /* These would need to be created by a custom sql command
            entity.HasOne(e => e.EmployeeData).WithMany()
                .HasForeignKey(e => new { e.BID, e.EmployeeID })
                .HasConstraintName("FK_Message.Participant.Info_Employee");

            entity.HasOne(e => e.EmployeeTeam).WithMany()
                .HasForeignKey(e => new { e.BID, e.TeamID })
                .HasConstraintName("FK_Message.Participant.Info_Team");

            entity.HasOne<ContactData>().WithMany()
                .HasForeignKey(e => new { e.BID, e.ContactID })
                .HasConstraintName("FK_Notification.Participant.Info_Contact.Data");
                */
        }
    }
}
