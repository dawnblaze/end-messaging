﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class EnumEmailAccountConfiguration : IEntityTypeConfiguration<EnumEmailAccountStatus>
    {
        public void Configure(EntityTypeBuilder<EnumEmailAccountStatus> entity)
        {
            entity.ToTable("enum.EmailAccountStatus");

            entity.HasKey(e => new { e.ID }).HasName("PK_enum.EmailAccountStatus");
            entity.Property(e => e.ID).HasColumnType("tinyint");
            entity.Property(e => e.Name).IsUnicode(false);
        }
    }
}
