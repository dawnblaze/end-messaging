﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class EmailAccountTeamLinkConfiguration : IEntityTypeConfiguration<EmailAccountTeamLink>
    {
        public void Configure(EntityTypeBuilder<EmailAccountTeamLink> entity)
        {
            entity.ToTable("Email.Account.TeamLink");
            entity.HasKey(e => new { e.BID, e.EmailAccountID, e.TeamID })
                .HasName("PK_Email.Account.TeamLink");

            entity.HasIndex(e => new { e.BID, e.TeamID })
                .HasName("IX_Email.Account.TeamLink_Team");

            entity.HasOne(e => e.EmailAccount)
                .WithMany(c => c.EmailAccountTeamLinks)
                .HasForeignKey(e => new { e.BID, e.EmailAccountID })
                .HasConstraintName("FK_Email.Account.TeamLink_Email.Account.Data");
            /* Foreign Key will have to be creaed manually
             * entity.HasOne(e => e.Team)
                .WithMany(c => c.EmailAccountTeamLinks)
                .HasForeignKey(e => new { e.BID, e.TeamID })
                .HasConstraintName("FK_Email.Account.TeamLink_Employee.Team");*/
        }
    }
}
