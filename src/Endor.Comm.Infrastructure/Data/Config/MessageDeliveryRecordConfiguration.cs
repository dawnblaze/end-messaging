﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    class MessageDeliveryRecordConfiguration : IEntityTypeConfiguration<MessageDeliveryRecord>
    {
        public void Configure(EntityTypeBuilder<MessageDeliveryRecord> entity)
        {
            entity.ToTable("Message.Delivery.Record");

            //Columns
            entity.Property(e => e.BID).IsRequired();
            entity.Property(e => e.ID).HasColumnName("ID").IsRequired();
            entity.Property(e => e.ParticipantID).HasColumnType("int").IsRequired();
            entity.Property(e => e.AttemptNumber).HasColumnType("smallint").IsRequired();
            entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14114))");
            entity.Property(e => e.IsPending)
                .HasComputedColumnSql(@"(IsNull(case when [AttemptedDT] IS NOT NULL OR [ScheduledDT] IS NULL 
                                                        then CONVERT([bit],(0)) 
                                                        else CONVERT([bit],(1)) end, 
                                            CONVERT([bit],(1))))");
            entity.Property(e => e.ScheduledDT).HasColumnType("datetime2(2)");
            entity.Property(e => e.AttemptedDT).HasColumnType("datetime2(2)");
            entity.Property(e => e.WasSuccessful).HasColumnType("bit").IsRequired();
            entity.Property(e => e.FailureMessage).HasColumnType("nvarchar(max)");
            entity.Property(e => e.MetaData).HasColumnType("xml");

            //primary key
            entity.HasKey(e => new { e.BID, e.ParticipantID, e.AttemptNumber }).HasName("PK_Message.Delivery.Record");

            //foreign keys
            entity.HasOne<MessageParticipantInfo>().WithMany(participant => participant.MessageDeliveryRecords)
                .HasForeignKey(e => new { e.BID, e.ParticipantID })
                .HasConstraintName("FK_Message.Delivery.Record_Message.Participant");
        }
    }
}
