﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class SystemMessageTemplateTypeConfiguration : IEntityTypeConfiguration<SystemMessageTemplateType>
    {
        public void Configure(EntityTypeBuilder<SystemMessageTemplateType> entity)
        {
            entity.ToTable("System.Message.TemplateType");

            //Columns
            entity.Property(e => e.AppliesToClassTypeID).HasColumnType("int").IsRequired();
            entity.Property(e => e.ID).HasColumnName("ID").HasColumnType("tinyint").IsRequired();
            entity.Property(e => e.Name).HasColumnType("varchar(100)").IsRequired();
            entity.Property(e => e.IsSystem).HasColumnType("bit").IsRequired();
            entity.Property(e => e.ChannelType).HasColumnType("tinyint").IsRequired();

            //primary key
            entity.HasKey(e => new { e.AppliesToClassTypeID, e.ID }).HasName("PK_System.Message.TemplateType");

            //foreign keys
            entity.HasOne<EnumMessageChannelType>().WithMany()
                .HasForeignKey(e => e.ChannelType)
                .HasConstraintName("FK_System.Message.TemplateType_enum.Message.ChannelType");
        }
    }
}
