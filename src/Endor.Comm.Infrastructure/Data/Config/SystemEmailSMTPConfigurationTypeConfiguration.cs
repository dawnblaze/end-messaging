﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class SystemEmailSMTPConfigurationTypeConfiguration : IEntityTypeConfiguration<SystemEmailSMTPConfigurationType>
    {
        public void Configure(EntityTypeBuilder<SystemEmailSMTPConfigurationType> entity)
        {
            entity.ToTable("System.Email.SMTP.ConfigurationType");

            entity.HasKey(t => new { t.ID });
            entity.Property(e => e.Name).HasColumnType("nvarchar(100)");
            entity.Property(e => e.SMTPAddress).HasColumnType("nvarchar(255)");
            entity.Property(e => e.ID).HasColumnType("tinyint");
            entity.Property(e => e.SMTPPort).HasColumnType("smallint");
            entity.Property(e => e.SMTPSecurityType).HasColumnType("tinyint");
        }
    }
}
