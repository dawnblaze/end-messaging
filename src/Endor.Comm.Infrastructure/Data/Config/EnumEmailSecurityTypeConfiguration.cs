﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class EnumEmailSecurityTypeConfiguration : IEntityTypeConfiguration<EnumEmailSecurityType>
    {
        public void Configure(EntityTypeBuilder<EnumEmailSecurityType> entity)
        {
            entity.ToTable("enum.Email.SMTP.SecurityType");
            entity.HasKey(t => new { t.ID });
            entity.Property(e => e.Name).HasColumnType("nvarchar(100)").IsRequired();
            entity.Property(e => e.ID).HasColumnType("tinyint");
        }
    }
}
