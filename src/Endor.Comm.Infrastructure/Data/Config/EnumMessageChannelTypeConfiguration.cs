﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class EnumMessageChannelTypeConfiguration : IEntityTypeConfiguration<EnumMessageChannelType>
    {
        public void Configure(EntityTypeBuilder<EnumMessageChannelType> entity)
        {
            entity.ToTable("enum.Message.ChannelType");
            entity.HasKey(e => e.ID);
            entity.Property(e => e.ID).HasColumnType("tinyint");
            entity.Property(e => e.Name).HasMaxLength(100).IsRequired();
        }
    }
}
