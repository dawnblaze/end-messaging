﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class EmailAccountDataConfiguration : IEntityTypeConfiguration<EmailAccountData>
    {
        public void Configure(EntityTypeBuilder<EmailAccountData> entity)
        {
            entity.ToTable("Email.Account.Data");

            entity.HasKey(e => new { e.BID, e.IDAsShort }).HasName("PK_Email.Account.Data");
            entity.Ignore(e => e.ID);
            entity.Property(e => e.IDAsShort).HasColumnName("ID").HasColumnType("smallint").IsRequired();

            entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1023))");
            entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
            entity.Property(e => e.IsActive).HasComputedColumnSql("(isnull(case when [StatusType]=(2) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))").IsRequired();
            entity.Property(e => e.StatusType).IsRequired();
            entity.Property(e => e.DomainEmailID).IsRequired();
            entity.Property(e => e.UserName).HasColumnType("varchar(255)").IsRequired();
            entity.Property(e => e.DomainName).HasColumnType("varchar(255)").IsRequired();
            entity.Property(e => e.IsPrivate).HasComputedColumnSql("(isnull(case when [EmployeeID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))").IsRequired();
            entity.Property(e => e.EmployeeID);
            entity.Property(e => e.Credentials).HasColumnType("varchar(max)");
            entity.Property(e => e.CredentialsExpDT).HasColumnType("DATETIME2(2)");
            entity.Property(e => e.LastEmailSuccessDT).HasColumnType("DATETIME2(2)");
            entity.Property(e => e.LastEmailFailureDT).HasColumnType("DATETIME2(2)");
            entity.Property(e => e.EmailAddress).HasComputedColumnSql("(concat([UserName],'@',[DomainName]))");
            entity.Property(e => e.AliasUserNames).HasColumnType("varchar(1024)");
            entity.Property(e => e.DisplayName).HasColumnType("varchar(255)");

            entity.HasIndex(e => new { e.BID, e.EmployeeID })
                .HasName("IX_Email.Account.Data_Employee");

            entity.HasIndex(e => new { e.BID, e.EmailAddress, e.IsActive, e.IsPrivate })
                .HasName("IX_Email.Account.Data_Email");

            /* foreign key will be created externally
             * entity.HasOne(e => e.Business).WithMany()
                .HasForeignKey(e => e.BID)
                //.OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("FK_Email.Account.Data_Business.Data");*/

            /* foreign key will be created externally
            entity.HasOne(e => e.Employee)
                .WithMany()
                .HasForeignKey(t => new { t.BID, t.EmployeeID })
                .HasConstraintName("FK_Email.Account.Data_Employee.Data")
                .OnDelete(DeleteBehavior.Restrict);*/

            entity.HasOne(e => e.EnumEmailAccountStatus)
                .WithMany()
                .HasForeignKey(t => new { t.StatusType })
                .HasConstraintName("FK_Email.Account.Data_enum.Email.AccountStatus")
                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(e => e.DomainEmail)
                .WithMany()
                .HasForeignKey(t => new { t.BID, t.DomainEmailID })
                .HasConstraintName("FK_Email.Account.Data_Domain.Email.Data")
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
