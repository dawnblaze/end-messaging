﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    class MessageHeaderConfiguration : IEntityTypeConfiguration<MessageHeader>
    {
        public void Configure(EntityTypeBuilder<MessageHeader> entity)
        {
            entity.ToTable("Message.Header");
            entity.Property(e => e.BID).IsRequired();
            entity.Property(e => e.ID).HasColumnType("int").IsRequired();
            entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14111))");
            entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()");
            entity.Property(e => e.ReceivedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()");
            entity.Property(e => e.EmployeeID).HasColumnType("smallint").IsRequired();
            entity.Property(e => e.ParticipantID).HasColumnType("int").IsRequired();
            entity.Property(e => e.BodyID).HasColumnType("int").IsRequired();
            entity.Property(e => e.IsRead).HasColumnType("bit").IsRequired();
            entity.Property(e => e.ReadDT).HasColumnType("datetime2(2)");
            entity.Property(e => e.IsDeleted).HasColumnType("bit").IsRequired();
            entity.Property(e => e.IsExpired).HasColumnType("bit").IsRequired();
            entity.Property(e => e.DeletedOrExpiredDT).HasColumnType("datetime2(2)");
            entity.Property(e => e.Channels).HasColumnType("tinyint").IsRequired();
            entity.Property(e => e.InSentFolder).HasColumnType("bit").IsRequired();

            //primary key
            entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Message.Header");

            // indexes
            entity.HasIndex(e => new { e.BID, e.EmployeeID, e.ReceivedDT }).HasName("IX_Message.Header_Employee");
            entity.HasIndex(e => new { e.BID, e.ParticipantID }).HasName("IX_Message.Header_Participant");
            entity.HasIndex(e => new { e.BID, e.EmployeeID, e.Channels, e.IsRead }).HasName("IX_Message.Header_EmployeeChannels");


            //foreign keys
            entity.HasOne(e => e.ParticipantInfo)
                .WithMany(mpi => mpi.MessageHeaders)
                .HasForeignKey(e => new { e.BID, e.ParticipantID })
                .HasConstraintName("FK_Message.Header_Message.Participant.Info");

            entity.HasOne(e => e.MessageBody).WithMany()
                .HasForeignKey(e => new { e.BID, e.BodyID })
                .HasConstraintName("FK_Message.Inbox_Message.Content");

            /* This one would need a custom sql command to create
            entity.HasOne<EmployeeData>().WithMany()
                .HasForeignKey(e => new { e.BID, e.EmployeeID })
                .HasConstraintName("FK_Message.Inbox_Employee.Data");
                */

            //[NotMapped]
            entity.Ignore(e => e._messageBodyString);

        }
    }
}
