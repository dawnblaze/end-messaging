﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class MessageBodyTemplateConfiguration : IEntityTypeConfiguration<MessageBodyTemplate>
    {
        public void Configure(EntityTypeBuilder<MessageBodyTemplate> entity)
        {
            entity.ToTable("Message.Body.Template");

            // primary keys
            entity.HasKey(e => new { e.BID, e.IDAsShort }).HasName("PK_Message.Body.Template");
            entity.Property(e => e.IDAsShort).HasColumnName("ID").HasColumnType("smallint").IsRequired();
            entity.Ignore(e => e.ID);
            entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14220))");
            entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(GETUTCDATE())");

            entity.Property(e => e.LocationID).HasColumnType("TINYINT");
            entity.Property(e => e.CompanyID).HasColumnType("INT");
            entity.Property(e => e.EmployeeID).HasColumnType("SMALLINT");
            entity.Property(e => e.AttachedFileNames).HasColumnType("VARCHAR(MAX) SPARSE");
            entity.Property(e => e.ParticipantInfoJSON).HasColumnType("VARCHAR(MAX)");

            // computed columns
            entity.Property(e => e.HasAttachment).HasComputedColumnSql("(ISNULL(CASE WHEN [AttachedFileCount]>(0) THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))");
            entity.Property(e => e.HasMergeFields).HasComputedColumnSql("(ISNULL(CASE WHEN LEN(MergeFieldList)>0 THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))");
            entity.Property(e => e.HasBody).HasComputedColumnSql("(ISNULL(CASE WHEN LEN(Body)>0 THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))");

            entity.Ignore(e => e.Participants);

            // foreign keys
            entity.HasOne(e => e.TemplateType).WithMany().HasForeignKey(e => new { e.AppliesToClassTypeID, e.MessageTemplateType }).HasPrincipalKey(e => new { e.AppliesToClassTypeID, e.ID }).HasConstraintName("FK_Message.Body.Template_System.Message.TemplateType");

            // indexes
            entity.HasIndex(e => new { e.BID, e.AppliesToClassTypeID, e.MessageTemplateType, e.Name, e.IsActive }).HasName("IX_Message.Body.Template_Name");
            entity.HasIndex(e => new { e.BID, e.AppliesToClassTypeID, e.MessageTemplateType, e.SortIndex, e.IsActive }).HasName("IX_Message.Body.Template_Type");
        }
    }
}
