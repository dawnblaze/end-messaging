﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class DomainEmailConfiguration : IEntityTypeConfiguration<DomainEmail>
    {
        public void Configure(EntityTypeBuilder<DomainEmail> entity)
        {
            entity.ToTable("Domain.Email.Data");
            entity.HasKey(t => new { t.BID, t.IDAsShort });

            entity.HasIndex(e => new { e.BID, e.DomainName, e.IsActive })
                .HasName("IX_Domain.Email.Data_Domain");
            entity.Ignore(e => e.ID);
            entity.Property(e => e.BID).HasColumnType("smallint");
            entity.Property(e => e.IDAsShort).HasColumnName("ID").HasColumnType("smallint").IsRequired();
            entity.Property(e => e.CreatedDate).HasColumnType("date");
            entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");
            entity.Property(e => e.ProviderType).HasColumnType("tinyint");
            entity.Property(e => e.SMTPConfigurationType).HasColumnType("tinyint");
            entity.Property(e => e.SMTPPort).HasColumnType("smallint");
            entity.Property(e => e.SMTPSecurityType).HasColumnType("tinyint");
            entity.Property(e => e.LastVerificationAttemptDT).HasColumnType("DATETIME2(2)");
            entity.Property(e => e.LastVerifiedDT).HasColumnType("DATETIME2(2)");
            entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1022))");
            entity.Property(e => e.IsForAllLocations).HasDefaultValueSql("((0))");
            entity.Property(e => e.IsActive).IsRequired();
            entity.Property(e => e.DomainName).IsRequired().HasColumnType("nvarchar(255)");
            entity.Property(e => e.SMTPAddress).HasColumnType("nvarchar(255)");
            entity.Property(e => e.LastVerificationResult).HasColumnType("nvarchar(255)");
            /*entity.HasOne<BusinessData>().WithMany()
                .HasForeignKey(e => e.BID)
                //.OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("FK_Domain.Email.Data_Business.Data");*/

            entity.HasOne(t => t.SecurityType).WithMany()
                .HasForeignKey(t => new { t.SMTPSecurityType })
                .HasConstraintName("FK_Domain.Email.Data_enum.Email.SMTP.SecurityType").OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(t => t.Provider).WithMany()
                .HasForeignKey(t => new { t.ProviderType })
                .HasConstraintName("FK_Domain.Email.Data_enum.Email.SMTP.ProviderType").OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(t => t.ConfigurationType).WithMany(x => x.DomainEmails)
                .HasForeignKey(t => new { t.SMTPConfigurationType })
                .HasConstraintName("FK_Domain.Email.Data_System.Email.SMTP.ConfigurationType").OnDelete(DeleteBehavior.Restrict);

        }
    }
}
