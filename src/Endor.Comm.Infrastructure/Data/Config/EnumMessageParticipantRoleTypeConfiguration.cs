﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    class EnumMessageParticipantRoleTypeConfiguration : IEntityTypeConfiguration<EnumMessageParticipantRoleType>
    {
        public void Configure(EntityTypeBuilder<EnumMessageParticipantRoleType> entity)
        {
            entity.ToTable("enum.Message.Participant.RoleType");
            entity.HasKey(e => e.ID);
            entity.Property(e => e.ID).HasColumnType("tinyint");
            entity.Property(e => e.Name).HasMaxLength(100).IsRequired();
        }
    }
}
