﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    class MessageObjectLinkConfiguration : IEntityTypeConfiguration<MessageObjectLink>
    {
        public void Configure(EntityTypeBuilder<MessageObjectLink> entity)
        {
            entity.ToTable("Message.Object.Link");

            entity.Property(e => e.BID).IsRequired();
            entity.Property(e => e.ID).HasColumnType("int").IsRequired();
            entity.Property(e => e.BodyID).HasColumnType("int").IsRequired();
            entity.Property(e => e.LinkedObjectClassTypeID).HasColumnType("int").IsRequired();
            entity.Property(e => e.LinkedObjectID).HasColumnType("int").IsRequired();
            entity.Property(e => e.IsSourceOfMessage).HasColumnName("SourceMessage").HasColumnType("bit").IsRequired().HasDefaultValue(false);
            entity.Property(e => e.DisplayText).HasColumnType("nvarchar(100)").IsRequired();

            //primary key
            entity.HasKey(e => new { e.BID, e.BodyID, e.LinkedObjectClassTypeID, e.LinkedObjectID }).HasName("PK_Message.Object.Link");

            // indexes
            entity.HasIndex(e => new { e.BID, e.LinkedObjectClassTypeID, e.LinkedObjectID }).HasName("IX_Message.Object.Link_LinkedObject");

            //foreign keys
            entity.HasOne<MessageBody>().WithMany(body => body.MessageObjectLinks)
                .HasForeignKey(e => new { e.BID, e.BodyID })
                .HasConstraintName("FK_Message.Related.Records_Message.Data");

        }
    }
}
