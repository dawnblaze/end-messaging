﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class DomainEmailLocationLinkConfiguration : IEntityTypeConfiguration<DomainEmailLocationLink>
    {
        public void Configure(EntityTypeBuilder<DomainEmailLocationLink> entity)
        {
            entity.ToTable("Domain.Email.LocationLink");

            entity.HasKey(t => new { t.BID, t.DomainID, t.LocationID });

            entity.Property(e => e.BID).HasColumnType("smallint");
            entity.Property(e => e.DomainID).HasColumnType("smallint");
            entity.Property(e => e.LocationID).HasColumnType("tinyint");

            /*entity.HasOne(t => t.Location).WithMany()
                .HasForeignKey(t => new { t.BID, t.LocationID })
                .HasConstraintName("FK_Domain.Email.LocationLink_Location.Data").OnDelete(DeleteBehavior.Restrict);*/

            entity.HasOne(t => t.DomainEmail).WithMany(x => x.LocationLinks)
                .HasForeignKey(t => new { t.BID, t.DomainID })
                .HasConstraintName("FK_Domain.Email.LocationLink_Domain.Email.Data").OnDelete(DeleteBehavior.Restrict);
        }
    }
}
