﻿using Endor.Comm.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Comm.Infrastructure.Data.Config
{
    public class EnumEmailProviderTypeConfiguration : IEntityTypeConfiguration<EnumEmailProviderType>
    {
        public void Configure(EntityTypeBuilder<EnumEmailProviderType> entity)
        {
            entity.ToTable("enum.Email.SMTP.ProviderType");
            entity.HasKey(t => new { t.ID });
            entity.Property(e => e.Name).HasColumnType("nvarchar(100)").IsRequired();
            entity.Property(e => e.ID).HasColumnType("tinyint");
        }
    }
}
