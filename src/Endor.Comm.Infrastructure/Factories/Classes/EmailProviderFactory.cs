﻿using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Factories.Interfaces;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.Infrastructure.Services.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Endor.Comm.Core.Factories.Classes
{
    public class EmailProviderFactory : IKeyedFactory<IEmailProvider, EmailProviderType>
    {
        public IEmailProvider Create(EmailProviderType key, params object[] additionalData)
        {
            return key switch
            {
                EmailProviderType.CustomSMTP => InstanceCreator<SMTPEmailProvider>(additionalData),
                EmailProviderType.GoogleGSuite => InstanceCreator<GmailEmailProvider>(additionalData),
                EmailProviderType.Microsoft365 => InstanceCreator<Office365EmailProvider>(additionalData),
                _ => throw new NotImplementedException($"The type provided {key.ToString()} is not implemented"),
            };
        }

        /// <summary>
        /// Simple instace creator for providers with/out arguments
        /// </summary>
        /// <typeparam name="T">Provider Type</typeparam>
        /// <param name="additionalData">Constructor parameters</param>
        /// <returns></returns>
        private T InstanceCreator<T>(params object[] additionalData) where T : class
        {
            var constructor = typeof(T).GetConstructors()?.FirstOrDefault();

            if (constructor == null)
                return Activator.CreateInstance<T>();

            if (additionalData == null)
                throw new ArgumentException("The constructor needs arguments, but no additionalData was specified.");

            return (T)Activator.CreateInstance(typeof(T), additionalData);
;        }
    }
}
