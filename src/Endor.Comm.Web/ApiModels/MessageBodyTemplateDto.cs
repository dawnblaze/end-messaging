﻿using AutoMapper;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Non_Owned_Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.ApiModels
{
    public class MessageBodyTemplateDto
    {
        public bool HasBody { get; set; }
        public bool HasMergeFields { get; set; }
        public bool HasAttachment { get; set; }
        public short SortIndex { get; set; }
        public int? SizeInKB { get; set; }
        public string AttachedFileNames { get; set; }
        public byte AttachedFileCount { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string Name { get; set; }
        public short? EmployeeID { get; set; }
        public int? CompanyID { get; set; }
        public byte? LocationID { get; set; }
        public MessageChannelType ChannelType { get; set; }
        public byte MessageTemplateType { get; set; }
        public int AppliesToClassTypeID { get; set; }
        public bool IsActive { get; set; }
        public DateTime ModifiedDT { get; set; }
        public int ClassTypeID { get; set; }
        public short ID { get; set; }
        public string Description { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SystemMessageTemplateType TemplateType { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<MessageParticipantInfoDto> Participants { get; set; }
    }

    public class MessageBodyTemplateProfile : Profile
    {
        public MessageBodyTemplateProfile()
        {
            CreateMap<MessageBodyTemplate, MessageBodyTemplateDto>();
            CreateMap<MessageBodyTemplateDto, MessageBodyTemplate>();
            CreateMap<MessageBodyTemplate, SimpleListItem>()
                .ForMember(dest => dest.ClassTypeID, opt => opt.MapFrom(src => src.AppliesToClassTypeID))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.Name));
        }
        
    }
}
