﻿using AutoMapper;
using Endor.Comm.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.ApiModels
{
    public class EmailAccountTeamLinkDto
    {
        /// <summary>
        /// Email Account ID
        /// </summary>
        public short EmailAccountID { get; set; }
        /// <summary>
        /// Team ID
        /// </summary>
        public int TeamID { get; set; }
    }

    public class EmailAccountTeamLinkProfile : Profile
    {
        public EmailAccountTeamLinkProfile()
        {
            CreateMap<EmailAccountTeamLink, EmailAccountTeamLinkDto>();
            CreateMap<EmailAccountTeamLinkDto, EmailAccountTeamLink>();
        }
    }
}
