﻿using AutoMapper;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.ApiModels
{
    public class SystemMessageTemplateTypeProfile : Profile
    {
        public SystemMessageTemplateTypeProfile()
        {
            CreateMap<SystemMessageTemplateType, SimpleListItem>()
                .ForMember(dest => dest.ClassTypeID, opt => opt.MapFrom(src => src.AppliesToClassTypeID))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.Name));
        }

    }
}
