﻿using AutoMapper;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.ApiModels
{
    public class DomainEmailDto
    {
        public int ClassTypeID { get; set; }
        public short ID { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsActive { get; set; }

        public string DomainName { get; set; }

        public EmailProviderType ProviderType { get; set; }

        public byte? SMTPConfigurationType { get; set; }

        public string SMTPAddress { get; set; }

        public short? SMTPPort { get; set; }

        public EmailSecurityType? SMTPSecurityType { get; set; }

        public bool? SMTPAuthenticateFirst { get; set; }

        public DateTime? LastVerificationAttemptDT { get; set; }

        public bool? LastVerificationSuccess { get; set; }

        public string LastVerificationResult { get; set; }

        public DateTime? LastVerifiedDT { get; set; }

        public bool IsForAllLocations { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<DomainEmailLocationLink> LocationLinks { get; set; }

        public SystemEmailSMTPConfigurationType ConfigurationType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<SimpleListItem> SimpleLocations { get; set; }
    }

    public class DomainEmailProfile : Profile
    {
        public DomainEmailProfile()
        {
            CreateMap<SimpleListItem, DomainEmailLocationLink>()
                .ForMember(dest => dest.LocationID, opt => opt.MapFrom(src => src.ID));
            CreateMap<DomainEmailLocationLink, SimpleListItem>()
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.LocationID));
            CreateMap<DomainEmail, DomainEmailDto>()
                .ForMember(dest => dest.SimpleLocations, opt => opt.MapFrom(src => src.LocationLinks));
            CreateMap<DomainEmailDto, DomainEmail>()
                .ForMember(dest => dest.LocationLinks, opt => opt.MapFrom(src => src.SimpleLocations))
                .AfterMap((src, dest) => dest.LocationLinks.ForEach(ll => ll.DomainID = src.ID));            
            CreateMap<DomainEmail, SimpleListItem>()
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.DomainName));
        }
    }
}
