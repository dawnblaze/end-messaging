﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Endor.Comm.Core.SharedKernel;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Services;
using Endor.Tenant;

namespace Endor.Comm.ApiModels
{
    public class MessageDto
    {
        public MessageDto()
        {

        }

        /// <summary>
        /// converts a message breakdown to Endor.Models.Message Object
        /// </summary>
        /// <param name="bodyString"> the actual html string we get from the blob storage </param>
        /// <param name="filterParticipants">  if the employee is the sender (From), then all participants are returned.  If the employee is not the sender, then BCC records are filtered out. </param>
        /// <param name="filterDeliveryRecords"> If the participant is not the Sender, the Message Delivery Records are filtered to those that apply to this employee only. </param>
        public MessageDto(MessageBreakdownService msgBreakdown, string bodyString = null, bool filterParticipants = true, bool filterDeliveryRecords = true)
        {
            
            //assign properties here
            //this.BID = msgBreakdown.MessageHeader.BID;
            this.ID = msgBreakdown.MessageHeader.ID;
            this.ClassTypeID = 14109;//(Read Only) An ID identifying the type of object.  Always 14109.
            //this.ModifiedDT = msgBreakdown.MessageHeader.ModifiedDT;
            this.ReceivedDT = msgBreakdown.MessageHeader.ReceivedDT;
            this.EmployeeID = msgBreakdown.MessageHeader.EmployeeID;
            this.ParticipantID = msgBreakdown.MessageHeader.ParticipantID;
            this.BodyID = msgBreakdown.MessageHeader.BodyID;
            this.IsRead = msgBreakdown.MessageHeader.IsRead;
            this.ReadDT = msgBreakdown.MessageHeader.ReadDT;
            this.IsDeleted = msgBreakdown.MessageHeader.IsDeleted;
            this.IsExpired = msgBreakdown.MessageHeader.IsExpired;
            this.DeletedOrExpiredDT = msgBreakdown.MessageHeader.DeletedOrExpiredDT;
            this.Channels = msgBreakdown.MessageHeader.Channels;
            this.IsSentFolder = msgBreakdown.MessageHeader.InSentFolder;

            this.Body = bodyString ?? msgBreakdown.BodyString;

            this.Subject = msgBreakdown.MessageBody.Subject;
            this.BodyFirstLine = msgBreakdown.MessageBody.BodyFirstLine;
            this.HasBody = msgBreakdown.MessageBody.HasBody;
            this.AttachedFileCount = msgBreakdown.MessageBody.AttachedFileCount;
            this.AttachedFileNames = msgBreakdown.MessageBody.AttachedFileNames;
            this.HasAttachment = msgBreakdown.MessageBody.HasAttachment;
            this.MetaData = msgBreakdown.MessageBody.MetaData;
            this.WasModified = msgBreakdown.MessageBody.WasModified;
            this.SizeInKB = msgBreakdown.MessageBody.SizeInKB;

            this.Participants = msgBreakdown.MessageBody.MessageParticipantInfos ?? new List<MessageParticipantInfo>();
            this.ObjectLinks = msgBreakdown.MessageBody.MessageObjectLinks ?? new List<MessageObjectLink>();
            this.DeliveryRecords = (msgBreakdown.MessageParticipantInfos ?? new List<MessageParticipantInfo>())
                                    .Where(mpi => mpi.MessageDeliveryRecords != null)
                                    .SelectMany(mpi => mpi.MessageDeliveryRecords)
                                    .ToList();


            var sender = this.Participants.FirstOrDefault(p => p.ParticipantRoleType == MessageParticipantRoleType.From);

            if (filterParticipants)
            {
                //When retrieving the record, if the employee is the sender (From), then all participants are returned.  
                //If the employee is not the sender, then BCC records are filtered out.
                if (sender?.EmployeeID != this.EmployeeID)//if not the sender
                {
                    this.Participants = (msgBreakdown.MessageParticipantInfos ?? new List<MessageParticipantInfo>())
                                        .Where(mpi => mpi.ParticipantRoleType != MessageParticipantRoleType.BCC)
                                        .ToList();
                }
            }

            if (filterDeliveryRecords)
            {

                // https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/846004318/Message+Object
                // If the employee is the Sender (FROM), then all Delivery Records for this message participant array are returned.
                // If the participant is not the Sender, the Message Delivery Records are filtered to those that apply to this employee only.   

                if (sender?.EmployeeID != this.EmployeeID)//if not the sender
                {
                    this.DeliveryRecords = (msgBreakdown.MessageParticipantInfos ?? new List<MessageParticipantInfo>())
                                        .Where(mpi => mpi.EmployeeID == this.EmployeeID && mpi.MessageDeliveryRecords != null)
                                        .SelectMany(mpi => mpi.MessageDeliveryRecords)
                                        .ToList();
                }
            }
        }

        public Tuple<bool,string> ValidateConversionToMessageBreakdown()
        {
            if (this.ObjectLinks != null)
            {
                var duplicateObjLinks = this.ObjectLinks.GroupBy(obj => new { obj.LinkedObjectClassTypeID, obj.LinkedObjectID }).ToList();
                if (duplicateObjLinks.Count < this.ObjectLinks.Count)
                {
                    return Tuple.Create(false, "ObjectLinks must have a unique set of (obj.LinkedObjectClassTypeID, obj.LinkedObjectID)");
                }
            }

            this.Participants = this.Participants ?? new List<MessageParticipantInfo>();
            var participantFROM = this.Participants.FirstOrDefault(p => p.ParticipantRoleType == MessageParticipantRoleType.From);
            var participantTO = this.Participants.FirstOrDefault(p => p.ParticipantRoleType == MessageParticipantRoleType.To);
            if (participantFROM == null)
            {
                return Tuple.Create(false, "Message requires a FROM participant");
            }

            if (participantTO == null && !this.Channels.HasFlag(MessageChannelType.Alert) && !this.Channels.HasFlag(MessageChannelType.SystemNotification))
            {
                return Tuple.Create(false, "At least one TO is required unless the Channel Type is Alerts (2) or System Notifications (128).");
            }
            return Tuple.Create(true, "");
        }

        public MessageBreakdownService ToMessageBreakdown()
        {
            var msgBreakdown = new MessageBreakdownService();
            //msgBreakdown.MessageHeader.BID = this.BID;
            msgBreakdown.MessageHeader.ID = this.ID;
            msgBreakdown.MessageHeader.ClassTypeID = 14111;
            //msgBreakdown.MessageHeader.ModifiedDT = this.ModifiedDT;
            msgBreakdown.MessageHeader.ReceivedDT = this.ReceivedDT;
            msgBreakdown.MessageHeader.EmployeeID = this.EmployeeID;
            msgBreakdown.MessageHeader.ParticipantID = this.ParticipantID;
            msgBreakdown.MessageHeader.BodyID = this.BodyID;
            msgBreakdown.MessageHeader.IsRead = this.IsRead;
            msgBreakdown.MessageHeader.ReadDT = this.ReadDT;
            msgBreakdown.MessageHeader.IsDeleted = this.IsDeleted;
            msgBreakdown.MessageHeader.IsExpired = this.IsExpired;
            msgBreakdown.MessageHeader.DeletedOrExpiredDT = this.DeletedOrExpiredDT;
            msgBreakdown.MessageHeader.Channels = this.Channels;
            msgBreakdown.MessageHeader.InSentFolder = this.IsSentFolder;

            msgBreakdown.MessageBody.Subject = this.Subject;
            msgBreakdown.MessageBody.BodyFirstLine = this.BodyFirstLine;
            msgBreakdown.MessageBody.HasBody = this.HasBody;
            msgBreakdown.MessageBody.AttachedFileCount = this.AttachedFileCount;
            msgBreakdown.MessageBody.AttachedFileNames = this.AttachedFileNames;
            msgBreakdown.MessageBody.HasAttachment = this.HasAttachment;
            msgBreakdown.MessageBody.MetaData = this.MetaData;
            msgBreakdown.MessageBody.WasModified = this.WasModified;
            msgBreakdown.MessageBody.SizeInKB = this.SizeInKB;

            msgBreakdown.BodyString = this.Body;

            msgBreakdown.MessageParticipantInfos = this.Participants;
            msgBreakdown.MessageObjectLinks = this.ObjectLinks;
            msgBreakdown.MessageDeliveryRecords = this.DeliveryRecords;
            msgBreakdown.MessageBody.MessageParticipantInfos = msgBreakdown.MessageParticipantInfos;
            msgBreakdown.MessageBody.MessageObjectLinks = msgBreakdown.MessageObjectLinks;
            //create the body
            //--strip html tags and get 50 substring
            msgBreakdown.MessageBody.BodyFirstLine = Regex.Replace(this.Body ?? "", "<.*?>", String.Empty);
            if (msgBreakdown.MessageBody.BodyFirstLine.Length > 50)
            {
                msgBreakdown.MessageBody.BodyFirstLine = msgBreakdown.MessageBody.BodyFirstLine.Substring(0, 50);
            }

            return msgBreakdown;
        }

        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ReceivedDT { get; set; }
        public short EmployeeID { get; set; }
        public int ParticipantID { get; set; }
        public int BodyID { get; set; }
        public bool IsRead { get; set; }
        public DateTime? ReadDT { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsExpired { get; set; }
        public DateTime? DeletedOrExpiredDT { get; set; }
        public MessageChannelType Channels { get; set; }
        public bool IsSentFolder { get; set; }
        public string Subject { get; set; }
        public string BodyFirstLine { get; set; }
        public bool HasBody { get; set; }
        public byte AttachedFileCount { get; set; }
        public string AttachedFileNames { get; set; }
        public bool HasAttachment { get; set; }
        public string MetaData { get; set; }
        public bool WasModified { get; set; }
        public int? SizeInKB { get; set; }
        public string Body { get; set; }
        public ICollection<MessageParticipantInfo> Participants { get; set; }
        public ICollection<MessageObjectLink> ObjectLinks { get; set; }
        public ICollection<MessageDeliveryRecord> DeliveryRecords { get; set; }
    }

    
}
