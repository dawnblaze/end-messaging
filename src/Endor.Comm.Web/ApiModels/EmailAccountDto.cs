﻿using AutoMapper;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.ApiModels
{
    public class EmailAccountDto
    {
        public int ID { get; set; }
        public bool IsActive { get; set; }
        public EmailAccountStatus StatusType { get; set; }
        public short DomainEmailID { get; set; }
        public string UserName { get; set; }
        public string DomainName { get; set; }
        public bool IsPrivate { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? EmployeeID { get; set; }
        public string Credentials { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? CredentialsExpDT { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastEmailSuccessDT { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastEmailFailureDT { get; set; }
        public string EmailAddress { get; set; }
        public string AliasUserNames { get; set; }
        public string DisplayName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<EmailAccountTeamLinkDto> EmailAccountTeamLinks { get; set; }

        //Extra fields for credentials
        public bool HasPassword { get; set; }
        public string NewPassword { get; set; }

        public void EncryptNewRequestedPassword(string passphrase)
        {
            NewPassword = StringEncryptor.Encrypt(NewPassword, passphrase);
        }
    }

    public class EmailAccountProfile : Profile
    {
        public EmailAccountProfile()
        {
            CreateMap<EmailAccountData, EmailAccountDto>()
                .ForMember(dest => dest.HasPassword, opt => opt.MapFrom(src => !String.IsNullOrWhiteSpace(src.Credentials)))
                .ForMember(dest => dest.Credentials, opt => opt.Ignore());

            // The credentials should be a JSON
            CreateMap<EmailAccountDto, EmailAccountData>()
                .ForMember(dest => dest.Credentials, opt => opt.MapFrom(src => MapCredentialsNewPassword(src.NewPassword)));

            CreateMap<EmailAccountData, SimpleEmailAccountData>();
        }

        public string MapCredentialsNewPassword(string newPassword)
        {
            try
            {
                var isJson = JToken.Parse(newPassword);
                return newPassword;
            }
            catch
            {
                return "{ \"password\": \"" + newPassword + "\" }";
            }
        }

    }
    public class SimpleEmailAccountData : SimpleListItem
    {
        public short? EmployeeID { get; set; }
    }
}
