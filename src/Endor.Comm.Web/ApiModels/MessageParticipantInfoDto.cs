﻿using AutoMapper;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.ApiModels
{
    public class MessageParticipantInfoDto
    {
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public short ID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public int ClassTypeID { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int BodyID { get; set; }
        public MessageParticipantRoleType ParticipantRoleType { get; set; }
        public MessageChannelType Channel { get; set; }
        public string UserName { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsMergeField { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? EmployeeID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ContactID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TeamID { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime? DeliveredDT { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? IsDelivered { get; set; }
    }

    public class MessageParticipantInfoProfile : Profile
    {
        public MessageParticipantInfoProfile()
        {
            CreateMap<MessageParticipantInfo, MessageParticipantInfoDto>();
            CreateMap<MessageParticipantInfoDto, MessageParticipantInfo>();
        }
    }
}
