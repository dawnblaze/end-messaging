﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.QueryParameters
{
    public class SystemMessageTemplateTypeFilters
    {
        public int? AppliesToClasstypeID { get; set; }
        public byte? ChannelType { get; set; }
    }
}
