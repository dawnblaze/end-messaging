﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.QueryParameters
{
    public class EmailAccountFilter
    {
        /// <summary>
        /// If only Active records should be returned.
        /// When not provided, returns both Active and InActive.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// get records that match this email
        /// </summary>
        /// <value></value>
        public string EmailAddress { get; set; }
    }
}
