﻿using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.QueryParameters
{
    public class MessageFilters
    {
        // <summary>
        /// short - required
        /// </summary>
        public short EmployeeID { get; set; } = 0;
        /// <summary>
        /// true(1), false(0), only(2)
        /// </summary>
        public TrueFalseOnly IncludeRead { get; set; } = TrueFalseOnly.False;
        /// <summary>
        /// true(1), false(0), only(2)
        /// </summary>
        public TrueFalseOnly IncludeSent { get; set; } = TrueFalseOnly.False;
        /// <summary>
        /// true(1), false(0), only(2)
        /// </summary>
        public TrueFalseOnly IncludeDeleted { get; set; } = TrueFalseOnly.False;

    }

    
}
