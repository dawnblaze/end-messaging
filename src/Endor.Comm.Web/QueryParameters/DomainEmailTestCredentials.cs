﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.QueryParameters
{
    public class DomainEmailTestCredentials
    {
        /// <summary>
        /// email address for test
        /// </summary>
        public string emailaddress { get; set; }
        /// <summary>
        /// password for test
        /// </summary>
        public string password { get; set; }
    }
}
