﻿using Endor.Comm.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Web.QueryParameters
{
    public class EmailAccountIncludes
    {
        public IncludesLevel IncludeTeams { get; set; } = IncludesLevel.None;

    }
}
