﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.QueryParameters
{
    public class SharedEmailAccountsFilter
    {
        /// <summary>
        ///  If true, also includes emails accounts that are shared for the applicable location.
        ///  The application location is the first one these locations that is applicable:
        ///  Order's Location
        ///  Estimate's Location
        ///  Company's Location
        ///  Employee's Location
        /// </summary>
        public bool IncludeShared { get; set; } = true;

        public int? orderid { get; set; }

        public int? estimateid { get; set; }

        public int? companyid { get; set; }
    }
}
