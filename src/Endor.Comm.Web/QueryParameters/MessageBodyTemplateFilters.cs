﻿using Endor.Comm.Core.SharedKernal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.QueryParameters
{
    public class MessageBodyTemplateFilters
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }
        public int? AppliesToClassTypeID { get; set; }
        public int? TemplateType { get; set; }

    }
}
