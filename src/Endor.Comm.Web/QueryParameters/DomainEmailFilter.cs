﻿using Endor.Comm.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.QueryParameters
{
    public class DomainEmailFilter : ActiveFilter
    {
        /// <summary>
        /// If supplied, filters the list to domain emails valid at that location.
        /// </summary>
        public byte? LocationID { get; set; }
    }
}
