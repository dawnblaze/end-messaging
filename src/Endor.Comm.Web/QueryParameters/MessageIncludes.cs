﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.QueryParameters
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/846004309/Message+API+Endpoints
    /// </summary>
    public class MessageIncludes
    {
        /// <summary>
        /// default false
        /// </summary>
        public bool IncludeLinks { get; set; } = false;
        /// <summary>
        /// default false
        /// </summary>
        public bool IncludeDeliveryHistory { get; set; } = false;

    }
}
