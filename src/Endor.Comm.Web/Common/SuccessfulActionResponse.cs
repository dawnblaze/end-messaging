﻿using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Web.Common
{
    public class SuccessfulActionResponse : IActionResponse
    {
        /// <summary>
        /// Response message
        /// </summary>
        public string message { get; set; }
        
        /// <summary>
        /// ID of object
        /// </summary>
        public int? ID { get; set; }
    }
}
