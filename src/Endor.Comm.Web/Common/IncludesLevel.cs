﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Common
{
    /// <summary>
    /// Include Level Enum
    /// </summary>
    public enum IncludesLevel : byte
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,
        /// <summary>
        /// Simple
        /// </summary>
        Simple = 1,
        /// <summary>
        /// Full
        /// </summary>
        Full = 2
    }
}
