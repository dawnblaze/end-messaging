﻿using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Common
{
    public class BooleanResponse : IActionResponse
    {
        /// <summary>
        /// Response Value
        /// </summary>
        public bool? Value { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string message { get; set; }
        
        /// <summary>
        /// ID of object
        /// </summary>
        public int? ID { get; set; }

    }

    public class InternalBooleanResponse : BooleanResponse
    {
        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// If there is an error
        /// </summary>
        public bool HasError { get { return !this.Success; } }
    }
}
