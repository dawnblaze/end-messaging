﻿using Endor.Comm.Core.Exceptions;
using Endor.Comm.Core.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Security;
using Endor.Comm.Core.Specifications;
using Endor.Tenant;
using Endor.Comm.Infrastructure.Common;
using Endor.Comm.Core.Non_Owned_Entities;

namespace Endor.Comm.Web.Common
{
    public class CustomActionFilter : ActionFilterAttribute
    {
        protected readonly IWebHostEnvironment _hostingEnvironment;
        protected readonly ITenantDataCache _tenantCache;

        public CustomActionFilter(IWebHostEnvironment hostingEnvironment, ITenantDataCache tenantDataCache)
        {
            _hostingEnvironment = hostingEnvironment;
            _tenantCache = tenantDataCache;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Canceled == true)
            {
                // Action execution was short-circuited by another filter.
            }
            if (context.Exception != null)
            {
                if (context.Exception is EntityNotFoundException)
                    context.Result = new NotFoundObjectResult(context.Exception.Message);
                else
                    context.Result = new BadRequestObjectResult(new DevActionErrorResponse()
                    {
                        message = context.Exception.Message,
                        innerError = context.Exception
                    });
                context.Exception = null; //Exception is now handled
            }
            int? statusCode = null;
            ObjectResult result = context.Result as ObjectResult;
            statusCode = result?.StatusCode;
            object obj = result != null ? result.Value : null;
            if (statusCode == null)
            {
                StatusCodeResult statusResult = context.Result as StatusCodeResult;
                statusCode = statusResult?.StatusCode;
            }
            if (statusCode != null) //some successful responses won't have a status code
            {
                var failureStatusCodes = new List<int>()
                {
                    (int)HttpStatusCode.Unauthorized,
                    (int)HttpStatusCode.Forbidden,
                    (int)HttpStatusCode.BadRequest,
                    (int)HttpStatusCode.InternalServerError
                };
                if (statusCode == (int)HttpStatusCode.NotFound)
                {
                    context.Result = new NotFoundObjectResult(new ActionErrorResponse()
                    {
                        code = "Not Found",
                        message = obj.ToString()
                    });
                }
                else if (failureStatusCodes.Contains(statusCode.Value))
                {
                    var returnObj = new ActionErrorResponse();
                    if (obj != null && obj is IErrorResponse)
                    {
                        returnObj = (ActionErrorResponse)obj;
                    }
                    else if (obj != null)
                    {
                        returnObj = new DevActionErrorResponse()
                        {
                            message = obj.ToString()
                        };
                    }
                    if (_hostingEnvironment.IsDevelopment())
                    {
                        var devReturnObj = (DevActionErrorResponse)returnObj;
                        devReturnObj.userID = context.HttpContext.User.UserID();
                        var bid = context.HttpContext.User.BID();
                        if (bid.HasValue && devReturnObj.userID.HasValue)
                        {
                            var ConnectionString = _tenantCache.Get(bid.Value).Result.BusinessDBConnectionString;
                            var _dapperRepository = new DapperRepository(bid.Value, "", ConnectionString);
                            _dapperRepository.SetTableNameForClassType(UserLink.ClassTypeIDValue);
                            devReturnObj.userAccessType = _dapperRepository.List(
                                        new UserLinkByUserIDSpecification(context.HttpContext.User.BID().Value, devReturnObj.userID.Value))
                                            .FirstOrDefault().UserAccessType;
                        }
                        result.Value = devReturnObj;
                    }
                    else result.Value = returnObj;
                    switch (statusCode)
                    {
                        case (int)HttpStatusCode.Unauthorized:
                            returnObj.code = "User is not authorized";
                            break;
                        case (int)HttpStatusCode.BadRequest:
                            returnObj.code = "Invalid Request";
                            break;
                        case (int)HttpStatusCode.Forbidden:
                            returnObj.code = "User does not access";
                            break;
                        case (int)HttpStatusCode.InternalServerError:
                            returnObj.code = "Internal Error";
                            break;
                    };
                }
            }
            base.OnActionExecuted(context);
        }
    }
}
