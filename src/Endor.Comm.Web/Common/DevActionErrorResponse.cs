﻿using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Web.Common
{
    public class DevActionErrorResponse : ActionErrorResponse, IDevResponse
    {
        /// <summary>
        /// User ID 
        /// </summary>
        public int? userID { get; set; }
        /// <summary>
        /// UserAccessType
        /// </summary>
        public int? userAccessType { get; set; }
        /// <summary>
        /// Full Exception
        /// </summary>
        public object innerError { get; set; }
    }
}
