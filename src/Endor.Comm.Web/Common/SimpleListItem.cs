﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Endor.Comm.Common
{
    public class SimpleListItem
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public string DisplayName { get; set; }
    }
}
