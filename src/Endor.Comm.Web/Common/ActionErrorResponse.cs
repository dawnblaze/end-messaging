﻿using Endor.Comm.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Web.Common
{
    public class ActionErrorResponse : IErrorResponse
    {
        /// <summary>
        /// Response message
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// ID of object
        /// </summary>
        public int? ID { get; set; }
        /// <summary>
        /// Response cod
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// Response details - if any
        /// </summary>
        public IErrorResponse[] details { get; set; }
    }
}
