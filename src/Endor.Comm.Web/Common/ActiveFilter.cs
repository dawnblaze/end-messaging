﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Comm.Common
{
    public class ActiveFilter
    {
        /// <summary>
        /// If only Active records should be returned.
        /// When not provided, returns both Active and InActive.
        /// </summary>
        public bool? IsActive { get; set; }

    }
}
