﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Comm.Common.Documents;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Infrastructure.Common;
using Endor.Security;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Endor.Comm.Common
{
    [Authorize]
    public abstract class BaseController : Controller
    {
        protected readonly IRepository _repository;
        protected readonly ITenantDataCache _tenantCache;
        protected readonly IHttpContextAccessor _httpCtx;
        protected readonly short? BID;
        protected readonly short? EmployeeID;
        protected readonly string ConnectionString;
        protected readonly IDapperReadOnlyRepository _dapperRepository;

        public BaseController(IRepository repository, IHttpContextAccessor httpCtx, ITenantDataCache tenantCache)
        {
            _repository = repository;
            _tenantCache = tenantCache;
            _httpCtx = httpCtx;
            BID = _httpCtx?.HttpContext?.User?.BID();
            EmployeeID = _httpCtx?.HttpContext?.User?.EmployeeID();
            ConnectionString = _tenantCache.Get(BID.Value).Result.BusinessDBConnectionString;
            _dapperRepository = new DapperRepository(BID.Value, "", ConnectionString);
        }

        protected IDapperReadOnlyRepository GetDapperRepository(int ClassTypeID)
        {
            _dapperRepository.SetTableNameForClassType(ClassTypeID);
            return _dapperRepository;
        }

        public static DocumentManager GetDocumentManager(short bid, int id, int classTypeID, BucketRequest bucketRequest, ITenantDataCache tenantDataCache)
        {
            var dmid = new Endor.DocumentStorage.Models.DMID()
            {
                ctid = classTypeID,
                id = int.Parse($"{id}")
            };
            var storage = new StorageContext(bid, bucketRequest, dmid);
            return new DocumentManager(tenantDataCache, storage);
        }
    }
}
