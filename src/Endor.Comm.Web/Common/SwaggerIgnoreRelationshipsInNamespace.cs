﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Comm.Core;
using System.Collections;
using System.Reflection;

namespace Endor.Comm.Web.Common
{
    public class SwaggerIgnoreRelationshipsInNamespace : ISchemaFilter
    {
        string nameSpaceToIgnore;
        public SwaggerIgnoreRelationshipsInNamespace(string ns)
        {
            nameSpaceToIgnore = ns;
        }

        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (model.Properties == null || model.Properties.Count == 0)
                return;
            var excludeList = new List<string>();

            if (context.Type.Namespace == nameSpaceToIgnore)
            {
                var props = context.Type.GetProperties();
                var propsToExclude = props.Where(p => p.GetCustomAttribute<SwaggerIncludeAttribute>() == null
                    && !p.Name.Contains("Locator")).Where(
                        p => p.PropertyType.Namespace == nameSpaceToIgnore || p.Name == "BID"
                        || (p.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(p.PropertyType))).ToList();
                excludeList.AddRange(propsToExclude.Select(p => p.Name));
            }

            foreach (var prop in excludeList)
            {
                var propToRemove = model.Properties.FirstOrDefault(x => x.Key.ToLower().Equals(prop.ToLower())).Key;
                if (propToRemove != null)
                    model.Properties.Remove(propToRemove);
            }
        }
    }
}
