﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.ApiModels;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using Microsoft.AspNetCore.Mvc;
using Endor.Comm.Common;
using Endor.Comm.Core.Specifications;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Endor.Comm.QueryParameters;
using Endor.Comm.Core.Services;
using Endor.Security;
using Endor.Comm.Core.Non_Owned_Entities;
using AutoMapper;
using Endor.Comm.Web.Common;

namespace Endor.Comm.Web.Controllers
{
    [Route("api/message/bodytemplate")]
    public class MessageBodyTemplateController : BaseController
    {
        private readonly IMapper _mapper;

        public MessageBodyTemplateController(IRepository repository, IHttpContextAccessor httpCtx, ITenantDataCache tenantCache, IMapper mapper)
            : base(repository, httpCtx, tenantCache)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// Returns list of MessageBodyTemplate. Can be filtered by AppliesToClassTypeID and TemplateType
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplateDto[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<MessageBodyTemplateDto>> GetMultiple([FromQuery] MessageBodyTemplateFilters filters)
        {
            var messageBodyTemplates = (await _repository.ListEntityAsync<MessageBodyTemplate>(new MessageBodyTemplateFilterSpecification(
                filters.IsActive,
                filters.AppliesToClassTypeID,
                filters.TemplateType)));
            return _mapper.Map<List<MessageBodyTemplate>, List<MessageBodyTemplateDto>>(messageBodyTemplates);
        }

        /// <summary>
        /// Returns a simple list of MessageBodyTemplate. Can be filtered by AppliesToClassTypeID and TemplateType
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ICollection<MessageBodyTemplateDto>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleListItem>> FilteredSimpleList([FromQuery] MessageBodyTemplateFilters filters)
        {
            var messageBodyTemplates = (await _repository.ListEntityAsync<MessageBodyTemplate>(new MessageBodyTemplateFilterSpecification(
                filters.IsActive,
                filters.AppliesToClassTypeID,
                filters.TemplateType)));
            return _mapper.Map<List<MessageBodyTemplate>, List<SimpleListItem>>(messageBodyTemplates);
        }
        /// <summary>
        /// Returns the specified MessageBodyTemplate
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplateDto))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<MessageBodyTemplateDto> GetSingleMessageBodyTemplate(short ID)
        {
            var messageBodyTemplate = await _repository.GetByShortIdAsync<MessageBodyTemplate>(ID);
            return _mapper.Map<MessageBodyTemplateDto>(messageBodyTemplate);
        }

        /// <summary>
        /// Create new MessageBodyTemplate record
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempID"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplateDto))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> Create([FromBody] MessageBodyTemplateDto newModel)
        {
            var messageBodyTemplate = _mapper.Map<MessageBodyTemplate>(newModel);
            await messageBodyTemplate.ValidateParticipants(_dapperRepository);
            await _repository.AddAsync<MessageBodyTemplate>(messageBodyTemplate);
            return Ok(_mapper.Map<MessageBodyTemplateDto>(messageBodyTemplate));
        }

        /// <summary>
        /// Updates the existing MessageBodyTemplate
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplateDto))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MessageBodyTemplate does not exist or the update fails.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> Update(short ID, [FromBody] MessageBodyTemplateDto update)
        {
            var messageBodyTemplate = await _repository.GetByShortIdAsync<MessageBodyTemplate>(ID);
            messageBodyTemplate = _mapper.Map(update, messageBodyTemplate);
            messageBodyTemplate.BID = BID.Value;
            await messageBodyTemplate.ValidateParticipants(_dapperRepository);
            await _repository.UpdateAsync(messageBodyTemplate);
            return Ok(_mapper.Map<MessageBodyTemplateDto>(messageBodyTemplate));
        }

        /// <summary>
        /// Deletes MessageBodyTemplate record
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OkResult))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> Delete(short ID)
        {
            var messageBodyTemplate = await _repository.GetByShortIdAsync<MessageBodyTemplate>(ID);
            await _repository.DeleteAsync(messageBodyTemplate);
            return Ok();
        }

        
        /// <summary>
        /// Sets MessageBodyTemplate to active
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Route("{ID}/action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            var messageBodyTemplate = await _repository.GetByShortIdAsync<MessageBodyTemplate>(ID);
            messageBodyTemplate.MarkAsActive();
            await _repository.UpdateAsync(messageBodyTemplate);
            return Ok(new SuccessfulActionResponse()
            {
                ID = ID,
                message = "Message Body Template set as active"
            });
        }

        /// <summary>
        /// Sets MessageBodyTemplate to inactive
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the MessageBodyTemplate does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            var messageBodyTemplate = await _repository.GetByShortIdAsync<MessageBodyTemplate>(ID);
            messageBodyTemplate.MarkAsInActive();
            await _repository.UpdateAsync(messageBodyTemplate);
            return Ok(new SuccessfulActionResponse()
            {
                ID = ID,
                message = "Message Body Template set as inactive"
            });
        }

        /// <summary>
        /// Checks if MessageBodyTemplate can be deleted
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            var messageBodyTemplate = await _repository.GetByShortIdAsync<MessageBodyTemplate>(ID);
            var result = await messageBodyTemplate.CanDeleteAsync(_repository);
            return Ok(new BooleanResponse()
                {
                    message = result.Item2,
                    Value = result.Item1,
                    ID = ID
                });

        }

        /// <summary>
        /// Move this record before the TargetID
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TargetID"></param>
        /// <returns></returns>
        [Route("{ID}/Action/MoveBefore/{TargetID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> MoveBefore(short ID, short TargetID)
        {
            var messageBodyTemplate = await _repository.GetByShortIdAsync<MessageBodyTemplate>(ID);
            var tuple = messageBodyTemplate.MoveTo((short)(TargetID));
            await _repository.UpdateAsync(messageBodyTemplate);
            var messageBodyTemplatesToAdjust = await _repository.ListEntityAsync(new RetrieveMessageBodyTemplatesBetweenSortIndexes(BID.Value, tuple.Item1, tuple.Item2));
            foreach (var template in messageBodyTemplatesToAdjust)
            {
                template.MoveSortIndexBy(tuple.Item3);
                await _repository.UpdateAsync(template);
            }
            return Ok(new SuccessfulActionResponse()
            {
                ID = ID,
                message = "Message Body Template successfully moved"
            });
        }

        /// <summary>
        /// Move this record after the TargetID
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TargetID"></param>
        /// <returns></returns>
        [Route("{ID}/Action/MoveAfter/{TargetID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> MoveAfter(short ID, short TargetID)
        {
            var messageBodyTemplate = await _repository.GetByShortIdAsync<MessageBodyTemplate>(ID);
            var tuple = messageBodyTemplate.MoveTo((short)(TargetID+1));
            await _repository.UpdateAsync(messageBodyTemplate);
            var messageBodyTemplatesToAdjust = await _repository.ListEntityAsync(new RetrieveMessageBodyTemplatesBetweenSortIndexes(BID.Value, tuple.Item1, tuple.Item2));
            foreach (var template in messageBodyTemplatesToAdjust)
            {
                template.MoveSortIndexBy(tuple.Item3);
                await _repository.UpdateAsync(template);
            }
            return Ok(new SuccessfulActionResponse()
            {
                ID = ID,
                message = "Message Body Template successfully moved"
            });
        }

        /// <summary>
        /// Clones the given record
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TargetID"></param>
        /// <returns></returns>
        [Route("{ID}/clone")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplateDto))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> Clone(short ID)
        {
            var messageBodyTemplate = await _repository.CloneAsync<MessageBodyTemplate>(ID, mt => mt.Name,typeof(short));
            return Ok(_mapper.Map<MessageBodyTemplateDto>(messageBodyTemplate));
        }
    }
}
