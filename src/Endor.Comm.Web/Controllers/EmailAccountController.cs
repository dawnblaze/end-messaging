﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Endor.Comm.ApiModels;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Factories.Interfaces;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.Services.Classes;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.Specifications;
using Endor.Comm.QueryParameters;
using Endor.Comm.Web.Common;
using Endor.Comm.Web.QueryParameters;
using Endor.Security;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Endor.Comm.Web.Controllers
{
    /// <summary>
    /// EmailAccountData Controller
    /// </summary>\
    [Route("api/emailaccount")]
    public class EmailAccountController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly Core.Configurations.EndorOptions _options;
        private readonly IKeyedFactory<IEmailProvider, EmailProviderType> _providerFactory;

        public EmailAccountController(IRepository repository, IHttpContextAccessor httpCtx, ITenantDataCache tenantCache, IMapper mapper, Core.Configurations.EndorOptions options, IKeyedFactory<IEmailProvider, EmailProviderType> providerFactory) : base(repository, httpCtx, tenantCache)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _providerFactory = providerFactory ?? throw new ArgumentNullException(nameof(providerFactory));
        }

        /// <summary>
        /// Get a list of EmailAccountData by location
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="locationID"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet("location/{locationID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountDto[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If a location with the given LocationID is not found.")]
        public async Task<IActionResult> ReadWithFiltersForLocation(byte locationID, [FromQuery] ActiveFilter filters)
        {
            var emailAccounts = await GetEmailAccountsForLocation(filters.IsActive, locationID);
            return Ok(_mapper.Map<List<EmailAccountData>, List<EmailAccountDto>>(emailAccounts));
        }

        private async Task<List<EmailAccountData>> GetEmailAccountsForLocation(bool? IsActive, byte locationID)
        {
            // verify LocationID matches existing LocationData
            var locationRepository = GetDapperRepository(LocationData.ClassTypeIDValue);
            await locationRepository.GetByIdAsync<LocationData>(locationID);

            var domainIDs = (await _repository.ListEntityAsync<DomainEmailLocationLink>(new DomainEmailLocationLinkFilterByLocationSpecification(locationID))).Select(x => x.DomainID).ToList();
            var emailAccounts = (await _repository.ListEntityAsync<EmailAccountData>(new EmailAccountActiveFilterSpecification(
                IsActive)));
            emailAccounts = emailAccounts.Where(e => domainIDs.Contains(e.DomainEmailID) && e.IsActive == IsActive).ToList();
            return emailAccounts;
        }

        /// <summary>
        ///  Returns JSON Collection of EmailAccount Objects for the specified employee.
        /// </summary>
        /// <param name="employeeID">The ID of the employee</param>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="IncludeShared">
        ///  IncludeShared - If true, also includes emails accounts that are shared for the applicable location.
        ///  The application location is the first one these locations that is applicable:
        ///  Order's Location
        ///  Estimate's Location
        ///  Company's Location
        ///  Employee's Location</param>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet("employee/{employeeID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountDto[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If an employee with the given EmployeeID is not found.")]
        public async Task<IActionResult> ReadWithFiltersForEmployee(short employeeID, [FromQuery] ActiveFilter activeFilters, [FromQuery] SharedEmailAccountsFilter sharedEmailAccountsFilters)
        {
            var emailAccounts = await GetEmailAccountsForEmployeeID(employeeID, activeFilters, sharedEmailAccountsFilters);
            return Ok(_mapper.Map<List<EmailAccountData>, List<EmailAccountDto>>(emailAccounts));
        }

        private async Task<List<EmailAccountData>> GetEmailAccountsForEmployeeID(short employeeID, ActiveFilter activeFilters, SharedEmailAccountsFilter sharedEmailAccountsFilters)
        {
            // verify EmployeeID matches existing EmployeeData
            var employeeRespository = GetDapperRepository(EmployeeData.ClassTypeIDValue);
            await employeeRespository.GetByIdAsync<EmployeeData>(employeeID);

            var emailAccounts = (await _repository.ListEntityAsync<EmailAccountData>(new EmailAccountsByEmployeeSpecification(
                activeFilters.IsActive,
                employeeID)));
            var employeeTeamRespository = GetDapperRepository(EmployeeTeam.ClassTypeIDValue);
            var teamIDs = (await employeeTeamRespository.ListEntityAsync<EmployeeTeamLink>(new EmployeeTeamLinksByEmployeeSpecification(employeeID))).Select(x => x.TeamID).ToList();
            var teamEmailAccounts = (await _repository.ListEntityAsync<EmailAccountTeamLink>(new EmailAccountTeamLinkIncludeEmailAccountSpecification()))
                .Where(x => teamIDs.Contains(x.TeamID))
                .Select(x => x.EmailAccount).ToList();
            emailAccounts.AddRange(teamEmailAccounts);
            if (sharedEmailAccountsFilters.IncludeShared)
            {
                byte locationID = await GetDefaultLocation(employeeID, sharedEmailAccountsFilters.orderid, sharedEmailAccountsFilters.estimateid, sharedEmailAccountsFilters.companyid);
                emailAccounts.AddRange(await GetEmailAccountsForLocation(activeFilters.IsActive, locationID));
                // distinct
                emailAccounts = emailAccounts.GroupBy(e => e.ID).Select(e => e.First()).ToList();
            }
            // NOTE: credentials are nulled
            emailAccounts.ForEach(e => e.Credentials = null);
            return emailAccounts;
        }

        private async Task<byte> GetDefaultLocation(short employeeID, int? orderId, int? estimateID, int? companyID)
        {
            byte locationID = 0;
            if (orderId.HasValue)
            {
                var transactionHeaderRespository = GetDapperRepository(EmployeeTeam.ClassTypeIDValue);
                locationID = (await transactionHeaderRespository.ListEntityAsync<TransactionHeader>(
                    new TransactionHeaderByIDAndTypeSpecification(
                        TransactionHeader.OrderTransactionType, orderId.Value)))
                    .Select(o => o.LocationID).First();
            }
            else if (estimateID.HasValue)
            {
                var transactionHeaderRespository = GetDapperRepository(EmployeeTeam.ClassTypeIDValue);
                locationID = (await transactionHeaderRespository.ListEntityAsync<TransactionHeader>(
                    new TransactionHeaderByIDAndTypeSpecification(
                        TransactionHeader.EstimateTransactionType, estimateID.Value)))
                    .Select(o => o.LocationID).First();
            }
            else if (companyID.HasValue)
            {
                var companyRespository = GetDapperRepository(CompanyData.ClassTypeIDValue);
                locationID = (await companyRespository.ListEntityAsync<CompanyData>(
                    new FilterByIDSpecification<CompanyData>(companyID.Value)))
                    .Select(o => o.LocationID).First();
            }
            else
            {
                var employeeRespository = GetDapperRepository(EmployeeData.ClassTypeIDValue);
                locationID = (await employeeRespository.ListEntityAsync<EmployeeData>(
                    new FilterByIDSpecification<EmployeeData>((int)employeeID)))
                    .Select(o => o.LocationID).First();
            }
            return locationID;
        }

        /// <summary>
        /// Returns simple list of domain email
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <param name="employeeID">The ID of the employee</param>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="IncludeShared"></param>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns></returns>
        [HttpGet("employee/{employeeID}/simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<IActionResult> SimpleListByEmployee(short employeeID, [FromQuery] ActiveFilter activeFilters, [FromQuery] SharedEmailAccountsFilter sharedEmailAccountsFilters)
        {
            var emailAccounts = await GetEmailAccountsForEmployeeID(employeeID, activeFilters, sharedEmailAccountsFilters);
            return Ok(_mapper.Map<List<EmailAccountData>, List<SimpleEmailAccountData>>(emailAccounts));
        }

        /// <summary>
        /// Get the default email account for an employee
        /// </summary>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet("default")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountDto))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadDefaultWithFiltersForEmployee([FromQuery] int? orderid = null, [FromQuery] int? estimateid = null, [FromQuery] int? companyid = null)
        {
            return await GetDefaultEmail(EmployeeID, orderid, estimateid, companyid);
        }

        /// <summary>
        /// Get the default email account for an employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet("employee/{employeeID}/default")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountDto[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadDefaultWithFiltersForEmployee(short employeeID, [FromQuery] int? orderid = null, [FromQuery] int? estimateid = null, [FromQuery] int? companyid = null)
        {
            return await GetDefaultEmail(employeeID, orderid, estimateid, companyid);
        }


        private async Task<IActionResult> GetDefaultEmail(short? employeeID, int? orderid = null, int? estimateid = null, int? companyid = null)
        {
            if (!employeeID.HasValue)
                return new UnauthorizedResult();

            var employeeRespository = GetDapperRepository(EmployeeData.ClassTypeIDValue);
            var employee = await employeeRespository.GetByIdAsync<EmployeeData>(employeeID.Value);
            if (employee.DefaultEmailAccountID.HasValue)
            {
                var emailAccount = await _repository.GetByShortIdAsync<EmailAccountData>(employee.DefaultEmailAccountID.Value);
                return Ok(_mapper.Map<EmailAccountData, EmailAccountDto>(emailAccount));
            }
            byte locationID = await GetDefaultLocation(employeeID.Value, orderid, estimateid, companyid);
            var locationRepository = GetDapperRepository(LocationData.ClassTypeIDValue);
            var location = await locationRepository.GetByByteIdAsync<LocationData>(locationID);
            if (location.DefaultEmailAccountID.HasValue)
            {
                var emailAccount = await _repository.GetByShortIdAsync<EmailAccountData>(employee.DefaultEmailAccountID.Value);
                return Ok(_mapper.Map<EmailAccountData, EmailAccountDto>(emailAccount));
            }
            else
                return NotFound("Could not find a default email account.");
        }

        
        /// <summary>
        /// Creates a new EmailAccountData
        /// </summary>
        /// <param name="newModel">The Email Account model to create</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountDto), "Created a new email account")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Invalid IDs for relation with EmailAccount were supplied or there was a different issue with the request")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public async Task<IActionResult> Create([FromBody] EmailAccountDto newModel)
        {
            //newModel.EncryptRequestedNewPassword(_options.EncryptionPassPhrase);

            var mappedModel = _mapper.Map<EmailAccountData>(newModel);

            await mappedModel.CheckIfValid(_dapperRepository);
            if (!mappedModel.IsValid)
            {
                var errorMessage = mappedModel.GetModelStateErrorsString();
                return BadRequest(errorMessage);
            }
          
            mappedModel.DomainEmail = await _repository.GetByShortIdAsync<DomainEmail>(mappedModel.DomainEmailID);

            if (mappedModel.DomainEmail == null)
                return base.NotFound($"The DomainEmail with ID {newModel.DomainEmailID} does not exists.");

            var validationResult = await mappedModel.IsValidEmailAccountForDomain(_repository);

            if (validationResult.Success)
            {
                var provider = _providerFactory.Create(mappedModel.DomainEmail.ProviderType, mappedModel.DomainEmail);

                if ((!string.IsNullOrWhiteSpace(newModel.Credentials)) || (!string.IsNullOrWhiteSpace(this._options.EncryptionPassPhrase)))
                    mappedModel.UpdateStatusBasedOnCredentials((await mappedModel.IsValidCredentials(provider, this._options.EncryptionPassPhrase)).Success, true);
                else
                    mappedModel.UpdateStatusBasedOnCredentials(false, true);

                var insertedModel = await _repository.AddAsync<EmailAccountData>(mappedModel);
                return Ok(_mapper.Map<EmailAccountDto>(insertedModel));
            }

            return base.BadRequest(validationResult.Message);
        }

        /// <summary>
        /// Get a list of EmailAccountData based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="includes"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountDto[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] EmailAccountFilter filters)
        {
            var emailAccountDatas = (await _repository.ListEntityAsync(new EmailAccountsSpecification(
                filters.IsActive,
                emailAddress: filters.EmailAddress)));

            return Ok(_mapper.Map<List<EmailAccountDto>>(emailAccountDatas));
        }

        /// <summary>
        /// Gets a single EmailAccountData by ID
        /// </summary>
        /// <param name="ID">The ID of the EmailAccountData to be retrieved</param>
        /// <param name="includes">Navigation Properties to include data for</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountDto), "Successfully retrieved the EmailAccountData")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "EmailAccountData was not found")]
        public async Task<IActionResult> ReadByIdWithIncludes(short ID, [FromQuery] EmailAccountIncludes includes = null)
        {
            var resp = await _repository.GetByShortIdAsync<EmailAccountData>(ID);

            // FIXME - Need to add the possibility to add includes on GetById
            if (includes != null && !(includes.IncludeTeams != Endor.Comm.Common.IncludesLevel.None))
                resp.EmailAccountTeamLinks = null;

            return Ok(_mapper.Map<EmailAccountDto>(resp));
        }

        /// <summary>
        /// Updates a EmailAccountData
        /// </summary>
        /// <param name="ID">The Id of the EmailAccountData to update</param>
        /// <param name="update">Contains the updated EmailAccountData model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountDto), "Successfully updated the EmailAccountData")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "EmailAccountData was not found")]
        public async Task<IActionResult> Update(short ID, [FromBody] EmailAccountDto update)
        {
            var emailAccountData = await _repository.GetByShortIdAsync<EmailAccountData>(ID);
            var oldCredentials = JsonConvert.DeserializeObject<EmailAccountCredentials>(emailAccountData.Credentials ?? string.Empty) ?? new EmailAccountCredentials();

            if (!String.IsNullOrWhiteSpace(update.NewPassword))
                update.EncryptNewRequestedPassword(_options.EncryptionPassPhrase);

            emailAccountData = _mapper.Map(update, emailAccountData);
            emailAccountData.BID = BID.Value;

            var newCredentials = JsonConvert.DeserializeObject<EmailAccountCredentials>(emailAccountData.Credentials ?? string.Empty) ?? new EmailAccountCredentials();

            if (newCredentials.Password.Equals(oldCredentials.Password) || String.IsNullOrWhiteSpace(newCredentials.Password))
                emailAccountData.Credentials = JsonConvert.SerializeObject(oldCredentials);

            await emailAccountData.CheckIfValid(_dapperRepository);
            if (!emailAccountData.IsValid)
            {
                var errorMessage = emailAccountData.GetModelStateErrorsString();
                return BadRequest(errorMessage);
            }
            var result = await emailAccountData.CanUpdateDomain(update.DomainEmailID, _repository);
            if(result.IsError)
            {
                return base.BadRequest(result);
            }
            await _repository.UpdateAsync(emailAccountData);
            return Ok(_mapper.Map<EmailAccountDto>(emailAccountData));
        }

        /// <summary>
        /// Deletes a EmailAccountData
        /// </summary>
        /// <param name="ID">The ID of the EmailAccountData to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "EmailAccountData is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "EmailAccountData was not found")]
        public async Task<IActionResult> Delete(short ID)
        {
            var emailAccountData = await _repository.GetByShortIdAsync<EmailAccountData>(ID);
            await _repository.DeleteAsync(emailAccountData);
            return Ok();
        }

        /// <summary>
        /// Sets a EmailAccountData to Active
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the EmailAccountData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            var emailAccountData = await _repository.GetByShortIdAsync<EmailAccountData>(ID);
            emailAccountData.MarkAsActive();
            await _repository.UpdateAsync(emailAccountData);
            return Ok(new SuccessfulActionResponse()
            {
                ID = ID,
                message = "Email Account marked as active"
            });
        }

        /// <summary>
        /// Sets an EmailAccountData to Inactive
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the EmailAccountData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            var emailAccountData = await _repository.GetByShortIdAsync<EmailAccountData>(ID);
            emailAccountData.MarkAsInActive();
            await _repository.UpdateAsync(emailAccountData);
            return Ok(new SuccessfulActionResponse()
            {
                ID = ID,
                message = "Email Account marked as inactive"
            });
        }

        /// <summary>
        /// Returns true | false based on whether the EmailAccountData can be deleted.
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the EmailAccountData does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            var emailAccountData = await _repository.GetByShortIdAsync<EmailAccountData>(ID);
            var result = await emailAccountData.CanDeleteAsync(_repository);
            return Ok(new BooleanResponse()
            {
                message = result.Item2,
                Value = result.Item1,
                ID = ID
            });
        }

        /// <summary>
        /// Tests a EmailAccountData
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/test")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the EmailAccountData does not exist.")]
        public async Task<IActionResult> Test(short ID)
        {
            var emailAccountData = await _repository.GetByShortIdAsync<EmailAccountData>(ID);

            var participantFrom = new MessageParticipantInfo()
            {
                EmployeeID = _httpCtx.HttpContext.User.EmployeeID().Value,
                BID = BID.Value,
                UserName = emailAccountData.EmailAddress,
                Channel = MessageChannelType.Email,
                ParticipantRoleType = MessageParticipantRoleType.From
            };

            var participantTo = new MessageParticipantInfo()
            {
                EmployeeID = _httpCtx.HttpContext.User.EmployeeID().Value,
                BID = BID.Value,
                UserName = emailAccountData.EmailAddress,
                Channel = MessageChannelType.Email,
                ParticipantRoleType = MessageParticipantRoleType.To
            };

            var messageBody = new MessageBody
            {
                BID = BID.Value,
                HasBody = false,
                Subject = $"Test Email {DateTime.UtcNow.ToLongDateString()} UTC",
                BodyFirstLine = "Test email content",
                MessageParticipantInfos = new List<MessageParticipantInfo>
                {
                    participantFrom, participantTo
                }
            };

            var createdMessageBody = await _repository.AddAsync<MessageBody>(messageBody);

            var messageHeader = new MessageHeader(true, false)
            {
                BID = BID.Value,
                ModifiedDT = DateTime.Now,
                ReceivedDT = DateTime.Now,
                EmployeeID = _httpCtx.HttpContext.User.EmployeeID().Value,
                IsDeleted = false,
                DeletedOrExpiredDT = DateTime.Now,
                IsExpired = false,
                Channels = MessageChannelType.Email,
                InSentFolder = false,
                BodyID = messageBody.ID,
                ParticipantID = participantFrom.ID
            };

            await _repository.AddAsync<MessageHeader>(messageHeader);

            var emailMessage = new EmailMessage
            {
                Content = messageBody.BodyFirstLine,
                Subject = messageBody.Subject,
                EmailAccountEmailAddress = emailAccountData.EmailAddress
            };

            var getFromFromParticipantsInfo = createdMessageBody.MessageParticipantInfos.FirstOrDefault(x => x.ParticipantRoleType == MessageParticipantRoleType.From);
            emailMessage.FromAddresses.Add(new EmailAddress(getFromFromParticipantsInfo.UserName, getFromFromParticipantsInfo.UserName));

            var getToFromParticipantsInfo = createdMessageBody.MessageParticipantInfos.FirstOrDefault(x => x.ParticipantRoleType == MessageParticipantRoleType.To);
            emailMessage.ToAddresses.Add(new EmailAddress(getToFromParticipantsInfo.UserName, getToFromParticipantsInfo.UserName));

            var sendResult = await emailAccountData.SendEmail(emailMessage,
                _providerFactory.Create(emailAccountData.DomainEmail.ProviderType, emailAccountData.DomainEmail),
                this._options.EncryptionPassPhrase);

            messageHeader.SaveDeliveryRecords();

            await _repository.UpdateAsync<MessageHeader>(messageHeader);

            return Ok();
        }

        /// <summary>
        /// Links an EmailAccount with an EmployeeTeam
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <param name="TeamID">EmployeeTeam ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/Action/LinkTeam/{TeamID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmailAccountData or the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the EmailAccountData fails to link with the EmployeeTeam.")]
        public async Task<IActionResult> LinkTeam(short ID, short TeamID)
        {
            var emailAccountData = await _repository.GetByShortIdAsync<EmailAccountData>(ID);
            return Ok(await UnLinkTeam(ID, TeamID, true));
        }

        /// <summary>
        /// Unlinks an EmailAccount from an EmployeeTeam
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <param name="TeamID">EmployeeTeam ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/Action/UnlinkTeam/{TeamID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmailAccountData or the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the EmailAccountData fails to unlink from the EmployeeTeam.")]
        public async Task<IActionResult> UnlinkTeam(short ID, short TeamID)
        {
            return Ok(await UnLinkTeam(ID, TeamID, false));
        }

        private async Task<InternalBooleanResponse> UnLinkTeam(short ID, short teamID, bool makeLink)
        {
            bool undoLink = !makeLink;

            var emailAccountData = await _repository.GetByShortIdAsync<EmailAccountData>(ID);

            if (emailAccountData == null)
            {
                return new InternalBooleanResponse
                {
                    message = "Email Account Not Found",
                    Success = false
                };
            }

            var employeeTeamRespository = GetDapperRepository(EmployeeTeam.ClassTypeIDValue);
            var employeeTeamW = EmployeeTeam.GetEmployeeTeamWithEmployeeTeamID(employeeTeamRespository.Connection(), BID.Value, teamID);

            if (employeeTeamW == null)
            {
                return new InternalBooleanResponse
                {
                    message = "Team Not Found",
                    Success = false
                };
            }

            //check if it already exists
            var existingLinks = await _repository.ListEntityAsync(new EmailAccountTeamLinkFilterByTeamSpecification(ID, teamID));
            bool hasExistingLink = existingLinks != null && existingLinks.Count > 0;


            if (undoLink && !hasExistingLink)
                return new InternalBooleanResponse
                {
                    message = "Email Account and Team already unlinked",
                    Success = true
                };
            else if (undoLink && hasExistingLink)
            {
                await _repository.DeleteListAsync(existingLinks);
                return new InternalBooleanResponse
                {
                    message = $"Successfully {(makeLink ? "linked" : "unlinked")} domain email and location",
                    Success = true
                };
            }
            else if (makeLink && !hasExistingLink)
            {
                await _repository.AddAsync(new EmailAccountTeamLink()
                {
                    BID = emailAccountData.BID,
                    EmailAccountID = emailAccountData.IDAsShort,
                    TeamID = teamID
                });

                return new InternalBooleanResponse
                {
                    message = $"Successfully {(makeLink ? "linked" : "unlinked")} domain email and location",
                    Success = true
                };
            }
            else
                return new InternalBooleanResponse
                {
                    message = "Domain and location already linked",
                    Success = true
                };
        }

        /// <summary>
        /// Returns simple list of domain email
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <param name="locationID"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet("location/{locationID}/simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Location with the specified LocationID is not found.")]
        public async Task<IActionResult> SimpleListByLocation(byte locationID, [FromQuery]EmailAccountFilter filters)
        {
            // verify LocationID matches existing LocationData
            var locationRepository = GetDapperRepository(LocationData.ClassTypeIDValue);
            await locationRepository.GetByShortIdAsync<LocationData>(locationID);

            var domainEmailLocationLink = (await _repository.ListEntityAsync(new DomainEmailLocationLinkFilterByLocationSpecification(locationID)))?.Select(a => a.DomainID)?.Distinct();

            if (domainEmailLocationLink == null || !domainEmailLocationLink.Any())
                return NotFound("Domain email for this location does not exists.");

            var emailAccountsForDomainID = await _repository.ListEntityAsync(new EmailAccountsSpecification(filters.IsActive, "", null, domainEmailLocationLink.ToArray()));

            return Ok(MapToSimpleList(emailAccountsForDomainID));
        }

        /// <summary>
        /// Returns simple list of domain email
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="IncludeShared"></param>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<IActionResult> SimpleListByEmployee([FromQuery]EmailAccountFilter filters, [FromQuery] bool IncludeShared = true, [FromQuery] int? orderid = null, [FromQuery] int? estimateid = null, [FromQuery] int? companyid = null)
        {
            if (!User.EmployeeID().HasValue)
                return new UnauthorizedResult();

            var employeeId = User.EmployeeID().Value;
            var emailAccounts = await _repository.ListEntityAsync(new EmailAccountsByEmployeeSpecification(filters.IsActive, User.EmployeeID().Value));
            var employeeTeamRespository = GetDapperRepository(EmployeeTeam.ClassTypeIDValue);
            var employeeTeamWithEmployeeTeamLinks = EmployeeTeam.GetEmployeeTeamWithEmployeeTeamLinks(employeeTeamRespository.Connection(), BID.Value)
                .SelectMany(a => a.EmployeeTeamLinks)
                .Where(a => a.EmployeeID == employeeId)
                .Select(a => a.TeamID)
                .Distinct()
                .ToList();

            var teamEmailAccounts = (await _repository.ListEntityAsync<EmailAccountTeamLink>(new EmailAccountTeamLinkIncludeEmailAccountSpecification()))
                .Where(x => employeeTeamWithEmployeeTeamLinks.Contains(x.TeamID))
                .Select(x => x.EmailAccount).ToList();

            emailAccounts.AddRange(teamEmailAccounts);

            if (IncludeShared)
            {
                byte locationID = await GetDefaultLocation(User.EmployeeID().Value, orderid, estimateid, companyid);
                emailAccounts.AddRange(await GetEmailAccountsForLocation(filters.IsActive, locationID));
                emailAccounts = emailAccounts.GroupBy(e => e.ID).Select(e => e.First()).ToList();
            }

            return Ok(MapToSimpleList(emailAccounts));
        }

        private SimpleEmailAccountData[] MapToSimpleList(List<EmailAccountData> results)
        {
            return results.Select(a => new SimpleEmailAccountData
            {
                ID = a.ID,
                ClassTypeID = a.ClassTypeID,
                DisplayName = a.DisplayName,
                EmployeeID = a.EmployeeID,
                IsActive = a.IsActive
            }).ToArray();
        }
    }
}
