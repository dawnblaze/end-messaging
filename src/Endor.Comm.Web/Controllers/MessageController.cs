﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.ApiModels;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using Microsoft.AspNetCore.Mvc;
using Endor.Comm.Common;
using Endor.Comm.Core.Specifications;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Endor.Comm.QueryParameters;
using Endor.Comm.Core.Services;
using Endor.Security;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Common.Documents;
using Endor.Comm.Web.Common;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Endor.Comm.Controllers
{
    [Route("api/message")]
    public class MessageController : BaseController
    {
        public MessageController(IRepository repository, IHttpContextAccessor httpCtx, ITenantDataCache tenantCache)
            : base(repository, httpCtx, tenantCache)
        {

        }
      
        /// <summary>
        /// Get Single Message
        /// </summary>
        /// <param name="headerID"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        [HttpGet("{headerID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OkObjectResult))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<MessageDto> GetSingleMessage([FromRoute]int headerID, [FromQuery]MessageIncludes includes)
        {
            var messageHeader = (await _repository.ListEntityAsync<MessageHeader>(new MessageHeaderIncludeSpecification(
                BID.Value,
                headerID,
                includes.IncludeDeliveryHistory,
                includes.IncludeLinks))).FirstOrDefault();

            var msgBreakdown = new MessageBreakdownService();
            var documentManager = GetDocumentManager(messageHeader.BID, messageHeader.MessageBody.BID, messageHeader.MessageBody.ClassTypeID, BucketRequest.Data, _tenantCache);
            await msgBreakdown.PopulateMessageBreakdownWithMessageHeader(messageHeader, documentManager);
            return new MessageDto(msgBreakdown);
        }

        
        /// <summary>
        /// Get Filtered Messages
        /// </summary>
        /// <param name="channelID"></param>
        /// <param name="includes"></param>
        /// <param name="filters"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        [HttpGet("channel/{channelID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ICollection<MessageDto>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> GetFilteredMessages([FromRoute]MessageChannelType channelID,
                                                                    [FromQuery]MessageIncludes includes,
                                                                    [FromQuery]MessageFilters filters,
                                                                    [FromQuery]int skip = 0,
                                                                    [FromQuery]int take = 100)
        {
            if (filters.EmployeeID==0)
                return BadRequest(new string[1] { "EmployeeID query parameter must be defined"});
            var messageHeaders = (await _repository.ListEntityAsync<MessageHeader>(new MessageHeadersFilterAndIncludeSpecification(
                BID.Value,
                channelID,
                filters.EmployeeID,
                filters.IncludeRead,
                filters.IncludeSent,
                filters.IncludeDeleted,
                includes.IncludeDeliveryHistory,
                includes.IncludeLinks,
                take,
                skip)));
            List<MessageDto> messagesToReturn = new List<MessageDto>();
            foreach (var messageHeader in messageHeaders)
            {
                var msgBreakdown = new MessageBreakdownService();
                var documentManager = GetDocumentManager(messageHeader.BID, messageHeader.MessageBody.BID, messageHeader.MessageBody.ClassTypeID, BucketRequest.Data, _tenantCache);
                await msgBreakdown.PopulateMessageBreakdownWithMessageHeader(messageHeader, documentManager);
                messagesToReturn.Add(new MessageDto(msgBreakdown));
            }
            return Ok(messagesToReturn);
        }

        /// <summary>
        /// CreateMessage
        /// </summary>
        /// <param name="msg"></param>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OkObjectResult))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BadRequestObjectResult), description: "If there's a failure..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> CreateMessage([FromBody]MessageDto msg)
        {
            var canConvert = msg.ValidateConversionToMessageBreakdown();
            if (!canConvert.Item1)
                return BadRequest(new string[2] { "Execution Failures creating Message", canConvert.Item2 });
            if (_httpCtx?.HttpContext?.User?.EmployeeID()==null)
                return BadRequest(new string[2] { "Execution Failures creating Message", "Missing EmployeeID" });
            var employeeID = _httpCtx?.HttpContext?.User?.EmployeeID().Value;
            msg.EmployeeID = employeeID.Value;
            msg.Participants.Where(p => p.ParticipantRoleType == MessageParticipantRoleType.From).First().EmployeeID = msg.EmployeeID;
            var msgBreakdown = msg.ToMessageBreakdown();
            
            //create message Body
            await _repository.AddAsync(msgBreakdown.MessageBody);
            var docman = GetDocumentManager(msgBreakdown.MessageBody.BID, msgBreakdown.MessageBody.ID, msgBreakdown.MessageBody.ClassTypeID, BucketRequest.Data, _tenantCache);
            var tskSetHtmlBody = msgBreakdown.MessageBody.SetHtmlMessageBody(msg.Body, _tenantCache, docman);
            await tskSetHtmlBody;

            var employeeTeamRespository = GetDapperRepository(EmployeeTeam.ClassTypeIDValue);
            var employeeTeamWithEmployeeTeamLinks = EmployeeTeam.GetEmployeeTeamWithEmployeeTeamLinks(employeeTeamRespository.Connection(), BID.Value);
            var allHeaders = await msgBreakdown.GetAllHeadersAsync(employeeTeamWithEmployeeTeamLinks);

            foreach (var header in allHeaders)
            {
                await _repository.AddAsync(header);
            }
            msgBreakdown.MessageHeader = allHeaders.Where(h => h.InSentFolder).FirstOrDefault();
            var messageToReturn = new MessageDto(msgBreakdown);
            return Ok(messageToReturn);
        }

        /*/// <summary>
        /// Create Email Message
        /// </summary>
        /// <param name="msgBody"></param>
        [HttpPost("email")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ExecutionResult))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ExecutionResult), description: "If there's a failure..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<MessageDto> CreateEmailMessage([FromBody]string msgBody)
        {
            //create To Participants
            List<MessageParticipantInfo> toParticipants = new List<MessageParticipantInfo>();
            toParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.To, ParseQueryString("ToEmail")));
            toParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.To, ClassType.Contact, ParseQueryInt("ToContactID")));
            toParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.To, ClassType.Employee, ParseQueryInt("ToEmployeeID"), true));

            //create cc Participate
            List<MessageParticipantInfo> ccParticipants = new List<MessageParticipantInfo>();
            ccParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.CC, ParseQueryString("CCEmail")));
            ccParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.CC, ClassType.Contact, ParseQueryInt("CCContactID")));
            ccParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.CC, ClassType.Employee, ParseQueryInt("CCEmployeeID"), true));

            //create From Participants
            List<MessageParticipantInfo> fromParticipants = new List<MessageParticipantInfo>();
            var employeeID = ParseQueryInt("From");
            fromParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.From, ClassType.Employee, employeeID, true));

            //create links
            List<MessageObjectLink> objectLinks = new List<MessageObjectLink>();
            List<int> orderID = ParseQueryInt("OrderID");
            List<int> estimateID = ParseQueryInt("EstimateID");
            List<int> companyID = ParseQueryInt("CompanyID");
            objectLinks.AddRange(createObjectLinks(ClassType.Order, orderID));
            objectLinks.AddRange(createObjectLinks(ClassType.Order, estimateID));
            objectLinks.AddRange(createObjectLinks(ClassType.Order, companyID));
            //create message
            var subject = ParseQueryString("Subject");
            Message msg = new Message
            {
                ReceivedDT = DateTime.UtcNow,
                EmployeeID = employeeID.Any() ? (short)employeeID.First() : User.EmployeeID().GetValueOrDefault(),
                ParticipantID = User.EmployeeID().GetValueOrDefault(),
                Channels = MessageChannelType.Email,
                Subject = subject.Any() ? subject.First() : "",
                HasBody = true,
                Body = msgBody,
                BID = User.BID().GetValueOrDefault(),
                Participants = toParticipants.Concat(ccParticipants).Concat(fromParticipants).ToArray(),
                ObjectLinks = objectLinks.ToArray()
            };
            var msgToSend = await this._service.CreateMessageAsync(msg, tempID);

            var emailService = AtomCRUDService<EmailAccountData, short>.CreateService<EmailAccountService>(this._context, this._taskQueuer, this._cache, this._logger, () => User.BID().Value, this._rtmClient, _migrationHelper);
            var employeeAccountOverride = ParseQueryInt("EmailAccountID");
            MimeMessage emailMsg = new MimeMessage();


            await emailService.Value.SendEmail(msg,
                this._options.EncryptionPassPhrase,
                (short)employeeID.First(),
                orderID.Count > 0 ? (int?)orderID.First() : null,
                estimateID.Count > 0 ? (int?)estimateID.First() : null,
                companyID.Count > 0 ? (int?)companyID.First() : null,
                employeeAccountOverride.Count > 0 ? (short?)employeeAccountOverride.First() : null
                );
            return msgToSend;


        }*/
        private List<MessageObjectLink> createObjectLinks(int type, List<int> IDs)
        {
            List<MessageObjectLink> returnList = new List<MessageObjectLink>();
            foreach (var id in IDs)
            {
                MessageObjectLink info = new MessageObjectLink()
                {
                    LinkedObjectClassTypeID = (int)type,
                    LinkedObjectID = id
                };
                returnList.Add(info);
            }
            return returnList;
        }

        private List<MessageParticipantInfo> createParticipantInfo(MessageParticipantRoleType roleType, List<string> emailAddresses)
        {
            List<MessageParticipantInfo> returnList = new List<MessageParticipantInfo>();
            foreach (var email in emailAddresses)
            {
                MessageParticipantInfo info = new MessageParticipantInfo()
                {
                    ParticipantRoleType = roleType,
                    Channel = MessageChannelType.Email,
                    UserName = email
                };
                returnList.Add(info);
            }
            return returnList;
        }

        private async Task<List<MessageParticipantInfo>> createParticipantInfo(MessageParticipantRoleType roleType, int type, List<int> IDs, bool IsEmployee = false)
        {
            List<MessageParticipantInfo> returnList = new List<MessageParticipantInfo>();
            foreach (var id in IDs)
            {
                var email = await this.GetEmailsForClassType(type, id);
                if (email.Length > 0)
                {
                    MessageParticipantInfo info = new MessageParticipantInfo()
                    {
                        ParticipantRoleType = roleType,
                        Channel = MessageChannelType.Email,
                        UserName = email,
                        EmployeeID = IsEmployee ? (short?)id : null
                    };
                    returnList.Add(info);
                }
            }
            return returnList;
        }

        /// <summary>
        /// Find Email Address BY ID
        /// </summary>
        /// <param name="type">ClassType</param>
        /// <param name="id"></param>
        private async Task<string> GetEmailsForClassType(int type, int id)
        {
            string emailAddress = "";
            var locatorRespository = GetDapperRepository(LocationData.ClassTypeIDValue);

            switch (type)
            {
                case ContactData.ClassTypeIDValue:
                    var contactLocators = ContactLocator.GetContactLocatorsForContactID(locatorRespository.Connection(), BID.Value, id);
                    var result = contactLocators.Where(l => l.eLocatorType == LocatorType.Email);
                    if (result.Any()) emailAddress = result.First().Locator;
                    break;
                case EmployeeData.ClassTypeIDValue:
                    var employeeDatas = EmployeeLocator.GetEmployeeLocatorsForEmployeeID(locatorRespository.Connection(), BID.Value, id);
                    var resultEmp = employeeDatas.First();
                    if (resultEmp.DefaultEmailAccountID != null)
                    {
                        emailAddress = (await _repository.GetByIdAsync<EmailAccountData>(resultEmp.DefaultEmailAccountID.Value)).EmailAddress;
                    }
                    else
                    {
                        var results = resultEmp.EmployeeLocators.Where(l => l.eLocatorType == LocatorType.Email);
                        if (results.Any()) emailAddress = results.First().Locator;
                    }
                    break;
            }
            return emailAddress;
        }

        private List<string> ParseQueryString(string field)
        {
            return Request.Query[field].ToList();
        }

        private List<int> ParseQueryInt(string field)
        {
            List<int> returnList = new List<int>();
            var values = Request.Query[field].ToList();
            foreach (var val in values)
            {
                int x = 0;
                if (Int32.TryParse(val, out x))
                {
                    returnList.Add(x);
                }
            }
            return returnList;
        }

        /// <summary>
        /// Mark MessageHeader as deleted(will not do an actual delete on DB)
        /// </summary>
        /// <param name="messageID">int</param>
        /// <returns></returns>
        [HttpDelete("{messageID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OkResult))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> Delete(int messageID)
        {
            try
            {
                var messageHeader = await _repository.GetByIdAsync<MessageHeader>(messageID);
                if (messageHeader == null)
                    return NotFound(new string[1] { $"Could not find message with ID of {messageID}" });
                if (!messageHeader.IsDeleted) {
                    messageHeader.MarkAsDeleted();
                    await _repository.UpdateAsync(messageHeader);
                    return Ok();
                } else
                {
                    await _repository.DeleteAsync(messageHeader);
                    return Ok(new SuccessfulActionResponse()
                    {
                        ID = messageID,
                        message = "Message deleted"
                    });
                }
            }
            catch (Exception e)
            {
                return BadRequest(new string[2] { "Execution Failure marking message as read", e.Message });
            }
        }



        /// <summary>
        /// Undelete msgHeader
        /// </summary>
        /// <param name="messageID">int</param>
        /// <returns></returns>
        [HttpPost("{messageID}/action/undelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> Undelete([FromRoute]int messageID)
        {
            try
            {
                var messageHeader = await _repository.GetByIdAsync<MessageHeader>(messageID);
                if (messageHeader == null)
                    return NotFound(new string[1] { $"Could not find message with ID of {messageID}" });
                messageHeader.Undelete();
                await _repository.UpdateAsync(messageHeader);
                return Ok(new SuccessfulActionResponse()
                {
                    ID = messageID,
                    message = "Message undeleted"
                });
            }
            catch (Exception e)
            {
                return BadRequest(new string[2] { "Execution Failure marking message as read", e.Message });
            }
        }
        
        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="messageID">int</param>
        /// <returns></returns>
        [HttpPost("{messageID}/action/markasread")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> MarkAsRead([FromRoute]int messageID)
        {
            var messageHeader = await _repository.GetByIdAsync<MessageHeader>(messageID);
            if (messageHeader == null)
                return NotFound(new string[1] { $"Could not find message with ID of {messageID}" });
            messageHeader.MarkAsRead();
            await _repository.UpdateAsync(messageHeader);
            return Ok(new SuccessfulActionResponse()
            {
                ID = messageID,
                message = "Message marked as read"
            });
        }
        
        /// <summary>
        /// MarkAsUnRead
        /// </summary>
        /// <param name="headerID">int</param>
        /// <returns></returns>
        [HttpPost("{messageID}/action/markasunread")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> MarkAsUnRead([FromRoute]int messageID)
        {
            try
            {
                var messageHeader = await _repository.GetByIdAsync<MessageHeader>(messageID);
                if (messageHeader == null)
                    return NotFound(new string[1] { $"Could not find message with ID of {messageID}" });
                messageHeader.MarkAsUnread();
                await _repository.UpdateAsync(messageHeader);
                return Ok(new SuccessfulActionResponse()
                {
                    ID = messageID,
                    message = "Message marked as unread"
                });
            }
            catch (Exception e)
            {
                return BadRequest(new string[2] { "Execution Failure marking message as read", e.Message });
            }
        }

        /// <summary>
        /// MarkAllRead
        /// </summary>
        /// <param name="channelID">Endor.Models.MessageChannelType</param>
        /// <param name="EmployeeID">short?</param>
        [HttpPost("channel/{channelID}/action/markallread")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> MarkAllRead([FromRoute]MessageChannelType channelID, [FromQuery] short? EmployeeID)
        {
            try
            {
                if (EmployeeID == null)
                    EmployeeID = _httpCtx?.HttpContext?.User?.EmployeeID().Value;
                var messageHeaders = await _repository.ListEntityAsync<MessageHeader>(new RetrieveAllMessagesInAChannel(BID.Value,channelID,EmployeeID));
                messageHeaders.ForEach(messageHeader =>
                {
                    messageHeader.MarkAsRead();
                });
                await _repository.UpdateListAsync(messageHeaders);
                return Ok(new SuccessfulActionResponse()
                {
                    ID = (byte)channelID,
                    message = "Messages in channel are marked as read"
                });
            }
            catch (Exception e)
            {
                return BadRequest(new string[2] { "Execution Failure marking message as read", e.Message });
            }
        }

        /// <summary>
        /// MarkAllDeleted
        /// </summary>
        /// <param name="channelID">Endor.Models.MessageChannelType</param>
        /// <param name="EmployeeID">short?</param>
        [HttpPost("channel/{channelID}/action/deleteall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> MarkAllDeleted([FromRoute]MessageChannelType channelID, [FromQuery] short? EmployeeID)
        {
            try
            {
                if (EmployeeID==null)
                    EmployeeID = _httpCtx?.HttpContext?.User?.EmployeeID().Value;
                var messageHeaders = await _repository.ListEntityAsync<MessageHeader>(new RetrieveAllMessagesInAChannel(BID.Value, channelID, EmployeeID));
                messageHeaders.ForEach(messageHeader =>
                {
                    messageHeader.MarkAsDeleted();
                });
                await _repository.UpdateListAsync(messageHeaders);
                return Ok(new SuccessfulActionResponse()
                    {
                        ID = (byte)channelID,
                        message = "Messages in channel are deleted"
                    });
            }
            catch (Exception e)
            {
                return BadRequest(new string[2] { "Execution Failure marking message as read", e.Message });
            }
        }
    }
}
