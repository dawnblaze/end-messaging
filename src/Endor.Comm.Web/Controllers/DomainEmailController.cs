﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Endor.Comm.ApiModels;
using Endor.Comm.Common;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Factories.Interfaces;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.Core.Non_Owned_Entities;
using Endor.Comm.Core.Services.Classes;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.Core.SharedKernal;
using Endor.Comm.Core.Specifications;
using Endor.Comm.QueryParameters;
using Endor.Comm.Web.Common;
using Endor.Security;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Endor.Comm.Web.Controllers
{
    [Route("api/domain/email")]
    public class DomainEmailController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IKeyedFactory<IEmailProvider, EmailProviderType> _providerFactory;
        private readonly Core.Configurations.EndorOptions _options;

        public DomainEmailController(IRepository repository, IHttpContextAccessor httpCtx, ITenantDataCache tenantCache, IMapper mapper, IKeyedFactory<IEmailProvider, EmailProviderType> providerFactory, Core.Configurations.EndorOptions options) : base(repository, httpCtx, tenantCache)
        {
            _mapper = mapper;
            _providerFactory = providerFactory ?? throw new ArgumentNullException(nameof(providerFactory));
            _options = options ?? throw new ArgumentNullException(nameof(options));
        }

        /// <summary>
        /// Creates a new DomainEmail
        /// </summary>
        /// <param name="newModel">The Email Domail model to create</param>
        /// <param name="tempID">A temporary ID used when creating objects with document storage</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OkObjectResult), "Created a new email domain")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public async Task<IActionResult> Create([FromBody] DomainEmailDto newModel, [FromQuery] Guid? tempID = null)
        {
            var domainEmail = _mapper.Map<DomainEmail>(newModel);
            await _repository.AddAsync<DomainEmail>(domainEmail);
            return Ok(_mapper.Map<DomainEmailDto>(domainEmail));
        }

        /// <summary>
        /// Get a list of DomainEmail based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of DomainEmail</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainEmailDto[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] DomainEmailFilter filters)
        {
            var domainEmails = (await _repository.ListEntityAsync<DomainEmail>(new DomainEmailFilterByActiveSpecification(
                filters.IsActive)));
            //Location filter has to be done seperately since we access child objects
            if (filters.LocationID != null)
                domainEmails = domainEmails.Where(d => d.IsForAllLocations || (d.LocationLinks.FirstOrDefault(l => l.LocationID == filters.LocationID.Value) != null)).ToList();
            return Ok(_mapper.Map<List<DomainEmail>, List<DomainEmailDto>>(domainEmails));
        }

        /// <summary>
        /// Gets a single DomainEmail by ID
        /// </summary>
        /// <param name="ID">The ID of the DomainEmail to be retrieved</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainEmailDto), "Successfully retrieved the DomainEmail")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "DomainEmail was not found")]
        public async Task<IActionResult> ReadById(short ID)
        {
            var domainEmail = await _repository.GetByShortIdAsync<DomainEmail>(ID);
            return Ok(_mapper.Map<DomainEmailDto>(domainEmail));
        }


        /// <summary>
        /// Updates a DomainEmail
        /// </summary>
        /// <param name="ID">The Id of the DomainEmail to update</param>
        /// <param name="update">Contains the updated DomainEmail model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainEmailDto), "Successfully updated the DomainEmail")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "DomainEmail was not found")]
        public async Task<IActionResult> Update(short ID, [FromBody] DomainEmailDto update)
        {
            var domainEmail = await _repository.GetByShortIdAsync<DomainEmail>(ID);
            domainEmail = _mapper.Map(update, domainEmail);
            domainEmail.BID = BID.Value;
            await _repository.UpdateAsync(domainEmail);
            return Ok(_mapper.Map<DomainEmailDto>(domainEmail));
        }

        /// <summary>
        /// Deletes a DomainEmail
        /// </summary>
        /// <param name="ID">The ID of the DomainEmail to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "DomainEmail is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> Delete(short ID)
        {
            var domainEmail = await _repository.GetByShortIdAsync<DomainEmail>(ID);
            await _repository.DeleteAsync(domainEmail);
            return Ok();
        }

        /// <summary>
        /// Sets a DomainEmail to Active
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(DomainEmailDto), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the DomainEmail does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            var domainEmail = await _repository.GetByShortIdAsync<DomainEmail>(ID);
            domainEmail.MarkAsActive();
            await _repository.UpdateAsync(domainEmail);
            return Ok(new SuccessfulActionResponse()
                {
                    ID = ID,
                    message = "Domain Email set as active"
                });
        }

        /// <summary>
        /// Sets an DomainEmail to Inactive
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SuccessfulActionResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the DomainEmail does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            var domainEmail = await _repository.GetByShortIdAsync<DomainEmail>(ID);
            domainEmail.MarkAsInActive();
            await _repository.UpdateAsync(domainEmail);
            return Ok(new SuccessfulActionResponse()
            {
                ID = ID,
                message = "Domain Email set as inactive"
            });
        }

        /// <summary>
        /// Returns true | false based on whether the DomainEmail can be deleted.
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the DomainEmail does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            var domainEmail = await _repository.GetByShortIdAsync<DomainEmail>(ID);
            var result = await domainEmail.CanDeleteAsync(_repository);
            return Ok(new BooleanResponse()
            {
                message = result.Item2,
                Value = result.Item1,
                ID = ID
            });
        }

        /// <summary>
        /// Tests a DomainEmail
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <param name="creds"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/test")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the DomainEmail fails to test.")]
        public async Task<IActionResult> Test(short ID, [FromBody]DomainEmailTestCredentials creds)
        {
            var domainEmail = await _repository.GetByShortIdAsync<DomainEmail>(ID);

            if (domainEmail == null)
                return NotFound("Domain Email of {ID} could not be found");

            domainEmail.LastVerificationAttemptDT = DateTime.UtcNow;

            var result = await DoTest(domainEmail, creds);

            domainEmail.LastVerificationSuccess = result.Success;
            domainEmail.LastVerificationResult = result.Success ? "Success" : result.message;
            if (result.Success)
                domainEmail.LastVerifiedDT = domainEmail.LastVerificationAttemptDT;

            await _repository.UpdateAsync(domainEmail);

            if (result.Success)
                return Ok(new BooleanResponse
                {
                    message = "Test Successful",
                    ID = ID
                });
            else return BadRequest("Email Credentials or Password Not Supplied or Valid");
        }

        private async Task<InternalBooleanResponse> DoTest(DomainEmail domainEmail, DomainEmailTestCredentials creds)
        {
            try
            {
                string emailAddress = creds.emailaddress;
                if (!emailAddress.Contains($"@{domainEmail.DomainName}"))
                    emailAddress = $"{creds.emailaddress}@{domainEmail.DomainName}";

                var emailMessage = new EmailMessage
                {
                    Content = $"Test Email {DateTime.UtcNow.ToLongDateString()} UTC",
                    Subject = "Test email content",
                    EmailAccountEmailAddress = emailAddress
                };
                emailMessage.FromAddresses.Add(new EmailAddress(emailAddress, emailAddress));
                emailMessage.ToAddresses.Add(new EmailAddress(emailAddress, emailAddress));

                var emailAccountData = new EmailAccountData()
                {
                    Credentials = "{ \"password\": \"" + StringEncryptor.Encrypt(creds.password, _options.EncryptionPassPhrase) + "\" }"
                };

                var sendResult = await emailAccountData.SendEmail(emailMessage,
                    _providerFactory.Create(domainEmail.ProviderType, domainEmail),
                    this._options.EncryptionPassPhrase);

                return new InternalBooleanResponse
                {
                    Success = true,
                    Value = true
                };
            } 
            catch (Exception ex)
            {
                return new InternalBooleanResponse
                {
                    Success = false,
                    message = ex.Message
                };
            }
        }

        private async Task<InternalBooleanResponse> UnLinkLocation(short ID, byte locationID, bool makeLink)
        {
            bool undoLink = !makeLink;

            var domainEmail = await _repository.GetByShortIdAsync<DomainEmail>(ID);

            if (domainEmail == null)
            {
                return new InternalBooleanResponse
                {
                    message = "Domain Not Found",
                    Success = false
                };
            }

            if (domainEmail.IsForAllLocations)
            {
                return new InternalBooleanResponse
                {
                    message = "This email domain is currently enabled for all Locations and may not be enabled or disabled by Location.",
                    Success = false
                };
            }

            var existingLocation = LocationDataLocator.GetLocationLocatorsForLocationID(GetDapperRepository(LocationData.ClassTypeIDValue).Connection(), BID.Value, locationID).FirstOrDefault();

            if (existingLocation == null)
            {
                return new InternalBooleanResponse
                {
                    message = "Location Not Found",
                    Success = false
                };
            }

            //check if it already exists
            var existingLinks = await _repository.ListEntityAsync(new DomainEmailLocationLinkFilterSpecification(BID.Value, ID, locationID));
            bool hasExistingLink = existingLinks != null && existingLinks.Count > 0;


            if (undoLink && !hasExistingLink)
                return new InternalBooleanResponse
                {
                    message = "Domain and location already unlinked",
                    Success = true
                };
            else if (undoLink && hasExistingLink)
            {
                await _repository.DeleteListAsync(existingLinks);
                return new InternalBooleanResponse
                {
                    message = $"Successfully {(makeLink ? "linked" : "unlinked")} domain email and location",
                    Success = true
                };
            }
            else if (makeLink && !hasExistingLink)
            {
                await _repository.AddAsync(new DomainEmailLocationLink()
                {
                    BID = domainEmail.BID,
                    DomainID = domainEmail.IDAsShort,
                    LocationID = locationID
                });

                return new InternalBooleanResponse
                {
                    message = $"Successfully {(makeLink ? "linked" : "unlinked")} domain email and location",
                    Success = true
                };
            }
            else
                return new InternalBooleanResponse
                {
                    message = "Domain and location already linked",
                    Success = true
                };
        }

        /// <summary>
        /// Links a DomainEmail and location
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <param name="locationID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/linklocation/{locationID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the DomainEmail or Location does not exist, fails to link, or is associated with All Locations.")]
        public async Task<IActionResult> LinkLocation(short ID, byte locationID)
        {
            var result = await UnLinkLocation(ID, locationID, true);
            result.ID = ID;
            if (result.Success)
                return Ok((BooleanResponse)result);
            else
                return BadRequest((BooleanResponse)result);
        }

        /// <summary>
        /// UnLinks a DomainEmail and location
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <param name="locationID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/unlinklocation/{locationID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(NotFoundResult), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(ActionErrorResponse), description: "If the DomainEmail or Location does not exist, fails to unlink, or is associated with All Locations.")]
        public async Task<IActionResult> UnlinkLocation(short ID, byte locationID)
        {
            var result = await UnLinkLocation(ID, locationID, false);
            result.ID = ID;
            if (result.Success)
                return Ok((BooleanResponse)result);
            else
                return BadRequest((BooleanResponse)result);
        }

        /// <summary>
        /// returns list of all smtp configuration types
        /// </summary>
        /// <returns></returns>
        [HttpGet("smtpconfigurationtype")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemEmailSMTPConfigurationType[]))]
        public async Task<IActionResult> SMTPConfigurationType()
        {
            var types = await _repository.ListAsync<SystemEmailSMTPConfigurationType>();
            return Ok(types);
        }


        /// <summary>
        /// Returns simple list of domain email
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleListItem[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<IActionResult> SimpleList([FromQuery]DomainEmailFilter filters)
        {
            var domainEmails = (await _repository.ListEntityAsync<DomainEmail>(new DomainEmailFilterByActiveSpecification(               
               filters.IsActive)));
            //Location filter has to be done seperately since we access child objects
            if (filters.LocationID != null)
                domainEmails = domainEmails.Where(d => d.IsForAllLocations || (d.LocationLinks.FirstOrDefault(l => l.LocationID == filters.LocationID.Value) != null)).ToList();
            return Ok(_mapper.Map<List<DomainEmail>, List<SimpleListItem>>(domainEmails));
        }
    }
}