﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Comm.Core.Interfaces;
using Endor.Comm.ApiModels;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.SharedKernal;
using Microsoft.AspNetCore.Mvc;
using Endor.Comm.Common;
using Endor.Comm.Core.Specifications;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Endor.Comm.QueryParameters;
using AutoMapper;

namespace Endor.Comm.Web.Controllers
{
    [Route("api/system/messagetemplatetype")]
    public class SystemMessageTemplateTypeController : BaseController
    {
        private readonly IMapper _mapper;
        
        public SystemMessageTemplateTypeController(IRepository repository, IHttpContextAccessor httpCtx, ITenantDataCache tenantCache, IMapper mapper)
            : base(repository, httpCtx, tenantCache)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all of the SystemMessageTemplateType.
        /// Can be filtered by AppliesClasstypeID and ChannelType
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ICollection<SystemMessageTemplateType>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SystemMessageTemplateType>> ReadWithFilters([FromQuery] SystemMessageTemplateTypeFilters filters)
        {
            var systemMessageTemplateTypes = (await _repository.ListAsync<SystemMessageTemplateType>());
            systemMessageTemplateTypes = SpecificationEvaluator<SystemMessageTemplateType>.GetQuery(systemMessageTemplateTypes.AsQueryable(), 
                new SystemMessageTemplateTypeFilterSpecification(
                filters.AppliesToClasstypeID,
                filters.ChannelType)).ToList();
            return systemMessageTemplateTypes;
        }

        /// <summary>
        /// Returns a simple list of all of the SystemMessageTemplateType.
        /// Can be filtered by AppliesClasstypeID and ChannelType
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ICollection<SimpleListItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleListItem>> FilteredSimpleList([FromQuery]  SystemMessageTemplateTypeFilters filters)
        {
            var systemMessageTemplateTypes = (await _repository.ListAsync<SystemMessageTemplateType>());
            systemMessageTemplateTypes = SpecificationEvaluator<SystemMessageTemplateType>.GetQuery(systemMessageTemplateTypes.AsQueryable(), new SystemMessageTemplateTypeFilterSpecification(
                filters.AppliesToClasstypeID,
                filters.ChannelType)).ToList();
            return _mapper.Map<List<SystemMessageTemplateType>, List<SimpleListItem>>(systemMessageTemplateTypes); ;
        }
    }
}
