using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Endor.Comm.Infrastructure.Data;
using Endor.Comm.Infrastructure;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerUI;
using AutoMapper;
using Endor.Comm.Common;
using Endor.Comm.Infrastructure.Common;
using Endor.Comm.Core.Configurations;
using Endor.Comm.Web.Common;
using Endor.Comm.Core.Factories.Interfaces;
using Endor.Comm.Core.Services.Interfaces;
using Endor.Comm.Core.Entities;
using Endor.Comm.Core.Factories.Classes;

namespace Endor.Comm
{
    public abstract class BaseStartup
    {
        private const string swaggerVersion = "v1.0.1";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public BaseStartup(IWebHostEnvironment env)
        {
            IConfigurationBuilder builder = GetConfigurationBuilder(env);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }

        /// <summary>
        /// Returns a configuration builder
        /// </summary>
        /// <param name="env"></param>
        /// <returns></returns>
        public virtual IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            return new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // NOTE: added code for net core 3
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConfiguration(Configuration.GetSection("Logging"));
                loggingBuilder.AddConsole();
                loggingBuilder.AddDebug();
            });

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };

            services.Configure<EndorOptions>(Configuration.GetSection("Endor"));
            services.AddHttpContextAccessor();
            services.AddMemoryCache();
            EndorOptions endorOptions = Configuration.GetSection("Endor").Get<EndorOptions>();
            services.AddSingleton<Endor.Tenant.IEnvironmentOptions>(endorOptions);
            services.AddSingleton<EndorOptions>(endorOptions);

            services.AddSingleton<IKeyedFactory<IEmailProvider, EmailProviderType>, EmailProviderFactory>();

            ConfigureExternalEndorServices(services);
            this.AddAuthentication(services, Configuration);
            services.AddDbContext<AppDbContext>();
            services.AddCors();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(CustomActionFilter));
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            })
            .AddApplicationPart(typeof(LogLevelController).GetTypeInfo().Assembly);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(swaggerVersion, new OpenApiInfo
                {
                    Title = "Endor Comm Api",
                    Version = swaggerVersion
                });
#pragma warning disable CS0618
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteActions();
                ApplyIgnoreRelationships(c);
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                c.OperationFilter<SwaggerSecurityRightsDocumentFilter>();
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".XML";
                string commentsFile = System.IO.Path.Combine(baseDirectory, commentsFileName);

                if (System.IO.File.Exists(commentsFile))
                {
                    c.IncludeXmlComments(commentsFile);
                }
                c.SchemaFilter<SwaggerAddMissingEnums>();
            });
            return ContainerSetup.InitializeWeb(Assembly.GetExecutingAssembly(), services);
        }

        /// <summary>
        /// This method configures the authentication service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public abstract void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration);

        /// <summary>
        /// Configures External Endor Services
        /// </summary>
        /// <param name="services"></param>
        public virtual void ConfigureExternalEndorServices(IServiceCollection services)
        {
            services.AddSingleton<ITenantDataCache, NetworkTenantDataCache>();
            services.AddSingleton<ITaskQueuer>(new HttpTaskQueuer(Configuration["Endor:TasksAPIURL"], Configuration["Endor:TenantSecret"]));
            services.AddSingleton<RemoteLogger>();
            services.AddTransient<IRTMPushClient>((x) => new RealtimeMessagingPushClient(Configuration["Endor:MessagingServerURL"]));
            
            /*var _gmailProvider = new GmailProvider(Configuration);
            var _office365Provider = new Office365Provider(Configuration);

            services.AddSingleton<GmailProvider>(_gmailProvider);
            services.AddSingleton<Office365Provider>(_office365Provider);
            */
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline. 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="appLifetime"></param>
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            RemoteLoggingExtensions.ConfigureSystemRemoteLogging(Configuration, loggerFactory, appLifetime);
            /*if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/error-local-development");
            }
            else
            {
                app.UseExceptionHandler("/error");
            }*/
            app.UseAuthentication();
            app.UseSwagger()
            .UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{swaggerVersion}/swagger.json", $"Endor Comm Api {swaggerVersion}");
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = "swagger";
            })
            .UseCors(t => t.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod())
            .UseRouting()
            .UseAuthorization()
            .UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        
        private static void ApplyIgnoreRelationships(Swashbuckle.AspNetCore.SwaggerGen.SwaggerGenOptions c)
        {
            c.SchemaFilter<SwaggerIgnoreRelationshipsInNamespace>("Endor.Comm.Core.Entities");
        }

    }

    /// <summary>
    /// Startup Class
    /// </summary>
    public class Startup : BaseStartup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env) : base(env)
        {
        }

        /// <summary>
        /// This method configures the authentication service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
            {
                option.Audience = Configuration["Auth:ValidAudience"];
                option.TokenValidationParameters = new TokenValidationParameters()
                {
                    NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Auth:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(WebEncoders.Base64UrlDecode(Configuration["Auth:SymmetricKey"])),
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMilliseconds(2)
                };
            });
            services.AddAuthorization();
        }
    }
}
